<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Postlikes extends Authenticatable {

    protected $table = 'postlikes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
