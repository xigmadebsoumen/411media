<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ride';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('title', 'description');
}
