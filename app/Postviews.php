<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postviews extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'postviews';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('post_id', 'viewer_id');
}
