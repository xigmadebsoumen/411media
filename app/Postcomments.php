<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Postcomments extends Authenticatable {

    protected $table = 'postcomments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
