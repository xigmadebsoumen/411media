<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'query';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('name', 'email', 'phone_number', 'message');
}
