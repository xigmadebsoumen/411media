<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Bannercontent extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'bannercontent';
    
    protected $fillable = [
        'title', 'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   
}
