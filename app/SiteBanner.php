<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteBanner extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'site_banner';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('name', 'image');
}