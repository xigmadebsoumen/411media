<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalTopic extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'legal_topic';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('name', 'image', 'description');
}
