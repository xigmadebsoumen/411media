<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class PostCategory extends Authenticatable
{
    
    
    
    protected $table = 'postcategories';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
