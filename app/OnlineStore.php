<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineStore extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'online_store';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('album_name', 'music_designed',
								'album_launch_by', 'album_launch_date',
								'songs_list', 'album_cover_image',
								'songs', 'language', 'no_of_songs',
								'new_till_date', 'price');
}
