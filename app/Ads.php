<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ads';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $fillable = array('page', 'position', 'ad', 'status');

}
