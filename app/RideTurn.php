<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class RideTurn extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ride_turn';
    
    protected $fillable = [
        'dateparture_timing','pickup_location','email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   
}
