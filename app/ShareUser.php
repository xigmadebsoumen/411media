<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ShareUser extends Authenticatable {

    protected $table = 'shareuser';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
