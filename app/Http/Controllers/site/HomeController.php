<?php

namespace App\Http\Controllers\site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\library\my_functions; // Get custom pagination function
use DB;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;
use Mail;
use App\SiteBanner;
use App\User;
use App\Ride;
use App\Route;
use App\RideTurn;
use App\EmailTemplate;
use App\Release;
use App\SiteSettings;
use App\Blog;
use App\Photos;
use App\Video;
use App\Query;
use Session;
use Cookie;

class HomeController extends Controller {

    function __construct() {





        $this->_myFun = new My_functions;
        $siteSettings = SiteSettings::all();
        foreach ($siteSettings as $siteSetting) {
# code...
        }
        view()->share('settings', $siteSetting);
    }

//public function autocomplete(Request $request){
//$query = $request->get('q');
//$searchResults = Products::where('product_name', 'LIKE', '%'.$query.'%');
//and router.php te:
//Route::get('autocomplete', array('as' => 'autocomplete', 'uses' => 'SearchController@autocomplete'));
//



    public function index() {
        
//        if (Auth::check()) {
//            //$u = Auth::user(); echo "<pre>"; print_r($u); exit;
//            $udt = Auth::user();
//            if($udt->role == "user"){
//                //return redirect('timeline')->send();
//            }   Redirect::to('timeline')->send();
//        }
        $user = User::all();
        $autocomplete = '';
        foreach ($user as $u) {
            if ($u->role != 'admin' && $u->status == 1) {
                $name = $u->fname . " " . $u->lname;
                $autocomplete = $autocomplete . "," . $name;
            }
        }
        $ac = ltrim($autocomplete, ",");
        return View::make('site.index')->with(compact('ac','cokemail','cokepass'));
//return View::make('site.index')->with(compact('ourbus', 'ride'));
    }

    public function ride_and_turn() {
// print_r($_REQUEST);die;
        $email = Input::get('email');
//print_r($email);die;
        $dep_datetime = Input::get('dep_datetime');
        $pickup = Input::get('pickup');


        $sql = " SELECT * FROM  ride_turn WHERE email='" . $email . "' ";


        $emailcheck = DB::select($sql);


        if ($emailcheck->num_rows() == 0) {

            echo "hi";
            die;
        }
//     $rideturn = new  RideTurn;
//     $rideturn->dateparture_timing  = $dep_datetime ;
//     $rideturn->pickup_location  =  $pickup;
//     $rideturn->email = $email;
//     $rideturn->save();
//    echo 1;
//   } else {
//   echo 0;
//  }
//   exit;
    }

    public function farespasses() {

        $route1 = Route::find($id = 1);
        $route2 = Route::find($id = 2);

        return View::make('site.fares&passes')->with(compact('route1', 'route2'));
    }

    public function maintenance() {

        $maintenance = Content::find($id = 5);
        $seo_details = array($maintenance->metatitle, $maintenance->meatkey,
            $maintenance->metades);
        return View::make('site.maintenance')->with(compact('maintenance'));
    }

    public function modernization() {

        $modernization = Content::find($id = 6);
        $seo_details = array($modernization->metatitle, $modernization->meatkey,
            $modernization->metades);
        return View::make('site.modernization')->with(compact('modernization'));
    }

    public function prepurchase() {

        $prepurchase = Content::find($id = 7);
        $seo_details = array($prepurchase->metatitle, $prepurchase->meatkey,
            $prepurchase->metades);
        return View::make('site.purchaseassessments')->with(compact('prepurchase'));
    }

    public function contact() {
//print_r($_POST);die;

        if ($_POST) {


// ----- success mail----- //
            $siteSettings = SiteSettings::all();
            foreach ($siteSettings as $siteSetting) {
                $emailid = $siteSetting->admin_email;
            }


            $string = EmailTemplate::find('2');



            $arr1 = array('[HOME]', '[WORK]');

            $arr2 = array(ucfirst($_POST['home']), $_POST['work']);

            print_r($arr2);
            die;

            $arr = str_replace($arr1, $arr2, $string['description']);
            $msg;
// echo"<pre>";
// print_r($arr);exit;

            $subject = $string['subject'];
            $email = $emailid;
// $email="munmun.saha@xigmapro.com";
            Mail::queue('blank', array('msg' => $arr), function($message) use ($subject, $email) {
                $message->to($email)->subject($subject);
            });
//     // ---- mail end ---- //
            return Redirect::to('')->with('message', 'Your message has sent  successfully, please check your mail!!');
        }
    }

}
