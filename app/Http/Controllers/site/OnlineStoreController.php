<?php

namespace App\Http\Controllers\site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\library\my_functions; // Get custom pagination function
use DB;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;
use URL;
use App\SiteSettings;
use App\OnlineStore;
use App\Order;
use App\EmailTemplate;
class OnlineStoreController extends Controller
{
	function __construct(){
    	$this->_myFun = new My_functions;
        $siteSettings = SiteSettings::all();
        foreach ($siteSettings as $siteSetting) {
            # code...
        }
        view()->share('settings', $siteSetting);
    }
	public function buyOnline()
	{
		$stores = OnlineStore::orderBy('created_at', 'desc')->get();
		return View::make('site.online_store')->with(compact('stores'));
	}
    public function online_form($id)
    {
        $stores = OnlineStore::find($id);
        return View::make('site.online_form')->with(compact('stores'));
    }
    public function saveOrder()
    {
        $data = Input::all();
        $order = new Order;
        $order->product_id = $data['product_id'];
        $order->transaction_id = 0;
        $order->user_name = $data['first_name']." ".$data['last_name'];
        $order->user_email = $data['email'];
        $order->user_phone_number = $data['phone_number'];
        $order->user_address = $data['address'];
        $order->status = 'pending';

        $order->save();

        $_POST['order_id'] = $order->id;
        $_POST['surl'] = URL::to('/payumoney/result');          
        return View::make('site.payumoney.payumoney');

    }
    public function payment()
    {
        $data = $_POST;
        $order = Order::find($data['order_id']);

        $order->transaction_id = $data['transaction_id'];
        $order->status = $data['status'];

        $order->save();

        $stores = OnlineStore::find($order['product_id']);

        if($order['status'] == 'success'){
        //     // ----- success mail----- //
            $string= EmailTemplate::find('2');
            $arr1 = array('[TRANSACTION_STATUS]','[TRANSACTION_ID]','[TRANSACTION_AMOUNT]',
                '[ALBUM_NAME]','[NO_OF_SONGS]', '[PRICE]');

            $arr2 = array(ucfirst($order['status']),$order['transaction_id'],$stores['price'],
                $stores['album_name'],$stores['no_of_songs'], $stores['price']);

            $arr = str_replace($arr1,$arr2,$string['description']);$msg;
            
            $subject = $string['subject'];
            $email = $stores['user_email'];
            Mail::queue('blank', array('msg' => $arr), function($message) use ($subject, $email) {
            $message->to($email)->subject($subject);
            });   
        //     // ---- mail end ---- //
            return Redirect::to('buy-online')->with('message', 'Your transaction has been completed successfully, please check your mail!!');
        } else {
        //     // ----- failed mail----- //
            $string= EmailTemplate::find('1');
            
            $arr = $string['description'];$msg;
            print_r($arr); return;
            $subject = $string['subject'];
            $email = $stores['user_email'];
            Mail::queue('blank', array('msg' => $arr), function($message) use ($subject, $email) {
            $message->to($email)->subject($subject);
            });   
        //     // ---- mail end ---- //
            return Redirect::to('buy-online')->with('message', 'Your transaction has been failed!!');
        }
    }
}