<?php

namespace App\Http\Controllers\site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Post;
use App\SiteSettings;
use App\EmailTemplate;
use Config;
use Mail;  
use Auth;
use Redirect;
use View;
use Validator;
use Image;
use DB;
use Hash;
use Session;

class UserController extends Controller {

    function __construct() {
        if (!Auth::check()) {
            Redirect::to('/')->send();
        } else {
            $udt = Auth::user();
            //echo "<pre>"; print_r($udt); exit;
            if($udt->role != "admin"){
                view()->share('user', Auth::user());
            } else if($udt->role != "user"){
                Redirect::to('/')->send();
            }
        }
    }


    
    
//    public function frontlogin(Request $request) {
//        
//        $email = $input['email'];
//        //$remember = true;
//        //$remember = (isset($data['remember'])) ? true : null;
//        $active_user = User::whereEmail($email)->where('role', '!=', 'admin')->where('status', '=', 1)->first();
//        //echo $active_user->email; echo "++"; echo $input['password'];       
//        if (!$active_user) {
//            $attempt = false;
//            $rs = 0;
//        } else {
//            $attempt = Auth::attempt(array('email' => $input['email'], 'password' => $input['password']));
//        }
//        print_r(Auth::user());
//        echo $attempt;
//        exit;
//    }    
    

//    public function addnewslike(Request $request) {
//        
//        $input = $request->all();
//        $input['u_id'] = Auth::guard('user')->user()->id;
//        $checkexistingnewslike = Newslike::where([['n_id','=',$input['n_id']],['u_id','=',$input['u_id']]])->get()->toArray();
//        /*if(count($checkexistingnewslike)==0) {
//            echo 0;
//        }*/
//        $newslike = Newslike::create($input);       
//        $newslikes = Newslike::where('n_id','=',$input['n_id'])->get()->toArray();
//        $like = ['count' => count($newslikes), 'id' => $newslike->id];        
//        echo json_encode($like);
//        exit;
//    }


    public function logout() {
        //echo "rr"; exit;
        Auth::logout();
        //Auth::guard('admin')->logout();
        return redirect('/')->with('message', 'Sign Out Successfully');
        //return Redirect::route('site')->with('message', 'Sign Out Successfully');
    }
    
  
    public function mysettings() {
        $udt = Auth::user();
        $users = User::find($udt->id);
        //echo "<pre>"; print_r($users); exit;
        return View::make('site.users.mysettings')->with(compact('users'));
    }    
    
    
    
    
    public function timeline() {
        //echo base64_decode('MTU='); exit;
        //echo "<pre>"; print_r(Auth::guard('user')->user()); echo Auth::check(); exit;
        //$u = Auth::user(); echo "<pre>"; print_r($u); exit;
        $users = User::where('role', '!=', 'admin')->get();
        //echo "<pre>"; print_r($users); exit;
        $udt = Auth::user();
        $users = User::where('id', '=', $udt->id)->get();
        //echo "<pre>"; print_r($users); exit;
        
        $postdt = DB::table('posts')->where('uid', '=', $udt->id)
                                   ->skip(0)
                                   ->take(24)
                                   ->select('posts.*')
                                   ->orderBy('id', 'desc')
                                   ->get();
        $sl = 0;
        $post = array();
        foreach($postdt as $pdtk => $pdtv){
            $postcat = DB::table('postcategories')->where('postid', '=', $pdtv->id)->get();
            $postuser = DB::table('users')->where('id', '=', $pdtv->uid)->first();
            $post[$sl]['Type'] = $pdtv->type;
            
            $post[$sl]['Time'] = $this->calculate_time_span($pdtv->updated_at);
            $post[$sl]['Post'] = $pdtv;
            $post[$sl]['Category'] = $postcat;
            $post[$sl]['User'] = $postuser;
            $sl++;
        }
        //echo "<pre>"; print_r($post); exit;
        return View::make('site.users.timeline')->with(compact('users','post'));
    }

    public function calculate_time_span($date){
    $seconds  = strtotime(date('Y-m-d H:i:s')) - strtotime($date);

        $months = floor($seconds / (3600*24*30));
        $day = floor($seconds / (3600*24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours*3600)) / 60);
        $secs = floor($seconds % 60);

        if($seconds < 60)
            $time = $secs." seconds ago";
        else if($seconds < 60*60 && $seconds > 60  )
            $time = $mins." min ago";
        else if($seconds < 24*60*60 && $seconds > 60*60)
            $time = $hours." hours ago";
        else if($seconds < 24*60*60*30 && $seconds > 24*60*60)
            $time = $day." day ago";
        else
            $time = $months." month ago";

        return $time;
    }
    
    public function myprofile() {
        $udt = Auth::user();
        $users = User::find($udt->id);
        //echo "<pre>"; print_r($users); exit;
        return View::make('site.users.myprofile')->with(compact('users'));
    }

    public function profile() {
        $udt = Auth::user();
        $users = User::find($udt->id);
        
        $follwing = DB::table('follows')->where('followby', '=', $users->id)->select('follows.*')->count();
        $followers = DB::table('follows')->where('followto', '=', $users->id)->select('follows.*')->count();         
        $views = DB::table('postviews')->where('user_id', '=', $users->id)->select('postviews.*')->count();        
        
        $postdt = DB::table('posts')->where('uid', '=', $udt->id)
                                   ->skip(0)
                                   ->take(24)
                                   ->select('posts.*')
                                   ->orderBy('id', 'desc')
                                   ->get();
        $sl = 0;
        $post = array();
        foreach($postdt as $pdtk => $pdtv){
            $postcat = DB::table('postcategories')->where('postid', '=', $pdtv->id)->get();
            $postuser = DB::table('users')->where('id', '=', $pdtv->uid)->first();
            $post[$sl]['Type'] = $pdtv->type;
            
            $post[$sl]['Time'] = $this->calculate_time_span($pdtv->updated_at);
            $post[$sl]['Post'] = $pdtv;
            $post[$sl]['Category'] = $postcat;
            $post[$sl]['User'] = $postuser;
            $sl++;
        }        
        
        //echo "<pre>"; print_r($users); exit;
        return View::make('site.users.profile')->with(compact('users','post','follwing','followers','views'));
    }    
    
    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateprofile(Request $request, $id) {
        $data = Input::all();
        
        //echo "<pre>"; print_r($data); echo "<pre>"; print_r($_FILES); exit; 
        
        
        $rules = array('fname' => 'required', 'lname' => 'required');

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {

            $user = User::find($id);
            $user->fname = Input::get('fname');
            $user->lname = Input::get('lname');
            $user->name = Input::get('fname')." ".Input::get('lname');
            $user->pname = Input::get('pname');
            $user->tagline = Input::get('tagline');
            $user->website = Input::get('website');
            $user->twitter = Input::get('twitter');
            $user->facebook = Input::get('facebook');
            $user->bio = Input::get('bio');
            $user->location = Input::get('location');
            $user->contactemail = Input::get('contactemail');
            $user->email = Input::get('email');

            if (Input::hasFile('image')) {
                $file = Input::file('image');
                $imageName = $file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image = time() . "image." . $file_ext;
                $destinationCropPath = public_path('/products/crop/user');
                $img = Image::make($file->getRealPath())->resize(970, 450, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationCropPath . '/' . $image);

                $destinationThumbPath = public_path('/products/thumb/user');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationThumbPath . '/' . $image);

                $upload_image = $file->move('products/main/user', $image);
                $user->image = $image;
            }

            if (Input::hasFile('headerimage')) {
                $file = Input::file('headerimage');
                $imageName = $file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image = time() . "headerimage." . $file_ext;
                $destinationCropPath = public_path('/products/crop/user');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationCropPath . '/' . $image);

                $destinationThumbPath = public_path('/products/thumb/user');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationThumbPath . '/' . $image);

                $upload_image = $file->move('products/main/user', $image);
                $user->headerimage = $image;
            }            
            
            
            
            $user->save();
            $new_password = Input::get('new_password');
            if ($new_password != '') {
                // echo "string"; return;
                    $password = Hash::make($new_password);
                    DB::table('users')
                            ->where('id', $id)
                            ->update(['password' => $password]);

            }
            return Redirect::route('site.user.profile');
        }
    }    
    
    
    
    
    public function create() {
        return View::make('admin.users.create');
    }

    public function store() {
        $data = Input::all();
        $rules = array('email' => 'required',
            'password' => 'required');
        $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $active_user = User::whereEmail(Input::get('email'))->first();
            if ($active_user) {
                return Redirect::back()->withInput()->with('message', 'This user already in our database');
            }
            $customer = new User;
            $customer->name = Input::get('name');
            $customer->email = Input::get('email');
            $customer->password = Hash::make(Input::get('password'));
            $customer->save();
            // Mail
            $settings = SiteSettings::find('1');
            $link = Config::get('config.baseurl');
            $link = url('/');
            $baseLink = url('/');
            $site_logo = $link . '/products/crop/settings/' . $settings->site_logo;
            $facebook_logo = $link . 'theme/site/images/facebook.png';
            $twitter_logo = $link . 'theme/site/images/twitter.png';
            $linkedin_logo = $link . 'theme/site/images/linkedin.png';
            $gmail_logo = $link . 'theme/site/images/google.png';
            $string = EmailTemplate::where('type', '=', 'newadminuser')->get()->toArray();
            //echo "<pre>"; print_r($string); //exit;
            $arr1 = array('[SITE_LOGO]', '[NAME]', '[EMAIL]', '[PASSWORD]', '[LINK]', '[SITELINK]', '[FACEBOOK]',
                '[TWITTER]', '[GMAIL]', '[LINKEDIN]', '[ADDRESS]', '[FACEBOOK_LOGO]',
                '[TWITTER_LOGO]', '[LINKEDIN_LOGO]', '[GOOGLE_LOGO]');

            $arr2 = array($site_logo, Input::get('name'), Input::get('email'), Input::get('password'), $link, $baseLink, $settings->facebook_url,
                $settings->twitter_url, $settings->google_url, $settings->pinterest_url, $settings->address,
                $facebook_logo, $twitter_logo, $linkedin_logo, $gmail_logo);
            $arr = str_replace($arr1, $arr2, $string[0]['description']);
            $msg;
            $email = Input::get('email');
            $subject = $string[0]['subject'];
            Mail::queue('blank', array('msg' => $arr), function($message) use ($subject, $email) {
                $message->to($email)->subject($subject);
            });
            // ---- mail end ---- //
            return Redirect::route('admin.users.index')->with('message', 'User added successfully');
        }
    }

    public function changeStatus($id) {

        $users = User::find($id);
        $status = $users->status;

        if ($status == 0) {
            $users->status = '1';
        } else {
            $users->status = '0';
        }
        $users->save();

        return Redirect::route('admin.users.index')->with('message', 'Status Changed Successfully');
    }

    public function destroy($id) {
        $users = User::find($id);

        $users->delete();
        // redirect
        return Redirect::route('admin.users.index')->with('message', 'User deleted Successfully');
    }

//    public function changeStatus($id) {
//
//        $users = User::find($id);
//        $status = $users->status;
//
//        if ($status == 0) {
//            $users->status = '1';
//            $msg = 'User Active.';
//            $alert = 'alert-success';
//        } else {
//            $users->status = '0';
//            $msg = 'User Inactive.';
//            $alert = 'alert-danger';
//        }
//        $users->save();
//
//        Session::flash('message', $msg);
//        Session::flash('alert-class', $alert);
//        return Redirect::route('admin.users.index');
//    }    
    // public function update(Request $request, $id)
    // {
    //     $data = Input::all();
    //     $rules = array('email' => 'required',
    //                     );   
    //     $validator = Validator::make($data, $rules);
    //     if ($validator->fails()) {
    //           return Redirect::back()->withErrors($validator)->withInput();
    //     }else {
    //         $user = User::find($id);
    //         $user->email  =  Input::get('email');     
    //         $user->save();
    //         $current_password = Input::get('current_password');
    //         $new_password = Input::get('new_password');
    //         if ($current_password != '' && $new_password != '') {
    //             if (Hash::check($current_password, $user->password)) {
    //                 $password = Hash::make($new_password);
    //                 DB::table('users')
    //                     ->where('id', 1)
    //                     ->update(['password' => $password]);
    //             } else {
    //                 return Redirect::back()->with('message', 'you entered wrong password');
    //             }
    //         }
    //         return Redirect::route('admin.settings.my-profile.index');
    //     }
    // } 


    public function update(Request $request, $id) {
        $data = Input::all();
        $rules = array('name' => 'required',
            'username' => 'required',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {

            $user = User::find($id);
            $user->name = Input::get('name');
            $user->username = Input::get('username');
            if (Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName = $file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image = time() . "." . $file_ext;
                $destinationCropPath = public_path('/products/crop/members');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationCropPath . '/' . $image);

                $destinationThumbPath = public_path('/products/thumb/members');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationThumbPath . '/' . $image);

                $upload_image = $file->move('products/main/members', $image);
                $user->image = $image;
            }
            $user->save();

            $current_password = Input::get('current_password');
            $new_password = Input::get('new_password');
            if ($current_password != '' && $new_password != '') {
                // echo "string"; return;
                if (Hash::check($current_password, $user->password)) {
                    $password = Hash::make($new_password);
                    DB::table('users')
                            ->where('id', 1)
                            ->update(['password' => $password]);
                } else {

                    return Redirect::back()->with('message', 'you entered wrong password');
                }
            }

            return Redirect::route('admin.user.index');
        }
    }

//    public function destroy($id) {
//        $user = User::find($id);
//        /*
//        if (file_exists(public_path('products/crop/banner/' . $user->image))) {
//            unlink(public_path('products/crop/banner/' . $user->image));
//        }
//        if (file_exists(public_path('products/thumb/banner/' . $user->image))) {
//            unlink(public_path('products/thumb/banner/' . $user->image));
//        }
//        if (file_exists(public_path('products/main/banner/' . $user->image))) {
//            unlink(public_path('products/main/banner/' . $user->image));
//        }
//        */
//        $user->delete();
//        // redirect
//        return Redirect::route('admin.users');
//    }
}
