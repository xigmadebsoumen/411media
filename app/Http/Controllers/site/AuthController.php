<?php

namespace App\Http\Controllers\site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
//use Illuminate\Support\Facades\Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Views;
use App\Follows;
use App\SiteSettings;
use App\EmailTemplate;
use Auth;
use Redirect;
use View;
use Validator;
use Session;
use Hash;
use Mail;
use DB;
use Cookie;

class AuthController extends Controller {

    protected $mailer;

    public function __construct(Mailer $mailer) {
        $this->mailer = $mailer;
        //$this->activationRepo = $activationRepo;
    }

    //use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    //protected $redirectTo = '/';


    /* public function __construct()
      {
      $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
      } */

    /* protected function validator(array $data)
      {
      return Validator::make($data, [
      'name' => 'required|max:255',
      'email' => 'required|email|max:255|unique:users',
      'password' => 'required|min:6|confirmed',
      ]);
      }


      protected function create(array $data)
      {
      return User::create([
      'name' => $data['name'],
      'email' => $data['email'],
      'password' => bcrypt($data['password']),
      ]);
      } */

    public function searchprofile($id = null) {
        if ($id != '') {
            $pname = $id;
        } else {
            Redirect::to('/')->send();
        }

        if (!Auth::check()) {
            $loguser = 0;
            $ip = $_SERVER['REMOTE_ADDR'];
            $usersdt = '';
        } else {
            $udt = Auth::user();
            $loguser = $udt->id;
            $ip = $_SERVER['REMOTE_ADDR'];
            $usersdt = User::where('id', '=', $udt->id)->first();
        }

        $users = DB::table('users')->where('pname', '=', $pname)->select('users.*')->first();
        $getUser = DB::table('views')->where('viewer_id', '=', $loguser)->where('ip', '=', $ip)->where('user_id', '=', $users->id)->select('views.*')->first();
        $getIp = DB::table('views')->where('views.ip', '=', $ip)->where('views.user_id', '=', $users->id)->select('views.*')->first();

        if (empty($getUser)) {
            if (empty($getIp)) {
                $views = new Views;
                $views->ip = $ip;
                $views->user_id = $users->id;
                $views->viewer_id = $loguser;
                $views->is_active = 1;
                $views->save();

                $dt = User::find($users->id);
                $dt->view_count = $users->view_count + 1;
                $dt->save();
            } else {
                if ($loguser != 0) {
                    $dt = Views::find($getIp->id);
                    $dt->viewer_id = $loguser;
                    $dt->save();
                }
            }
        }
        if (Auth::check()) {
            $getFollow = DB::table('follows')->where('follows.followto', '=', $users->id)->where('follows.followby', '=', $udt->id)->select('follows.*')->first();
        } else {
            $getFollow = array();
        }

        //echo "<pre>"; print_r($getFollow); exit;

        $follwing = DB::table('follows')->where('followby', '=', $users->id)->select('follows.*')->count();
        $followers = DB::table('follows')->where('followto', '=', $users->id)->select('follows.*')->count();
        $views = DB::table('postviews')->where('user_id', '=', $users->id)->select('postviews.*')->count();
        $postdt = DB::table('posts')->where('uid', '=', $users->id)
                ->skip(0)
                ->take(25)
                ->select('posts.*')
                ->orderBy('id', 'desc')
                ->get();
        $sl = 0;
        $post = array();
        foreach ($postdt as $pdtk => $pdtv) {
            $postcat = DB::table('postcategories')->where('postid', '=', $pdtv->id)->get();
            $postuser = DB::table('users')->where('id', '=', $pdtv->uid)->first();
            $post[$sl]['Type'] = $pdtv->type;
            $post[$sl]['Time'] = $this->calculate_time_span($pdtv->updated_at);
            $post[$sl]['Post'] = $pdtv;
            $post[$sl]['Category'] = $postcat;
            $post[$sl]['User'] = $postuser;
            $sl++;
        }

        //echo "<pre>"; print_r($usersdt); exit;
        //echo base64_decode("MQ=="); exit;

        return View::make('site.users.searchprofile')->with(compact('users', 'post', 'usersdt', 'follwing', 'followers', 'views', 'getFollow'));
    }

    public function calculate_time_span($date) {
        $seconds = strtotime(date('Y-m-d H:i:s')) - strtotime($date);

        $months = floor($seconds / (3600 * 24 * 30));
        $day = floor($seconds / (3600 * 24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours * 3600)) / 60);
        $secs = floor($seconds % 60);

        if ($seconds < 60)
            $time = $secs . " seconds ago";
        else if ($seconds < 60 * 60 && $seconds > 60)
            $time = $mins . " min ago";
        else if ($seconds < 24 * 60 * 60 && $seconds > 60 * 60)
            $time = $hours . " hours ago";
        else if ($seconds < 24 * 60 * 60 * 30 && $seconds > 24 * 60 * 60)
            $time = $day . " day ago";
        else
            $time = $months . " month ago";
        return $time;
    }

    public function checkfbuser(Request $request) {
        $input = $request->all();
        $pnameExist = User::where('fbid', '=', $input['id'])->get()->toArray();
        if (!empty($pnameExist)) {
            echo 1;
            exit;
        } else {
            $emailExist = User::whereEmail($input['email'])->first();
            if (!empty($emailExist)) {
                echo 2;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
        //echo "<pre>"; print_r($pnameExist);         echo "<pre>"; print_r($input);  exit;
    }

    public function checkloginfbuser(Request $request) {
        $input = $request->all();
        $pnameExist = User::where('fbid', '=', $input['id'])->get()->toArray();
        if (!empty($pnameExist)) {
            echo 1;
            exit;
        } else {
            $emailExist = User::whereEmail($input['email'])->first();
            if (!empty($emailExist)) {
                echo 2;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
        //echo "<pre>"; print_r($pnameExist);         echo "<pre>"; print_r($input);  exit;
    }

    public function usersignup(Request $request) {
        $input = $request->all();
        //echo "<pre>"; print_r($input); exit;
        $flag = true;
        $fbid = $input['fbid'];
        $fname = $input['fname'];
        $lname = $input['lname'];
        $email = $input['email'];
        $password = $input['password'];
        //$pname = $input['pname'];
        $emailExist = User::whereEmail($email)->first();
        //print_r($emailExist); exit;
        //$pnameExist = User::where('pname', '=', $pname)->get()->toArray();
        //$pnameExist = User::whereEmail($email)->where('role', '!=', 'admin')->where('pname', '=', $pname)->first();
        if ($emailExist) {
            echo "e";
            exit;
//        } else if ($pnameExist) {
//            echo "p";
//            exit;
        } else {
            $customer = new User;
            $customer->fname = Input::get('fname');
            $customer->lname = Input::get('lname');
            $customer->name = Input::get('fname') . " " . Input::get('lname');
            $customer->email = Input::get('email');
            $customer->fbid = Input::get('fbid');
            $customer->password = Hash::make(Input::get('password'));
            //$customer->pname = Input::get('pname');
            $customer->role = "user";
            $customer->status = 1;
            $customer->remember_token = Input::get('_token');

            $customer->save();
            $custid = $customer->id;

            $settings = SiteSettings::find('1');
            //$link = Config::get('config.baseurl');
            $link = url('/');
            $baseLink = url('/');

            //echo $flink; exit;
            //$name = $emailExist->fname . " " . $emailExist->lname;
            $site_logo = $link . '/products/crop/settings/' . $settings->site_logo;
            $string = EmailTemplate::where('type', '=', 'newreg')->get()->toArray();
            $arr1 = array('[SITE_LOGO]', '[NAME]', '[LINK]');
            $arr2 = array($site_logo, Input::get('fname'), $baseLink);
            $arr = str_replace($arr1, $arr2, $string[0]['description']);
            $msg;
            $email = Input::get('email');
            $subject = $string[0]['subject'];
            Mail::queue('blank', array('msg' => $arr), function($message) use ($subject, $email) {
                $message->to($email)->subject($subject);
            });
            echo $customer->id; exit;
        }
    }
    
    
    public function checkpname(Request $request) {
        $input = $request->all();
        //echo "<pre>"; print_r($input); exit;
        $pnameExist = User::where('pname', '=', $input['pname'])->get()->toArray();       
        if($pnameExist){
            echo 1; exit;
        } else {
            $users = User::find($input['id']);
            $users->pname = $input['pname'];
            $users->save();
            echo 0; exit;
        }

    }    
    
    public function forgotpassword(Request $request) {
        $input = $request->all();
        $flag = true;
        $email = $input['email'];
        $emailExist = User::whereEmail($email)->first();
        if ($emailExist) {
            // Mail
            $cpval = str_random($emailExist->id);
            User::where('id', $emailExist->id)->update(array('cpval' => $cpval));
            $settings = SiteSettings::find('1');
            //$link = Config::get('config.baseurl');
            $link = url('/');
            $baseLink = url('/');

            $flink = url('changepassword/' . $cpval);
            //echo $flink; exit;
            $name = $emailExist->fname . " " . $emailExist->lname;
            $site_logo = $link . '/products/crop/settings/' . $settings->site_logo;
            $string = EmailTemplate::where('type', '=', 'forgotpassword')->get()->toArray();
            $arr1 = array('[SITE_LOGO]', '[NAME]', '[FLINK]', '[LINK]');
            $arr2 = array($site_logo, $name, $flink, $baseLink);
            $arr = str_replace($arr1, $arr2, $string[0]['description']);
            $msg;
            $email = $emailExist->email;
            $subject = $string[0]['subject'];

            //mail($email,$subject,$msg);
//            $this->mailer->raw($arr, function (Message $m) use ($email) {
//                $m->to($email)->subject('Forgot password mail');
//            });


            Mail::queue('blank', array('msg' => $arr), function($message) use ($subject, $email) {
                $message->to($email)->subject($subject);
            });
            // ---- mail end ---- //            
            echo 1;
            exit;
        } else {
            echo 0;
            exit;
        }
    }

    public function changepassword($id) {
        $user = User::where('cpval', '=', $id)->get(); 
        //echo $user->id; echo "<pre>"; print_r($user); exit;
        if(empty($user[0]->id)){
            Redirect::to('/')->send();
        } else {
            return View::make('site.users.changepassword')->with(compact('user'));
        }
    }

    public function updatepassword(Request $request) {
        $data = Input::all();
        //echo "<pre>"; print_r($data); 
        if ($data['npass'] != $data['rpass']) {
            return Redirect::back()->with('message', 'Confirm Password Not Matched.');
        } else {
            $user = User::find($data['id']);
            $current_password = $data['cpass'];
            $new_password = $data['npass'];
            if ($current_password != '' && $new_password != '') {
                if (Hash::check($current_password, $user->password)) {
                    //echo "ok";
                    $password = Hash::make($new_password);
                    DB::table('users')
                            ->where('id', $data['id'])
                            ->update(['password' => $password]);
                    $dt = User::find($data['id']);
                    $dt->cpval = "";
                    $dt->save();
                } else {
                    return Redirect::back()->with('message', 'your current password entered wrong.');
                }
            }
            
            //Session::flash('message', 'Authentication require.'); 
            //Session::flash('alert-class', 'alert-warning');
            //return Redirect::route('admin.settings.my-profile.index');
            return redirect('/')->with('message', 'Password Changed Successfully');
        }
    }

    public function frontlogin(Request $request) {
        $data = Input::all();
        //echo "<pre>";  print_r($data);

        $email = $data['email'];
        $active_user = User::whereEmail($email)->where('role', '!=', 'admin')->first();
        //echo "<pre>";  print_r($active_user); exit;
        //echo $active_user->email; echo "++"; echo $input['password'];       
        if (!$active_user) {
            echo "Email not Exist. Signup as new User to login.";
            exit;
        } else {
            if ($active_user->status != 1) {
                echo "Your account has been suspended.";
                exit;
            } else if (!Hash::check($data['password'], $active_user->password)) {
                echo "Your Password Not Matched.";
                exit;
            } else {
                echo 1;
                exit;
            }
        }
    }

    public function postLogin(Request $request) {
        $remember = (isset($_REQUEST['remember'])) ? true : null;
        //echo "<pre>"; print_r($_REQUEST);
        if (isset($_REQUEST['remember'])) {
            $cookieemail = Cookie::make('logemail', $_REQUEST['email'], 60);
            $cookiepass = Cookie::make('logpass', $_REQUEST['password'], 60);
        }
        $attempt = Auth::attempt(array('email' => $_REQUEST['email'], 'password' => $_REQUEST['password']), $remember);
        //print_r(Auth::user());
        return Redirect::intended('timeline');
    }

    public function fblogin(Request $request) {
        $data = Input::all();
        $fbid = $data['fblogid'];
        $fbidExist = User::where('fbid', '=', $data['fblogid'])->first();
        Auth::loginUsingId($fbidExist->id);
        return Redirect::intended('timeline');
    }

    public function frfblogin(Request $request) {
        $data = Input::all();
        //echo "<pre>"; print_r($data); //exit;
        $fbid = $data['fbid'];
        $fbidExist = User::where('fbid', '=', $data['fbid'])->first();
        //echo $fbidExist->email; echo "<br>";
        //echo $fbidExist->password; echo "<br>";
        //echo "<pre>"; print_r($fbidExist); exit;
        //$attempt = Auth::attempt(array('email' => $fbidExist->email, 'password' => $fbidExist->password));

        Auth::loginUsingId($fbidExist->id);
        //if ( Auth::attempt(array('email' => $fbidExist->email, 'password' => $fbidExist->password), true) ) {
        //    echo "<pre>"; print_r(Auth::user()); exit;
        //} else {
        //    echo "errore"; exit;
        //}
        //echo "<pre>"; print_r($fbidExist); exit;
        return Redirect::intended('timeline');
    }

    public function searchfriends() {
        $dt = trim($_REQUEST['string']);
        if (!Auth::check()) {
            $uid = 0;
        } else {
            $udt = Auth::user();
            $uid = $udt->id;
        }
        $result = User::where('fname', 'like', '%' . $dt . '%')
                ->orWhere('lname', 'like', '%' . $dt . '%')
                ->orWhere('pname', 'like', '%' . $dt . '%')
                ->where('role', '!=', 'admin')
                ->where('status', '=', 1)
                ->get();

        $users = array();
        $sl = 0;
        foreach ($result as $dt) {
            if ($dt->role != "admin" && $dt->status != 0) {
                if ($dt->id != $uid) {
                    $users[$sl]['id'] = $dt->id;
                    $users[$sl]['pname'] = $dt->pname;
                    $users[$sl]['name'] = $dt->fname . " " . $dt->lname;
                    $users[$sl]['img'] = $dt->image;
                    $sl++;
                }
            }
        }
        return View::make('site.search')->with(compact('users'));
    }

//    public function searchfriends() {
//        $dt = trim($_REQUEST['string']);
//        //echo "<pre>"; print_r($_REQUEST); exit;
//
//                
//        $result = DB::table('address')->where(function($query) use ($dt) {
//                            $query->where('fname', 'LIKE', '%' . $dt . '%')
//                                    ->where('status', '=', '1');
//                                    ->or_where('lname', 'LIKE', '%' . $dt . '%')
//                                    ->or_where('pname', 'LIKE', '%' . $dt . '%')
//                        });
//        echo "<pre>"; print_r($result); exit;
//        echo "ok"; exit;
//        //return View::make('site.auth.search');
//    }

    public function getLogin() {

        if (Auth::check()) {
            Session::flash('message', 'Already logged in.');
            Session::flash('alert-class', 'alert-warning');
            return redirect('admin/dashboard');
        }
        return View::make('admin.auth.login');
    }

    public function dashboard() {

        if (!Auth::check()) {
            Session::flash('message', 'Authentication require.');
            Session::flash('alert-class', 'alert-warning');
            return redirect('admin/login');
        }
        return View::make('admin.auth.index');
    }

    public function getProfile() {

        if (!Auth::check()) {
            Session::flash('message', 'Authentication require.');
            Session::flash('alert-class', 'alert-warning');
            return redirect('admin/login');
        }
        return View::make('admin.auth.profile');
    }

    public function postProfile(Request $request) {

        if (!Auth::check()) {
            Session::flash('message', 'Authentication require.');
            Session::flash('alert-class', 'alert-warning');
            return redirect('admin/login');
        }
        if ($request->input()) {
            $input = $request->input();
            if ($request->input('new_password')) {
                $input['password'] = bcrypt($request->input('new_password'));
            }
            $data = User::findOrFail(Auth::user()->id);
            $data->update($input);
            Session::flash('message', 'Profile updated successfully');
            Session::flash('alert-class', 'alert-success');
            return redirect('admin/profile');
        }
    }

    public function getLogout() {
        Auth::logout();
        Session::flash('message', 'Logout successfully');
        Session::flash('alert-class', 'alert-success');
        return redirect('admin/login');
    }

    public function follownow(Request $request) {
        $input = $request->all();
            $follow = new Follows;
            $follow->followto = Input::get('followto');
            $follow->followby = Input::get('followby');
            $follow->save();        
            echo 1; exit;
    }    
    
    public function unfollownow(Request $request) {
        $input = $request->all();
        $getRow = DB::table('follows')->where('followto', '=', Input::get('followto'))->where('followby', '=', Input::get('followby'))->select('follows.id')->first();
        $followa = Follows::find($getRow->id);
        $followa->delete();       
        echo 1; exit;
    }    
  
    public function getpostview($id) {
        //$input = $request->all();
        echo $id; exit;

    }     
    
    
}

?>