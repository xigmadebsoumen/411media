<?php

namespace App\Http\Controllers\site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Post;
use App\SiteSettings;
use App\EmailTemplate;
use Config;
use Mail;  
use Auth;
use Redirect;
use View;
use Validator;
use Image;
use DB;
use Hash;
use Session;

class PageController extends Controller {

    function __construct() {
        if (!Auth::check()) {
            Redirect::to('/')->send();
        } else {
            $udt = Auth::user();
            //echo "<pre>"; print_r($udt); exit;
            if($udt->role != "admin"){
                view()->share('user', Auth::user());
            } else if($udt->role != "user"){
                Redirect::to('/')->send();
            }
        }
    }

    public function blockuser() {
        $udt = Auth::user();
        $users = User::find($udt->id);
        //echo "<pre>"; print_r($users); exit;
        return View::make('site.page.blockuser')->with(compact('users'));
    }    
    
    public function copyright() {
        $udt = Auth::user();
        $users = User::find($udt->id);
        //echo "<pre>"; print_r($users); exit;
        return View::make('site.page.copyright')->with(compact('users'));
    }    
    
    public function notifications() {
        $udt = Auth::user();
        $users = User::find($udt->id);
        //echo "<pre>"; print_r($users); exit;
        return View::make('site.page.notifications')->with(compact('users'));
    }    
    
    public function privacy() {
        $udt = Auth::user();
        $users = User::find($udt->id);
        //echo "<pre>"; print_r($users); exit;
        return View::make('site.page.privacy')->with(compact('users'));
    }    
    
    public function terms() {
        $udt = Auth::user();
        $users = User::find($udt->id);
        //echo "<pre>"; print_r($users); exit;
        return View::make('site.page.terms')->with(compact('users'));
    }    
    
    
}
