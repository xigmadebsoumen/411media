<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Route;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;

class RouteController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $route = route::all();
        return View::make('admin.route.index')->with(compact('route'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.route.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
       // echo"<pre>";
        //print_r($_POST);exit;
        $data = Input::all();
        $rules = array( 'title' => 'required',
                        'description' => 'required',
                        
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            $blog = new route;
            $blog->title  =  Input::get('title');
            $blog->description  =  Input::get('description');
            $blog->date = strtotime(date('Y-m-d h:i:s'));
            $blog->meatkey  =  Input::get('meatkey');
            $blog->metatitle  =  Input::get('metatitle');
            $blog->metades = strtotime(date('metades'));




            if(Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/route');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/blog');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/route', $image);
                $blog->image = $image;                
            }
            
            $blog->save();

            return Redirect::route('admin.route.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $route = route::find($id);
        return View::make('admin.route.edit')->with(compact('route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $data = Input::all();
        $rules = array( 'title' => 'required',
                        'description' => 'required',
                       
                        );  
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            
            $route = route::find($id);
            $route->title  =  Input::get('title');
            $route->description  =  Input::get('description');

          
           // $blog->date = strtotime(date('Y-m-d h:i:s'));

            $route->save();

            return Redirect::route('admin.route.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = route::find($id);
        if (file_exists(public_path('products/crop/route/'.$blog->image))) {
            unlink(public_path('products/crop/blogs/'.$blog->image));
        }
        if (file_exists(public_path('products/thumb/route/'.$blog->image))) {
            unlink(public_path('products/thumb/blogs/'.$blog->image));
        }
        if (file_exists(public_path('products/main/route/'.$blog->image))) {
            unlink(public_path('products/main/blogs/'.$blog->image));
        }
        $blog->delete();
        // redirect
        return Redirect::route('admin.route.index');
    }
}
