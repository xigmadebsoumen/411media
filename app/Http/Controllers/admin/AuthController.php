<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
//use Illuminate\Support\Facades\Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Redirect;
use View;
use Validator;
use Session;


class AuthController extends Controller {

    public function getLogin() {
        if (Auth::check()) {
            return Redirect::intended('admin/dashboard');
        }

        return View::make('admin.auth.login');
    }

        //$active_user = User::whereUsername($username)->first();


    public function postLogin() {
        $rules = [
            'password' => 'required',
            'email' => 'required'
        ];
        $validator = Validator::make($data = Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $email = $data['email'];
        $remember = (isset($data['remember'])) ? true : null;
        //echo "<pre>"; print_r($data);
        $active_user = User::whereEmail($email)->first();
        //echo "<pre>"; print_r($active_user); return;
        if (!$active_user) {
            $attempt = false;
        } else {
            $attempt = Auth::attempt(array('email' => $active_user->email, 'password' => $data['password']), $remember);
        }
         //print_r($attempt); return;
        if ($attempt) {
            return Redirect::intended('admin/dashboard');
        }

        return Redirect::back()->withInput()->withFlashMessage('Incorrect Username/Password. Please try again.');
    }    
    
    
    
    public function logout() {
        Auth::logout();
        return Redirect::route('admin.auth.login');
    }

}
