<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LegalTopic;
use Validator;
use Hash;
use Redirect;
use View;
use Input;
use Image;
use Auth;
class LegalTopicController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    
    public function index()
    {
    	$legalTopics = LegalTopic::all();
    	return View::make('admin.legal_topic.index', compact('legalTopics'));
    }
    public function create()
    {
    	return View::make('admin.legal_topic.create');
    }
    public function store()
    {
    	$data = Input::all();
        $rules = array('name' => 'required',
                        'image'=>'required'
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            
            $legal_topic = new LegalTopic;
            $legal_topic->name  =  Input::get('name');
            $legal_topic->description  =  Input::get('description');

            if(Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/legal_topic');
                $img = Image::make($file->getRealPath())->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                
                $upload_image = $file->move('products/main/legal_topic', $image);
                $legal_topic->image = $image;                
            }
           
            $legal_topic->save();

            return Redirect::route('admin.legal_topic.index');
        }
    }
    public function edit($id)
    {
        $legal_topics = LegalTopic::find($id);
        return View::make('admin.legal_topic.edit')->with(compact('legal_topics'));

    }

    public function update($id)
    {
        $data = Input::all();
        $rules = array('name' => 'required'
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            
            $legal_topic = LegalTopic::find($id);
            $legal_topic->name  =  Input::get('name');
            $legal_topic->description  =  Input::get('description');

            if(Input::hasFile('image')) {

                if (file_exists(public_path('products/crop/legal_topic/'.$legal_topic->image))) {
                    unlink(public_path('products/crop/legal_topic/'.$legal_topic->image));
                }
                if (file_exists(public_path('products/main/legal_topic/'.$legal_topic->image))) {
                    unlink(public_path('products/main/legal_topic/'.$legal_topic->image));
                }
                
                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/legal_topic');
                $img = Image::make($file->getRealPath())->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                
                $upload_image = $file->move('products/main/legal_topic', $image);
                $legal_topic->image = $image;                
            }
           
            $legal_topic->save();

            return Redirect::route('admin.legal_topic.index');
        }
    }

    public function destroy($id)
    {
        $legal_topic = LegalTopic::find($id);
        if (file_exists(public_path('products/crop/legal_topic/'.$legal_topic->image))) {
            unlink(public_path('products/crop/legal_topic/'.$legal_topic->image));
        }
        if (file_exists(public_path('products/main/legal_topic/'.$legal_topic->image))) {
            unlink(public_path('products/main/legal_topic/'.$legal_topic->image));
        }
        
        $legal_topic->delete();
        // redirect
        return Redirect::route('admin.legal_topic.index');
    }
}
