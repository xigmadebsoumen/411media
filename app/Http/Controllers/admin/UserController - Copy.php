<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Redirect;
use View;
use Input;
use Validator;
use Image;
use DB;
use Hash;
class UserController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('login')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::find('1');
        return View::make('admin.users.index')->with(compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Input::all();
        $rules = array('name' => 'required',
                        'username'=>'required',
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            
            $user = User::find($id);
            $user->name  =  Input::get('name');
            $user->username  =  Input::get('username');     
            if(Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/members');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/members');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/members', $image);
                $user->image = $image;                
            }     
            $user->save();

            $current_password = Input::get('current_password');
            $new_password = Input::get('new_password');
            if ($current_password != '' && $new_password != '') {
                // echo "string"; return;
                if (Hash::check($current_password, $user->password)) {
                    $password = Hash::make($new_password);
                    DB::table('users')
                        ->where('id', 1)
                        ->update(['password' => $password]);
                } else {

                    return Redirect::back()->with('message', 'you entered wrong password');
                }
            }

            return Redirect::route('admin.user.index');
        }
    }    
}
