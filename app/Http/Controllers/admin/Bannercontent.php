<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Bannercontent;
use Auth;
use Redirect;
use View;
use Input;
use Validator;
use DB;
use Hash;
class BannercontentController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('login')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $bannercontents = Bannercontent::find('1');
        return View::make('admin.bannercontent.index')->with(compact('bannercontents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Input::all();
        $rules = array('title' => 'required',
                       'description'=>'required',
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            
            $bannercontent = bannercontent::find($id);
            $bannercontent->title  =  Input::get('title');
            $bannercontent->description  =  Input::get('description');        
            $bannercontent->save();
            return Redirect::bannercontent('admin.bannercontent.index');
        }
    }    
}
