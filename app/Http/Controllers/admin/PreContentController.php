<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Content;
use App\SiteFooter;
use Validator;
use Hash;
use Redirect;
use View;
use Input;
use Image;
use Auth;
use DB;
class ContentController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($type)
    {
        $content = Content::whereType($type)->first();
        // $menus = SiteFooter::where('status','=','1')->get();
        // foreach ($menus as $value) {
        //     $menu[] = $value->name;
        // }
        // echo "<pre>";
        // print_r($content); die();
        return View::make('admin.contents.index')->with(compact('content', 'type'));
    }

    public function update($id, $type)
    {
        $data = Input::all();
        $rules = array( 'title' => 'required',
                        'description' => 'required'
                        ); 
        $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {  
            $content = Content::find($id);

            if (isset($data['status'])) {
                $status = 1;
            } else {
                $status = 0;
            }

            $content->title = $data['title'];
            $content->subtitle = $data['subtitle'];
            $content->description = $data['description']; 
            $content->status = $status; 
            $content->seo_name = $data['seo_name']; 
            $content->seo_description = $data['seo_description']; 
            $content->seo_keywords = $data['seo_keywords']; 

            if(Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('products/crop/content');
                $img = Image::make($file->getRealPath())->resize(300, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('products/thumb/content');
                $img = Image::make($file->getRealPath())->resize(1200, 540, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/content', $image);
                $content->image = $image;                
            }
            $content->save();
            

            return Redirect::to('admin/contents/'.$type);        
        }
    }

}