<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\SiteSettings;
use App\EmailTemplate;
use Config;
use Mail;
use Auth;
use Redirect;
use View;
use Input;
use Validator;
use Image;
use DB;
use Hash;


class UserController extends Controller {

    function __construct() {
        if (Auth::check()) {
            view()->share('user', Auth::user());
        } else {
            Redirect::to('login')->send();
        }
    }

    public function myprofile() {
        $users = User::find('1');
        return View::make('admin.users.myprofile')->with(compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateprofile(Request $request, $id) {
        $data = Input::all();
        $rules = array('name' => 'required',
            'email' => 'required',
        );

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {

            $user = User::find($id);
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            if (Input::hasFile('image')) {
                //echo "ff";
                $file = Input::file('image');
                $imageName = $file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image = time() . "." . $file_ext;
                $destinationCropPath = public_path('/products/crop/members');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationCropPath . '/' . $image);

                $destinationThumbPath = public_path('/products/thumb/members');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationThumbPath . '/' . $image);

                $upload_image = $file->move('products/main/members', $image);
                $user->image = $image;
            }

            $user->save();
            $current_password = Input::get('current_password');
            $new_password = Input::get('new_password');
            if ($current_password != '' && $new_password != '') {
                // echo "string"; return;
                if (Hash::check($current_password, $user->password)) {
                    $password = Hash::make($new_password);
                    DB::table('users')
                            ->where('id', 1)
                            ->update(['password' => $password]);
                } else {
                    return Redirect::back()->with('message', 'you entered wrong password');
                }
            }
            return Redirect::route('admin.user.myprofile');
        }
    }

    public function index() {
        $users = User::where('role', '!=', 'admin')->get();

        //echo "<pre>"; print_r($users); exit;


        return View::make('admin.users.index')->with(compact('users'));
    }

    public function create() {
        return View::make('admin.users.create');
    }

    public function store() {
        $data = Input::all();
        $rules = array('email' => 'required',
            'password' => 'required');
        $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $active_user = User::whereEmail(Input::get('email'))->first();
            if ($active_user) {
                return Redirect::back()->withInput()->with('message', 'This user already in our database');
            }
            $customer = new User;
            $customer->fname = Input::get('fname');
            $customer->lname = Input::get('lname');
            $customer->name = Input::get('fname')." ".Input::get('lname');
            $customer->email = Input::get('email');
            $customer->password = Hash::make(Input::get('password'));
            $customer->save();
            // Mail
            $settings = SiteSettings::find('1');
            $link = Config::get('config.baseurl');
            $link = url('/');
            $baseLink = url('/');
            $site_logo = $link . '/products/crop/settings/' . $settings->site_logo;
            $facebook_logo = $link . 'theme/site/images/facebook.png';
            $twitter_logo = $link . 'theme/site/images/twitter.png';
            $linkedin_logo = $link . 'theme/site/images/linkedin.png';
            $gmail_logo = $link . 'theme/site/images/google.png';
            $string = EmailTemplate::where('type', '=', 'newadminuser')->get()->toArray();
            //echo "<pre>"; print_r($string); //exit;
            $arr1 = array('[SITE_LOGO]', '[NAME]', '[EMAIL]', '[PASSWORD]', '[LINK]', '[SITELINK]', '[FACEBOOK]',
                '[TWITTER]', '[GMAIL]', '[LINKEDIN]', '[ADDRESS]', '[FACEBOOK_LOGO]',
                '[TWITTER_LOGO]', '[LINKEDIN_LOGO]', '[GOOGLE_LOGO]');

            $arr2 = array($site_logo, Input::get('name'), Input::get('email'), Input::get('password'), $link, $baseLink, $settings->facebook_url,
                $settings->twitter_url, $settings->google_url, $settings->pinterest_url, $settings->address,
                $facebook_logo, $twitter_logo, $linkedin_logo, $gmail_logo);
            $arr = str_replace($arr1, $arr2, $string[0]['description']);
            $msg;
            $email = Input::get('email');
            $subject = $string[0]['subject'];
            Mail::queue('blank', array('msg' => $arr), function($message) use ($subject, $email) {
                $message->to($email)->subject($subject);
            });
            // ---- mail end ---- //
            return Redirect::route('admin.users.index')->with('message', 'User added successfully');
        }
    }

    
    public function edit($id) {

        $users = User::find($id);
        return View::make('admin.users.edit')->with(compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
    
//    public function update($id) {
//
//        $data = Input::all();
//        $rules = array('ad' => 'required',
//        );
//        $validator = Validator::make($data, $rules);
//        if ($validator->fails()) {
//            return Redirect::back()->withErrors($validator)->withInput();
//        } else {
//
//            $ads = ads::find($id);
//            $ads->ad = Input::get('ad');
//            //$ads->description = Input::get('description');
//
//
//            // $blog->date = strtotime(date('Y-m-d h:i:s'));
//
//            $ads->save();
//
//            return Redirect::route('admin.ads.index')->with('message', 'Advertisement updated successfully');
//        }
//    }    
    
    
    
    
    
    
    public function changeStatus($id) {

        $users = User::find($id);
        $status = $users->status;

        if ($status == 0) {
            $users->status = '1';
        } else {
            $users->status = '0';
        }
        $users->save();

        return Redirect::route('admin.users.index')->with('message', 'Status Changed Successfully');
    }

    public function destroy($id) {
        $users = User::find($id);

        $users->delete();
        // redirect
        return Redirect::route('admin.users.index')->with('message', 'User deleted Successfully');
    }

//    public function changeStatus($id) {
//
//        $users = User::find($id);
//        $status = $users->status;
//
//        if ($status == 0) {
//            $users->status = '1';
//            $msg = 'User Active.';
//            $alert = 'alert-success';
//        } else {
//            $users->status = '0';
//            $msg = 'User Inactive.';
//            $alert = 'alert-danger';
//        }
//        $users->save();
//
//        Session::flash('message', $msg);
//        Session::flash('alert-class', $alert);
//        return Redirect::route('admin.users.index');
//    }    
    
    
    
    // public function update(Request $request, $id)
    // {
    //     $data = Input::all();
    //     $rules = array('email' => 'required',
    //                     );   
    //     $validator = Validator::make($data, $rules);
    //     if ($validator->fails()) {
    //           return Redirect::back()->withErrors($validator)->withInput();
    //     }else {
    //         $user = User::find($id);
    //         $user->email  =  Input::get('email');     
    //         $user->save();
    //         $current_password = Input::get('current_password');
    //         $new_password = Input::get('new_password');
    //         if ($current_password != '' && $new_password != '') {
    //             if (Hash::check($current_password, $user->password)) {
    //                 $password = Hash::make($new_password);
    //                 DB::table('users')
    //                     ->where('id', 1)
    //                     ->update(['password' => $password]);
    //             } else {
    //                 return Redirect::back()->with('message', 'you entered wrong password');
    //             }
    //         }
    //         return Redirect::route('admin.settings.my-profile.index');
    //     }
    // } 


    public function update(Request $request, $id) {
        $data = Input::all();
        $rules = array('fname' => 'required',
            'lname' => 'required',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $user = User::find($id);
            $user->fname = Input::get('fname');
            $user->lname = Input::get('lname');
            $user->name = Input::get('fname')." ".Input::get('lname');
            $user->email = Input::get('email');
            if(!empty(Input::get('password'))){
            $user->password = Hash::make(Input::get('password'));

            }
//            if (Input::hasFile('image')) {
//
//                $file = Input::file('image');
//                $imageName = $file->getClientOriginalName();
//                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
//                $image = time() . "." . $file_ext;
//                $destinationCropPath = public_path('/products/crop/members');
//                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
//                            $constraint->aspectRatio();
//                        })->save($destinationCropPath . '/' . $image);
//
//                $destinationThumbPath = public_path('/products/thumb/members');
//                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
//                            $constraint->aspectRatio();
//                        })->save($destinationThumbPath . '/' . $image);
//
//                $upload_image = $file->move('products/main/members', $image);
//                $user->image = $image;
//            }
            $user->save();

//            $current_password = Input::get('current_password');
//            $new_password = Input::get('new_password');
//            if ($current_password != '' && $new_password != '') {
//                // echo "string"; return;
//                if (Hash::check($current_password, $user->password)) {
//                    $password = Hash::make($new_password);
//                    DB::table('users')
//                            ->where('id', 1)
//                            ->update(['password' => $password]);
//                } else {
//
//                    return Redirect::back()->with('message', 'you entered wrong password');
//                }
//            }

            return Redirect::route('admin.users.index');
        }
    }

//    public function destroy($id) {
//        $user = User::find($id);
//        /*
//        if (file_exists(public_path('products/crop/banner/' . $user->image))) {
//            unlink(public_path('products/crop/banner/' . $user->image));
//        }
//        if (file_exists(public_path('products/thumb/banner/' . $user->image))) {
//            unlink(public_path('products/thumb/banner/' . $user->image));
//        }
//        if (file_exists(public_path('products/main/banner/' . $user->image))) {
//            unlink(public_path('products/main/banner/' . $user->image));
//        }
//        */
//        $user->delete();
//        // redirect
//        return Redirect::route('admin.users');
//    }

}
