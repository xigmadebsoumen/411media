<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SiteSettings;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;

class SettingController extends Controller {

    function __construct() {
        
        if (Auth::check()) {
            view()->share('user', Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        $settings = SiteSettings::find('1');
        return View::make('admin.settings.index')->with(compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id) {
        //echo"<pre>"; print_r($_POST); //exit;
        $data = Input::all();
        $rules = array('site_name' => 'required',
            'facebook_url' => 'required',
            'twitter_url' => 'required',
            'linkedin_url' => 'required',
            'youtube_url' => 'required',
            'office_address' => 'required',
            'admin_ph_no' => 'required',
            'admin_fax_no' => 'required',
            'admin_email' => 'required|email'
        );
        $validator = Validator::make($data, $rules);
//        if ($validator->fails()) {
//            return Redirect::back()->withErrors($validator)->withInput();
//        } else {
            
            //echo"<pre>"; print_r($data);exit; 
            
            $setting = SiteSettings::find($id);
            $setting->site_name = $data['site_name'];
            //$setting->slogan_text = $data['slogan_text'];
 
//            $setting->facebook_url = $data['facebook_url'];
//            $setting->twitter_url = $data['twitter_url'];
//            $setting->linkedin_url = $data['linkedin_url'];
//            $setting->youtube_url = $data['youtube_url'];
            if(isset($data['is_captcha'])){ $setting->is_captcha = 1; } else { $setting->is_captcha = 0; }
            if(isset($data['is_facebooklgin'])){ $setting->is_facebooklgin = 1; } else { $setting->is_facebooklgin = 0; }
            
            $setting->censor = $data['censor'];
            //$setting->office_address = $data['office_address'];
            //$setting->residencial_address = $data['residencial_address'];
//            $setting->address_lat = $data['address_lat'];
//            $setting->address_lang = $data['address_lang'];
            $setting->admin_ph_no = $data['admin_ph_no'];
            $setting->admin_fax_no = $data['admin_fax_no'];
            $setting->admin_email = $data['admin_email'];

            $setting->meta_description = $data['meta_description'];
            $setting->meta_keywords = $data['meta_keywords'];
            $setting->site_title = $data['site_title'];

            if (Input::hasFile('site_logo')) {

                if (file_exists(public_path('products/crop/setting/' . $setting->site_logo))) {
                    unlink(public_path('products/crop/setting/' . $setting->site_logo));
                }
                if (file_exists(public_path('products/thumb/setting/' . $setting->site_logo))) {
                    unlink(public_path('products/thumb/setting/' . $setting->site_logo));
                }
                if (file_exists(public_path('products/main/setting/' . $setting->site_logo))) {
                    unlink(public_path('products/main/setting/' . $setting->site_logo));
                }
                $file = Input::file('site_logo');
                $imageName = $file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image = time() . "." . $file_ext;
                $destinationCropPath = public_path('/products/crop/settings');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationCropPath . '/' . $image);

                $destinationThumbPath = public_path('/products/thumb/settings');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationThumbPath . '/' . $image);

                $upload_image = $file->move('products/main/settings', $image);
                $setting->site_logo = $image;
            }

            $setting->save();
            return Redirect::to('admin/settings/site-setting');
        //}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
