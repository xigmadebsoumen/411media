<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ads;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;

class AdsController extends Controller {

    function __construct() {
        if (Auth::check()) {
            view()->share('user', Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $dt = ads::all();
        //echo "<pre>"; print_r($content); exit;


        return View::make('admin.ads.index')->with(compact('dt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return View::make('admin.content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store() {
        //echo"<pre>"; print_r($_POST);exit;
        $data = Input::all();
        $rules = array('title' => 'required',
            'description' => 'required',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $content = new content;
            $content->title = Input::get('title');
            $content->description = Input::get('description');
//            $blog->date = strtotime(date('Y-m-d h:i:s'));
//            $blog->meatkey = Input::get('meatkey');
//            $blog->metatitle = Input::get('metatitle');
//            $blog->metades = strtotime(date('metades'));




//            if (Input::hasFile('image')) {
//
//                $file = Input::file('image');
//                $imageName = $file->getClientOriginalName();
//                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
//                $image = time() . "." . $file_ext;
//                $destinationCropPath = public_path('/products/crop/ourbus');
//                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
//                            $constraint->aspectRatio();
//                        })->save($destinationCropPath . '/' . $image);
//
//                $destinationThumbPath = public_path('/products/thumb/blog');
//                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
//                            $constraint->aspectRatio();
//                        })->save($destinationThumbPath . '/' . $image);
//
//                $upload_image = $file->move('products/main/ourbus', $image);
//                $blog->image = $image;
//            }

            $content->save();

            return Redirect::route('admin.content.index')->with('message', 'Content added successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $ads = ads::find($id);
        return View::make('admin.ads.edit')->with(compact('ads'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id) {

        $data = Input::all();
        $rules = array('ad' => 'required',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {

            $ads = ads::find($id);
            $ads->ad = Input::get('ad');
            //$ads->description = Input::get('description');


            // $blog->date = strtotime(date('Y-m-d h:i:s'));

            $ads->save();

            return Redirect::route('admin.ads.index')->with('message', 'Advertisement updated successfully');
        }
    }

    public function changeStatus($id) {

        $dt = Ads::find($id);
        $status = $dt->status;

        if ($status == 0) {
            $dt->status = '1';
        } else {
            $dt->status = '0';
        }
        $dt->save();

        return Redirect::route('admin.ads.index')->with('message', 'Status Changed Successfully');
    }    
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $content = content::find($id);
        if (file_exists(public_path('products/crop/ourbus/' . $content->image))) {
            unlink(public_path('products/crop/blogs/' . $content->image));
        }
        if (file_exists(public_path('products/thumb/ourbus/' . $content->image))) {
            unlink(public_path('products/thumb/blogs/' . $content->image));
        }
        if (file_exists(public_path('products/main/ourbus/' . $content->image))) {
            unlink(public_path('products/main/blogs/' . $content->image));
        }
        $content->delete();
        // redirect
        return Redirect::route('admin.contents.index')->with('message', 'Content deleted successfully');
    }

}
