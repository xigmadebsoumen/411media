<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Photos;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;
use DB;
class PhotosController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Photos::all();
        return View::make('admin.photos.index')->with(compact('photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$album = DB::table('album')->lists('album_name','album_name');
        return View::make('admin.photos.create')->with(compact('album'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = Input::all();
        
        $rules = array( 'image'=>'required' );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
        	if($data['album_type'] == 'new_album'){
	        	$album_name = $data['name'];
                DB::table('album')->insert([
                                'album_name'=>$album_name,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                ]);
	        } else {
	        	$album_name = $data['album_name'];
	        }
	        // print_r($album_name); return;
            $photos = new Photos;
            $photos->album_name  =  $album_name;

            if(Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/photos');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/photos');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/photos', $image);
                $photos->image = $image;                
            }            
            
            $photos->save();

            return Redirect::route('admin.gallery.photos.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $photos = Photos::find($id);
        $album = DB::table('album')->lists('album_name','album_name');
        return View::make('admin.photos.edit')->with(compact('photos','album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $data = Input::all();
        
        $rules = array( 'image'=>'required' );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
        	
	        // print_r($album_name); return;
            $photos = Photos::find($id);
            $photos->album_name  =  $data['album_name'];

            if(Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/photos');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/photos');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/photos', $image);
                $photos->image = $image;                
            }
            
            $photos->save();

            return Redirect::route('admin.gallery.photos.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photos = Photos::find($id);
        if (file_exists(public_path('products/crop/photoss/'.$photos->image))) {
            unlink(public_path('products/crop/photoss/'.$photos->image));
        }
        if (file_exists(public_path('products/thumb/photoss/'.$photos->image))) {
            unlink(public_path('products/thumb/photoss/'.$photos->image));
        }
        if (file_exists(public_path('products/main/photoss/'.$photos->image))) {
            unlink(public_path('products/main/photoss/'.$photos->image));
        }
        $photos->delete();
        // redirect
        return Redirect::route('admin.gallery.photos.index');
    }
}
