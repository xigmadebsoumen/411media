<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AllBanner;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;
class AllBannerController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_banners = AllBanner::all();
        return View::make('admin.all_banner.index')->with(compact('all_banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $all_banners = AllBanner::find($id);
        return View::make('admin.all_banner.edit')->with(compact('all_banners'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $data = Input::all();
        $banner = AllBanner::find($id);
        
        if(Input::hasFile('image')) {

            if (file_exists(public_path('products/crop/all_banner/'.$banner->image))) {
                unlink(public_path('products/crop/all_banner/'.$banner->image));
            }
            if (file_exists(public_path('products/thumb/all_banner/'.$banner->image))) {
                unlink(public_path('products/thumb/all_banner/'.$banner->image));
            }
            if (file_exists(public_path('products/main/all_banner/'.$banner->image))) {
                unlink(public_path('products/main/all_banner/'.$banner->image));
            }

            $file = Input::file('image');
            $imageName=$file->getClientOriginalName();
            $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
            $image= time().".".$file_ext;
            $destinationCropPath = public_path('/products/crop/all_banner');
            $img = Image::make($file->getRealPath())->resize(300, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationCropPath.'/'.$image);

            $destinationThumbPath = public_path('/products/thumb/all_banner');
            $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationThumbPath.'/'.$image);

            $upload_image = $file->move('products/main/all_banner', $image);
            $banner->image = $image;                
        }

        $banner->save();

        return Redirect::route('admin.settings.all-banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = AllBanner::find($id);
        if (file_exists(public_path('products/crop/banner/'.$banner->image))) {
            unlink(public_path('products/crop/banner/'.$banner->image));
        }
        if (file_exists(public_path('products/thumb/banner/'.$banner->image))) {
            unlink(public_path('products/thumb/banner/'.$banner->image));
        }
        if (file_exists(public_path('products/main/banner/'.$banner->image))) {
            unlink(public_path('products/main/banner/'.$banner->image));
        }
        $banner->delete();
        // redirect
        return Redirect::route('admin.settings.all-banner.index');
    }
}
