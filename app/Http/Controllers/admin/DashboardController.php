<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use View;
use Input;


class DashboardController extends Controller
{
	function __construct(){
       if (Auth::check()) {
       		view()->share('user',Auth::user());
		} else {
			//Redirect::to('admin/login')->send();
                        Redirect::to('admin/login')->send();
		}
   	}

    public function index() {
        
        //$u = Auth::user();
        //echo "<pre>"; print_r($u); exit;
        
		return View::make('admin.index');
	}
}
