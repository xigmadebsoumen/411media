<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;
use DB;
class OrderController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = DB::table('order')
            ->join('online_store', 'online_store.id', '=', 'order.product_id')
            ->select('order.id as order_id','order.product_id','order.user_name',
                'order.user_email','order.status','order.id','online_store.*')
            ->get();
        // echo "<pre>";
        // print_r($orders); return;
        return View::make('admin.order.index')->with(compact('orders'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $orders = Order::find($id);
        $orders = DB::table('order')
            ->join('online_store', 'online_store.id', '=', 'order.product_id')
            ->select('order.id as order_id','order.*','online_store.*')
            ->where('order.id','=',$id)
            ->get();
        // echo "<pre>";
        // print_r($orders); return;
        return View::make('admin.order.view')->with(compact('orders'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orders = Order::find($id);
        $orders->delete();
        // redirect
        return Redirect::route('admin.order.index');
    }
}
