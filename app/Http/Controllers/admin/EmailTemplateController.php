<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EmailTemplate;
use Validator;
use Redirect;
use View;
use Input;
use Image;
use Auth;
class EmailTemplateController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = EmailTemplate::all();
        return View::make('admin.email_template.index')->with(compact('templates'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $templates = EmailTemplate::find($id);
        return View::make('admin.email_template.edit')->with(compact('templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $data = Input::all();
        $rules = array('subject' => 'required',
                        'description'=>'required'
                        ); 
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            $templates = EmailTemplate::find($id);
            $templates->subject = $data['subject'];
            $templates->description = $data['description'];

            $templates->save();
            return Redirect::route('admin.settings.email-template.index');
        }  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $email = EmailTemplate::find($id);
        
        $email->delete();
        // redirect
        return Redirect::route('admin.settings.email-template.index');
    }
}
