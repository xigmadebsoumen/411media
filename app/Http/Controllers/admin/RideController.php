<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ride;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;

class RideController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
     
        $ride = ride::all();
        return View::make('admin.ride.index')->with(compact('ride'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.ride.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
       // echo"<pre>";
        //print_r($_POST);exit;
        $data = Input::all();
        $rules = array( 'title' => 'required',
                        'description' => 'required',
                        
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            $blog = new ride;
            $blog->title  =  Input::get('title');
            $blog->description  =  Input::get('description');
            $blog->date = strtotime(date('Y-m-d h:i:s'));
            $blog->meatkey  =  Input::get('meatkey');
            $blog->metatitle  =  Input::get('metatitle');
            $blog->metades = strtotime(date('metades'));




            if(Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/ride');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/blog');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/ride', $image);
                $blog->image = $image;                
            }
            
            $blog->save();

            return Redirect::route('admin.ride.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $ride = ride::find($id);
        return View::make('admin.ride.edit')->with(compact('ride'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $data = Input::all();
        $rules = array( 'title' => 'required',
                        'description' => 'required',
                       
                        );  
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            
            $ride = ride::find($id);
            $ride->title  =  Input::get('title');
            $ride->description  =  Input::get('description');
            $ride->save();

            return Redirect::route('admin.ride.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = ride::find($id);
        if (file_exists(public_path('products/crop/ride/'.$blog->image))) {
            unlink(public_path('products/crop/blogs/'.$blog->image));
        }
        if (file_exists(public_path('products/thumb/ride/'.$blog->image))) {
            unlink(public_path('products/thumb/blogs/'.$blog->image));
        }
        if (file_exists(public_path('products/main/ride/'.$blog->image))) {
            unlink(public_path('products/main/blogs/'.$blog->image));
        }
        $blog->delete();
        // redirect
        return Redirect::route('admin.ride.index');
    }
}
