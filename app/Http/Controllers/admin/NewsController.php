<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\News;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;

class NewsController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newss = News::all();
        return View::make('admin.news.index')->with(compact('newss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = Input::all();
        $rules = array( 'title' => 'required',
                        'description' => 'required',
                        'image'=>'required'
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            $news = new News;
            $news->title  =  Input::get('title');
            $news->description  =  Input::get('description');
            $news->date = strtotime(date('Y-m-d h:i:s'));

            if(Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/news');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/news');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/news', $image);
                $news->image = $image;                
            }
            
            $news->save();

            return Redirect::route('admin.news.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $newss = News::find($id);
        return View::make('admin.news.edit')->with(compact('newss'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $data = Input::all();
        $rules = array( 'title' => 'required',
                        'description' => 'required',
                        'image'=>'required'
                        );  
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            
            $news = News::find($id);
            $news->title  =  Input::get('title');
            $news->description  =  Input::get('description');
            $news->date = strtotime(date('Y-m-d h:i:s'));

            if(Input::hasFile('image')) {
                if (file_exists(public_path('products/crop/news/'.$news->image))) {
                    unlink(public_path('products/crop/news/'.$news->image));
                }
                if (file_exists(public_path('products/thumb/news/'.$news->image))) {
                    unlink(public_path('products/thumb/news/'.$news->image));
                }
                if (file_exists(public_path('products/main/news/'.$news->image))) {
                    unlink(public_path('products/main/news/'.$news->image));
                }

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/news');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/news');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/news', $image);
                $news->image = $image;                
            }
            
            $news->save();

            return Redirect::route('admin.news.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        if (file_exists(public_path('products/crop/news/'.$news->image))) {
            unlink(public_path('products/crop/news/'.$news->image));
        }
        if (file_exists(public_path('products/thumb/news/'.$news->image))) {
            unlink(public_path('products/thumb/news/'.$news->image));
        }
        if (file_exists(public_path('products/main/news/'.$news->image))) {
            unlink(public_path('products/main/news/'.$news->image));
        }
        $news->delete();
        // redirect
        return Redirect::route('admin.news.index');
    }
}
