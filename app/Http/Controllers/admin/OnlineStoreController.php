<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\OnlineStore;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;

class OnlineStoreController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = OnlineStore::all();
        return View::make('admin.stores.index')->with(compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = Input::all();
        $rules = array( 'album_name' => 'required',
                        'music_designed' => 'required',
                        'album_launch_by' => 'required',
                        'album_launch_date' => 'required',
                        'songs_list' => 'required',
                        'price' => 'required',
                        'album_cover_image' => 'required',
                        'songs'=>'required',
                        'no_of_songs' => 'required',
                        'new_till_date' => 'required',
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            $stores = new OnlineStore;
            $stores->album_name  =  Input::get('album_name');
            $stores->music_designed  =  Input::get('music_designed');
            $stores->album_launch_by  =  Input::get('album_launch_by');
            $stores->album_launch_date = strtotime(Input::get('album_launch_date'));
            $stores->songs_list  =  Input::get('songs_list');
            $stores->price  =  Input::get('price');
            $stores->language  =  Input::get('language');
            $stores->no_of_songs  =  Input::get('no_of_songs');
            $stores->new_till_date  =  strtotime(Input::get('new_till_date'));
            if (Input::get('out_of_stock')) {
            	$status = Input::get('out_of_stock');
            } else {
            	$status = 'no';
            }
            $stores->out_of_stock  =  $status;

            if(Input::hasFile('album_cover_image')) {

                $file = Input::file('album_cover_image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/stores');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/stores');
                $img = Image::make($file->getRealPath())->resize(360, 410, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/stores', $image);
                $stores->album_cover_image = $image;                
            }

            if (Input::hasFile('songs')) {
            	$mp3file = Input::file('songs');
            	$mp3Name=$mp3file->getClientOriginalName();
            	// $file_ext = pathinfo($mp3Name, PATHINFO_EXTENSION);
             //    $mp3File= time().".".$file_ext;
                
                $upload_mp3 = $mp3file->move('products/main/audio', $mp3Name);
                $stores->songs = $mp3Name;                
            }
            $stores->save();

            return Redirect::route('admin.online-store.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stores = OnlineStore::find($id);
        return View::make('admin.stores.edit')->with(compact('stores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $data = Input::all();
        $rules = array( 'album_name' => 'required',
                        'music_designed' => 'required',
                        'album_launch_by' => 'required',
                        'album_launch_date' => 'required',
                        'songs_list' => 'required',
                        'price' => 'required',
                        'album_cover_image' => 'required',
                        'songs'=>'required',
                        'no_of_songs' => 'required',
                        'new_till_date' => 'required',
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            $stores = OnlineStore::find($id);
            $stores->album_name  =  Input::get('album_name');
            $stores->music_designed  =  Input::get('music_designed');
            $stores->album_launch_by  =  Input::get('album_launch_by');
            $stores->album_launch_date = strtotime(Input::get('album_launch_date'));
            $stores->songs_list  =  Input::get('songs_list');
            $stores->price  =  Input::get('price');
            $stores->language  =  Input::get('language');
            $stores->no_of_songs  =  Input::get('no_of_songs');
            $stores->new_till_date  =  strtotime(Input::get('new_till_date'));
            if (Input::get('out_of_stock')) {
            	$status = Input::get('out_of_stock');
            } else {
            	$status = 'no';
            }
            $stores->out_of_stock  =  $status;

            if(Input::hasFile('album_cover_image')) {

                $file = Input::file('album_cover_image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/stores');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/stores');
                $img = Image::make($file->getRealPath())->resize(360, 410, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/stores', $image);
                $stores->album_cover_image = $image;                
            }

            if (Input::hasFile('songs')) {
            	$mp3file = Input::file('songs');
            	$mp3Name=$mp3file->getClientOriginalName();
                
                $upload_mp3 = $mp3file->move('products/main/audio', $mp3Name);
                $stores->songs = $mp3Name;                
            }
            $stores->save();

            return Redirect::route('admin.online-store.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stores = OnlineStore::find($id);
        if (file_exists(public_path('products/crop/stores/'.$stores->album_cover_image))) {
            unlink(public_path('products/crop/stores/'.$stores->album_cover_image));
        }
        if (file_exists(public_path('products/thumb/stores/'.$stores->album_cover_image))) {
            unlink(public_path('products/thumb/stores/'.$stores->album_cover_image));
        }
        if (file_exists(public_path('products/main/stores/'.$stores->album_cover_image))) {
            unlink(public_path('products/main/stores/'.$stores->album_cover_image));
        }
        if (file_exists(public_path('products/main/audio/'.$stores->songs))) {
            unlink(public_path('products/main/audio/'.$stores->songs));
        }
        $stores->delete();
        return Redirect::route('admin.online-store.index');
    }
}
