<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SiteBanner;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;

class SiteBannerController extends Controller {

    function __construct() {
        if (Auth::check()) {
            view()->share('user', Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $banners = SiteBanner::all();
        return View::make('admin.banner.index')->with(compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return View::make('admin.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store() {
        $data = Input::all();
        $rules = array('image' => 'required');
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $banner = new SiteBanner;
            $banner->name = Input::get('name');

            if (Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName = $file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image = time() . "." . $file_ext;
                $destinationCropPath = public_path('/products/crop/banner');
                $img = Image::make($file->getRealPath())->resize(300, 200, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationCropPath . '/' . $image);

                $destinationThumbPath = public_path('/products/thumb/banner');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationThumbPath . '/' . $image);

                $upload_image = $file->move('products/main/banner', $image);
                $banner->image = $image;
            }

            $banner->save();

            return Redirect::route('admin.settings.site-banner.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $banners = SiteBanner::find($id);
        return View::make('admin.banner.edit')->with(compact('banners'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id) {
        $data = Input::all();
        $banner = SiteBanner::find($id);
        $banner->name = Input::get('name');

        if (Input::hasFile('image')) {

            if (file_exists(public_path('products/crop/banner/' . $banner->image))) {
                unlink(public_path('products/crop/banner/' . $banner->image));
            }
            if (file_exists(public_path('products/thumb/banner/' . $banner->image))) {
                unlink(public_path('products/thumb/banner/' . $banner->image));
            }
            if (file_exists(public_path('products/main/banner/' . $banner->image))) {
                unlink(public_path('products/main/banner/' . $banner->image));
            }

            $file = Input::file('image');
            $imageName = $file->getClientOriginalName();
            $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
            $image = time() . "." . $file_ext;
            $destinationCropPath = public_path('/products/crop/banner');
            $img = Image::make($file->getRealPath())->resize(300, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationCropPath . '/' . $image);

            $destinationThumbPath = public_path('/products/thumb/banner');
            $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationThumbPath . '/' . $image);

            $upload_image = $file->move('products/main/banner', $image);
            $banner->image = $image;
        }

        $banner->save();

        return Redirect::route('admin.settings.site-banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $banner = SiteBanner::find($id);
        if (file_exists(public_path('products/crop/banner/' . $banner->image))) {
            unlink(public_path('products/crop/banner/' . $banner->image));
        }
        if (file_exists(public_path('products/thumb/banner/' . $banner->image))) {
            unlink(public_path('products/thumb/banner/' . $banner->image));
        }
        if (file_exists(public_path('products/main/banner/' . $banner->image))) {
            unlink(public_path('products/main/banner/' . $banner->image));
        }
        $banner->delete();
        // redirect
        return Redirect::route('admin.settings.site-banner.index');
    }

}
