<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Content;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;

class ContentController extends Controller {

    function __construct() {
        if (Auth::check()) {
            view()->share('user', Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $content = content::all();
        //echo "<pre>"; print_r($content); exit;


        return View::make('admin.content.index')->with(compact('content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return View::make('admin.content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store() {
        //echo"<pre>"; print_r($_POST);exit;
        $data = Input::all();
        $rules = array('title' => 'required',
            'description' => 'required',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $content = new content;
            $content->title = Input::get('title');
            $content->description = Input::get('description');
//            $blog->date = strtotime(date('Y-m-d h:i:s'));
//            $blog->meatkey = Input::get('meatkey');
//            $blog->metatitle = Input::get('metatitle');
//            $blog->metades = strtotime(date('metades'));




//            if (Input::hasFile('image')) {
//
//                $file = Input::file('image');
//                $imageName = $file->getClientOriginalName();
//                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
//                $image = time() . "." . $file_ext;
//                $destinationCropPath = public_path('/products/crop/ourbus');
//                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
//                            $constraint->aspectRatio();
//                        })->save($destinationCropPath . '/' . $image);
//
//                $destinationThumbPath = public_path('/products/thumb/blog');
//                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
//                            $constraint->aspectRatio();
//                        })->save($destinationThumbPath . '/' . $image);
//
//                $upload_image = $file->move('products/main/ourbus', $image);
//                $blog->image = $image;
//            }

            $content->save();

            return Redirect::route('admin.content.index')->with('message', 'Content added successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $content = content::find($id);
        return View::make('admin.content.edit')->with(compact('content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id) {

        $data = Input::all();
        $rules = array('title' => 'required',
            'description' => 'required',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {

            $content = content::find($id);
            $content->title = Input::get('title');
            $content->description = Input::get('description');


            // $blog->date = strtotime(date('Y-m-d h:i:s'));

            $content->save();

            return Redirect::route('admin.contents.index')->with('message', 'Content updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $content = content::find($id);
        if (file_exists(public_path('products/crop/ourbus/' . $content->image))) {
            unlink(public_path('products/crop/blogs/' . $content->image));
        }
        if (file_exists(public_path('products/thumb/ourbus/' . $content->image))) {
            unlink(public_path('products/thumb/blogs/' . $content->image));
        }
        if (file_exists(public_path('products/main/ourbus/' . $content->image))) {
            unlink(public_path('products/main/blogs/' . $content->image));
        }
        $content->delete();
        // redirect
        return Redirect::route('admin.contents.index')->with('message', 'Content deleted successfully');
    }

}
