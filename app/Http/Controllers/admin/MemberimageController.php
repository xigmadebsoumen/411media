<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Memberimage;
use View;
use Input;
use Validator;
use Redirect;
use Image;
use Auth;

class MemberimageController extends Controller
{
    function __construct(){
       if (Auth::check()) {
            view()->share('user',Auth::user());
        } else {
            Redirect::to('admin/login')->send();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       

        $memberimage = Memberimage::all();
        return View::make('admin.memberimage.index')->with(compact('memberimage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return View::make('admin.memberimage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = Input::all();
        $rules = array( 'name' => 'required',
                        'designation' => 'required',
                        'image'=>'required'
                        );   
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            $member = new Memberimage;
            $member->name  =  Input::get('name');
            $member->designation  =  Input::get('designation');
            //$member->date = strtotime(date('Y-m-d h:i:s'));

            if(Input::hasFile('image')) {

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/members');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/members');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/members', $image);
                $member->image = $image;                
            }
            
            $member->save();

              return Redirect::route('admin.member.photos.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)


    {

        $memberimage = Memberimage::find($id);
        return View::make('admin.memberimage.edit')->with(compact('memberimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $data = Input::all();
        $rules = array('name' => 'required',
                        'designation' => 'required',
                        'image'=>'required'
                        );  
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
              return Redirect::back()->withErrors($validator)->withInput();
        }else {
            $member = Memberimage::find($id);
            $member->name  =  Input::get('name');
            $member->designation  =  Input::get('designation');

        if(Input::hasFile('image')) {
                if (file_exists(public_path('products/crop/members/'.$member->image))) {
                    unlink(public_path('products/crop/members/'.$member->image));
                }
                if (file_exists(public_path('products/thumb/members/'.$member->image))) {
                    unlink(public_path('products/thumb/members/'.$member->image));
                }
                if (file_exists(public_path('products/main/members/'.$member->image))) {
                    unlink(public_path('products/main/members/'.$member->image));
                }

                $file = Input::file('image');
                $imageName=$file->getClientOriginalName();
                $file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
                $image= time().".".$file_ext;
                $destinationCropPath = public_path('/products/crop/members');
                $img = Image::make($file->getRealPath())->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationCropPath.'/'.$image);

                $destinationThumbPath = public_path('/products/thumb/members');
                $img = Image::make($file->getRealPath())->resize(360, 280, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationThumbPath.'/'.$image);

                $upload_image = $file->move('products/main/members', $image);
                $member->image = $image;                
            }
            
            $member->save();

            return Redirect::route('admin.member.photos.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $memberimage = Memberimage::find($id);
        if (file_exists(public_path('products/crop/members/'.$memberimage->image))) {
            unlink(public_path('products/crop/members/'.$memberimage->image));
        }
        if (file_exists(public_path('products/thumb/members/'.$memberimage->image))) {
            unlink(public_path('products/thumb/members/'.$memberimage->image));
        }
        if (file_exists(public_path('products/main/members/'.$memberimage->image))) {
            unlink(public_path('products/main/members/'.$memberimage->image));
        }
        $memberimage->delete();
        // redirect
        return Redirect::route('admin.member.photos.index');
    }
}
