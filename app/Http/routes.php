<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
 
Route::group(array('prefix' => 'admin', 'before' => 'admin', 'namespace' => 'admin'), function() {
    Route::get('/', array('as' => 'admin.auth.index', 'uses' => 'AuthController@getLogin'));
    Route::get('login', array('as' => 'admin.auth.index', 'uses' => 'AuthController@getLogin'));
    Route::post('login', array('as' => 'admin.auth.login', 'uses' => 'AuthController@postLogin'));
    Route::get('logout', array('as' => 'admin.auth.logout', 'uses' => 'AuthController@logout'));
});


Route::group(array('prefix' => 'admin', 'namespace' => 'admin', 'before' => 'admin'), function() {
    Route::get('dashboard', array('as' => 'admin.dashboard', 'uses' => 'DashboardController@index'));
    Route::get('my-profile', array('as' => 'admin.user.myprofile', 'uses' => 'UserController@myprofile'));
    Route::put('my-profile/{id}', array('as' => 'admin.user.updateprofile', 'uses' => 'UserController@updateprofile'));
    Route::resource('users', 'UserController');
    Route::get('users/change-status/{id}', 'UserController@changeStatus');

    //Route::get('banner-content', array('as' => 'admin.bannercontent.index', 'uses' => 'BannercontentController@index'));
    //Route::put('banner-content/{id}', array('as' => 'admin.bannercontent.update', 'uses' => 'BannercontentController@update'));
    //Route::get('our-route', array('as' => 'admin.route.index', 'uses' => 'RouteController@index'));
    //Route::put('our-route/{id}', array('as' => 'admin.route.update', 'uses' => 'RouteController@update'));
    //Route::get('banner-content', array('as' => 'admin.bannercontent.index', 'uses' => 'BannercontentController@index'));
    //Route::put('banner-content/{id}', array('as' => 'admin.bannercontent.update', 'uses' => 'BannercontentController@update'));
    //Route::get('contents/{type}', 'ContentController@index');
    //Route::get('contents/', 'ContentController@index');
    //Route::get('contents/create', 'ContentController@create');
    //Route::post('contents/store', 'ContentController@store');

    Route::resource('contents', 'ContentController');
    Route::resource('ads', 'AdsController');
    Route::get('ads/change-status/{id}', 'AdsController@changeStatus');
    //Route::resource('news','NewsController');

    Route::get('ourbus/{type}', 'OurbusController@index');
    Route::get('ride/{type}', 'RideController@index');
    Route::get('route/{type}', 'RouteController@index');
    Route::get('rideturn/{type}', 'RideturnController@index');
    //Route::get('bannercontent/{type}', 'BannercontentController@index');
    //Route::put('ourbus/{type}/{id}', array('as' => 'admin.ourbus.update', 'uses' => 'OurbusController@update'));
    //Route::get('contents/{type}', 'ContentController@index');
    //Route::put('contents/{type}/{id}', array('as' => 'admin.contents.update', 'uses' => 'ContentController@update'));
    Route::resource('legal_topic', 'LegalTopicController', array('except' => array('show')));
    Route::get('settings/site-setting', 'SettingController@index');
    Route::put('settings/site-setting/{id}', array('as' => 'admin.settings.update', 'uses' => 'SettingController@update'));
    Route::resource('settings/email-template', 'EmailTemplateController');
    Route::resource('settings/site-banner', 'SiteBannerController');
    Route::get('settings/site-banner/change-status/{id}', 'SiteBannerController@changeStatus');
    Route::resource('settings/all-banner', 'AllBannerController');
    Route::resource('blog', 'BlogController');
    Route::resource('route', 'RouteController');
    Route::resource('ourbus', 'OurbusController');
    Route::resource('rideturn', 'RideturnController');
    Route::resource('ride', 'RideController');
    // Route::resource('bannercontent', 'BannercontentController');
    Route::resource('member/photos', 'MemberimageController');
    Route::resource('member', 'MemberimageController');
    Route::resource('gallery/photos', 'PhotosController');
    Route::resource('gallery/videos', 'VideoController');
    Route::resource('release', 'ReleaseController');
    Route::resource('online-store', 'OnlineStoreController');
    Route::resource('query', 'QueryController');
    Route::resource('order', 'OrderController');
});


Route::group(array('before' => 'site', 'namespace' => 'site'), function() {
    
    Route::get('/', array('as' => 'site', 'uses' => 'HomeController@index'));
    Route::post('frontlogin', array('as' => 'auth.frontlogin', 'uses' => 'AuthController@frontlogin'));
    //Route::post('frontlogin', array('as' => 'site.auth.login', 'uses' => 'AuthController@postLogin'));
    Route::post('login', array('as' => 'site.auth.login', 'uses' => 'AuthController@postLogin'));
    Route::post('checkpname', array('as' => 'auth.checkpname', 'uses' => 'AuthController@checkpname'));
    Route::post('checkfbuser', array('as' => 'auth.checkfbuser', 'uses' => 'AuthController@checkfbuser'));
    Route::post('frfblogin', array('as' => 'site.auth.frfblogin', 'uses' => 'AuthController@frfblogin'));

    
    Route::post('checkloginfbuser', array('as' => 'auth.checkloginfbuser', 'uses' => 'AuthController@checkloginfbuser'));
    Route::post('fblogin', array('as' => 'site.auth.fblogin', 'uses' => 'AuthController@fblogin'));
    
    Route::get('getpostview/{id}', array('as' => 'auth.getpostview', 'uses' => 'AuthController@getpostview'));
    
    Route::post('usersignup', array('as' => 'auth.usersignup', 'uses' => 'AuthController@usersignup'));
    Route::post('forgotpassword', array('as' => 'auth.forgotpassword', 'uses' => 'AuthController@forgotpassword'));
    Route::get('changepassword/{id}', array('as' => 'site.users.changepassword', 'uses' => 'AuthController@changepassword'));
    Route::post('updatepassword', array('as' => 'site.user.updatepassword', 'uses' => 'AuthController@updatepassword'));

    Route::get('mysettings', array('as' => 'site.user.mysettings', 'uses' => 'UserController@mysettings'));
    
    
    Route::get('blockuser', array('as' => 'site.page.blockuser', 'uses' => 'PageController@blockuser'));
    Route::get('copyright', array('as' => 'site.page.copyright', 'uses' => 'PageController@copyright'));
    Route::get('notifications', array('as' => 'site.page.notifications', 'uses' => 'PageController@notifications'));
    Route::get('privacy', array('as' => 'site.page.privacy', 'uses' => 'PageController@privacy'));
    Route::get('terms', array('as' => 'site.page.terms', 'uses' => 'PageController@terms'));
    
    
    Route::get('my-profile', array('as' => 'site.user.myprofile', 'uses' => 'UserController@myprofile'));
    Route::put('updateprofile/{id}', array('as' => 'site.user.updateprofile', 'uses' => 'UserController@updateprofile'));    
    Route::get('profile', array('as' => 'site.user.profile', 'uses' => 'UserController@profile'));
    
    Route::post('follownow', array('as' => 'auth.follownow', 'uses' => 'AuthController@follownow'));
    Route::post('unfollownow', array('as' => 'auth.unfollownow', 'uses' => 'AuthController@unfollownow'));
    
    Route::post('sharenow', array('as' => 'post.sharenow', 'uses' => 'PostController@sharenow'));
    Route::post('likenow', array('as' => 'post.likenow', 'uses' => 'PostController@likenow'));
    Route::post('unlikenow', array('as' => 'post.unlikenow', 'uses' => 'PostController@unlikenow'));    
    Route::post('blockuser', array('as' => 'post.blockuser', 'uses' => 'PostController@blockuser'));
    Route::post('reportpost', array('as' => 'post.reportpost', 'uses' => 'PostController@reportpost'));
    Route::post('sharethisuser', array('as' => 'post.sharethisuser', 'uses' => 'PostController@sharethisuser'));
    
    
    
    
    Route::post('setnotification', array('as' => 'post.likenow', 'uses' => 'PostController@likenow'));
    Route::post('unsetnotification', array('as' => 'post.unlikenow', 'uses' => 'PostController@unlikenow')); 
    

    Route::get('delete/{id}', array('as' => 'post.delete', 'uses' => 'PostController@delete'));
    Route::get('report/{id}', array('as' => 'post.report', 'uses' => 'PostController@report'));
    Route::get('turnoffcomments/{id}', array('as' => 'post.turnoffcomments', 'uses' => 'PostController@turnoffcomments'));
    Route::get('turnoncomments/{id}', array('as' => 'post.delete', 'uses' => 'PostController@turnoncomments'));
    Route::get('turnoffshare/{id}', array('as' => 'post.turnoffshare', 'uses' => 'PostController@turnoffshare'));
    Route::get('turnonshare/{id}', array('as' => 'post.turnonshare', 'uses' => 'PostController@turnonshare'));
    
    Route::get('sharelist/{id}', array('as' => 'post.sharelist', 'uses' => 'PostController@sharelist'));
    Route::get('commentlist/{id}', array('as' => 'post.commentlist', 'uses' => 'PostController@commentlist'));
    Route::get('likelist/{id}', array('as' => 'post.likelist', 'uses' => 'PostController@likelist'));
        
    Route::get('comment/{id}', array('as' => 'post.comment', 'uses' => 'PostController@comment'));
    Route::post('savecomment/{id}', array('as' => 'post.savecomment', 'uses' => 'PostController@savecomment'));
    
    Route::get('messageuser/{id}', array('as' => 'post.messageuser', 'uses' => 'PostController@messageuser'));
    Route::post('savemessageuser', array('as' => 'site.post.savemessageuser', 'uses' => 'PostController@savemessageuser'));    
    
    Route::post('searchfriends', array('as' => 'auth.searchfriends', 'uses' => 'AuthController@searchfriends'));
    Route::post('searchfriends1', array('as' => 'auth.searchfriends1', 'uses' => 'AuthController@searchfriends1'));
    Route::get('searchprofile/{id}', array('as' => 'site.auth.searchprofile', 'uses' => 'AuthController@searchprofile'));
    
    Route::get('logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
    Route::get('timeline', array('as' => 'site.user.timeline', 'uses' => 'UserController@timeline'));
    
    
    Route::get('addpost', array('as' => 'site.post.addpost', 'uses' => 'PostController@addpost'));
    Route::post('savepost', array('as' => 'site.post.savepost', 'uses' => 'PostController@savepost'));
    Route::get('showpost/{id}', array('as' => 'site.post.showpost', 'uses' => 'PostController@showpost'));
    
    Route::get('addblog', array('as' => 'site.post.addblog', 'uses' => 'PostController@addblog'));
    Route::post('saveblog', array('as' => 'site.post.saveblog', 'uses' => 'PostController@saveblog'));
    Route::get('showblog/{id}', array('as' => 'site.post.showblog', 'uses' => 'PostController@showblog'));
    
    Route::get('addlink', array('as' => 'site.post.addlink', 'uses' => 'PostController@addlink'));
    Route::post('savelink', array('as' => 'site.post.savelink', 'uses' => 'PostController@savelink'));
    Route::get('showlink/{id}', array('as' => 'site.post.showlink', 'uses' => 'PostController@showlink'));
    
    
    Route::get('addphoto', array('as' => 'site.post.addphoto', 'uses' => 'PostController@addphoto'));
    Route::post('savephoto', array('as' => 'post.savephoto', 'uses' => 'PostController@savephoto'));
    Route::get('showphoto/{id}', array('as' => 'site.post.showphoto', 'uses' => 'PostController@showphoto'));
    
    
    Route::get('addvideo', array('as' => 'site.post.addvideo', 'uses' => 'PostController@addvideo'));
    Route::post('savevideo', array('as' => 'site.post.savevideo', 'uses' => 'PostController@savevideo'));
    Route::get('showvideo/{id}', array('as' => 'site.post.showvideo', 'uses' => 'PostController@showvideo'));

    Route::get('addtwitterlink', array('as' => 'site.post.addtwitterlink', 'uses' => 'PostController@addtwitterlink'));
    Route::post('savetwitterlink', array('as' => 'site.post.savetwitterlink', 'uses' => 'PostController@savetwitterlink'));
    Route::get('showtwitterlink/{id}', array('as' => 'site.post.showtwitterlink', 'uses' => 'PostController@showtwitterlink'));
    
    Route::get('addinstagramlink', array('as' => 'site.post.addinstagramlink', 'uses' => 'PostController@addinstagramlink'));
    Route::post('saveinstagramlink', array('as' => 'site.post.saveinstagramlink', 'uses' => 'PostController@saveinstagramlink'));
    Route::get('showinstagramlink/{id}', array('as' => 'site.post.showinstagramlink', 'uses' => 'PostController@showinstagramlink'));    
    
    Route::get('addmultipleimg', array('as' => 'site.post.addslide', 'uses' => 'PostController@addslide'));
    Route::post('saveslide', array('as' => 'site.post.saveslide', 'uses' => 'PostController@saveslide'));
    Route::get('showalbum/{id}', array('as' => 'site.post.showalbum', 'uses' => 'PostController@showalbum'));

    Route::post('savecomment', array('as' => 'site.post.savecomment', 'uses' => 'PostController@savecomment'));


    //Route::post('addnewslike', array('as'=>'news.addnewslike','uses'=>'NewsController@addnewslike'));
    Route::post('ride-and-turn', array('as' => 'ride.turn', 'uses' => 'HomeController@ride_and_turn'));
    Route::get('farespasses', array('as' => 'site.farespasses', 'uses' => 'HomeController@farespasses'));
    Route::get('maintenance', array('as' => 'site.maintenance', 'uses' => 'HomeController@maintenance'));
    Route::get('modernization', array('as' => 'site.modernization', 'uses' => 'HomeController@modernization'));
    Route::get('equipment', array('as' => 'site.equipment', 'uses' => 'HomeController@equipment'));
    Route::get('prepurchase', array('as' => 'site.prepurchase', 'uses' => 'HomeController@prepurchase'));
    Route::get('consultation', array('as' => 'site.consultation', 'uses' => 'HomeController@consultation'));
    Route::get('conclusion', array('as' => 'site.conclusion', 'uses' => 'HomeController@conclusion'));
    Route::post('contact', array('as' => 'site.contact', 'uses' => 'HomeController@contact'));
    Route::post('query', array('as' => 'query.contact', 'uses' => 'HomeController@contact'));
    Route::get('artist-profile', array('as' => 'site.artist_profile', 'uses' => 'HomeController@aboutUs'));
    Route::get('news-&-events', array('as' => 'site.news_events', 'uses' => 'HomeController@news_events'));
    Route::get('news-&-events/{id}', array('as' => 'site.individual.news_events', 'uses' => 'HomeController@individual_news_events'));
    Route::get('press-release', array('as' => 'site.press_release', 'uses' => 'HomeController@press_release'));
    Route::get('contacts', array('as' => 'site.contacts', 'uses' => 'HomeController@contacts'));
    // Route::get('blogs', array('as' => 'site.blogs', 'uses' => 'HomeController@blogs'));
    // Route::get('blog/{id}', array('as' => 'site.individual.blog', 'uses' => 'HomeController@individual_blog'));
    Route::get('photo-gallery', array('as' => 'site.photos', 'uses' => 'HomeController@photos'));
    Route::get('video-gallery', array('as' => 'site.videos', 'uses' => 'HomeController@videos'));
    Route::get('buy-online', array('as' => 'site.buy', 'uses' => 'OnlineStoreController@buyOnline'));
    Route::get('{id}/online-form', array('as' => 'site.online_form', 'uses' => 'OnlineStoreController@online_form'));
    Route::post('buy-online', array('as' => 'order.save', 'uses' => 'OnlineStoreController@saveOrder'));
    Route::post('query', array('as' => 'query.save', 'uses' => 'HomeController@saveQuery'));
    Route::post('payment/response', array('as' => 'payment.response', 'uses' => 'OnlineStoreController@payment'));
    // Route::post('quote-send', array('as' => 'quote-send', 'uses' => 'QuotesController@sendMessage'));
    // Route::post('career-send', array('as' => 'career-send', 'uses' => 'CareerController@career'));
});
Route::post('payumoney/result', function() {
    return View::make('site.payumoney.success');
});
