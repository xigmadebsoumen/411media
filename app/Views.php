<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Views extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'views';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('user_id', 'viewer_id');
}
