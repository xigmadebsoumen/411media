<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSettings extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'settings';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('site_name', 'site_logo', 'slogan_text', 'copy_right',
								'facebook_url', 'twitter_url', 'google_url', 'linkedin_url',
								'youtube_url', 'address', 'address_lat', 'address_lang',
								'admin_ph_no', 'admin_email', 'site_phone_number', 'meta_description',
								'meta_keywords', 'site_title');
}
