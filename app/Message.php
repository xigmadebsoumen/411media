<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'message';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $fillable = array('name', 'description', 'date', 'image');
}
