<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Notify extends Authenticatable {

    protected $table = 'notifyme';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
