<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$path_image = public_path('products/crop/members');

		User::create([
			'username' => 'lopamudra.mitra',
			'name' => 'Lopamudra Mitra',
			'password' => Hash::make('password'),
			'image' => $faker->Image($path_image, $width = 200, $height = 200, null, false),
		]);
	}

}
