<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_store', function (Blueprint $table) {
            $table->increments('id');
            $table->string('album_name');
            $table->string('music_designed');
            $table->string('album_launch_by');
            $table->string('album_launch_date');
            $table->string('songs_list');
            $table->string('price');
            $table->string('album_cover_image');
            $table->string('songs');
            $table->string('language'); 
            $table->double('no_of_songs');
            $table->bigInteger('new_till_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('online_store');
    }
}
