<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_name');
            $table->string('site_logo');
            $table->string('slogan_text');

            $table->string('facebook_url');
            $table->string('twitter_url');
            $table->string('linkedin_url');
            $table->string('youtube_url');

            $table->string('office_address');
            $table->string('residencial_address');
            $table->string('address_lat');
            $table->string('address_lang');
            $table->string('admin_ph_no');
            $table->string('admin_email');
            
            $table->string('meta_description');
            $table->string('meta_keywords');
            $table->string('site_title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
