@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($content,array('route' => array('admin.contents.update',$content->id, $type),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body">
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        @if (session('message'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{ session('message') }}</strong>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Content:</strong> {!! ucfirst($type) !!}</h3>
                    </div>
                   
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Page Name</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('type',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Type of the member</span>
                                        {!! $errors->first('type','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Page Title</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('title',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Page Title</span>
                                        {!! $errors->first('title','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                   
                                <div class="form-group ">
                                    {!! Form::label('description','Page Content',array('class' => 'col-md-3 control-label'))  !!}
                                    <div class="col-lg-9">
                                        {!! Form::textarea('description',null,array('class'=>'form-control','placeholder'=>'Post Content here')) !!}
                                        {!! $errors->first('description','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div> 
                                </div> 

                                <div class='clearfix'></div>                                
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="reset" class="btn btn-default">Clear Form</button>                                    
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>            
            </div>
        </div>                    
        
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>              
{!! Form::close() !!}
</div>
@stop
