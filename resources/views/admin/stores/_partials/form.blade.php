<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    @if (session('message'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <strong>{{ session('message') }}</strong>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Products</strong> Form</h3>
                </div>
               
                <div class="panel-body">                                                                        
                    <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                                <label class="col-md-2 control-label">Album Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('album_name', null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Name of the album</span>
                                    {!! $errors->first('album_name','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Music Designed</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('music_designed',null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Music Designed By</span>
                                    {!! $errors->first('music_designed','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Album Launch</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('album_launch_by',null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Album Launch By</span>
                                    {!! $errors->first('album_launch_by','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group">                                        
                                <label class="col-md-2 control-label">Date of Album Launch</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        @if(isset($stores->album_launch_date))
                                        <?php $date = date('Y-m-d', $stores->album_launch_date); ?>
                                            {!! Form::text('album_launch_date',$date, array('class' => 'form-control datepicker'))  !!}
                                        @else
                                            {!! Form::text('album_launch_date',null, array('class' => 'form-control datepicker'))  !!}
                                        @endif
                                    </div>
                                    <span class="help-block">Click on input field to get calender</span>
                                    {!! $errors->first('album_launch_date','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Songs List</label>
                                <div class="col-md-9">
                                    {!! Form::text('songs_list',null, array('class' => 'tagsinput'))  !!}
                                    <span class="help-block">Add Songs name</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Album Price</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('price',null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Price of the album</span>
                                    {!! $errors->first('price','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('album_cover_image','Album Cover Image',array('class' => 'col-md-2 control-label')) !!}  
                                <div class="col-md-9">
                                    {!! Form::hidden('album_cover_image', null, array('class' => 'control-label')) !!}
                                    {!! Form::file('album_cover_image', null, array('class' => 'fileinput btn-primary')) !!}
                                    <span class="help-block">Input type JPG, JPEG, PNG</span>
                                    <br>
                                    {!! isset($stores->album_cover_image) ? Html::image('products/crop/stores/'.$stores->album_cover_image, $stores->album_name)  : ' '  !!}
                                    {!! $errors->first('album_cover_image','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                {!! Form::label('songs','Audio (Only one song)',array('class' => 'col-md-2 control-label')) !!}  
                                <div class="col-md-9">
                                    {!! Form::hidden('songs', null, array('class' => 'control-label')) !!}
                                    {!! Form::file('songs', null, array('class' => 'fileinput btn-primary')) !!}
                                    <span class="help-block">Input type mp3</span>
                                    <br>
                                    {!! Html::image('theme/admin/img/songs_admin.jpg') !!}
                                    {!! isset($stores->songs) ? $stores->songs : ' '  !!}
                                    {!! $errors->first('songs','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Language</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('language', null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Language of audio</span>
                                    {!! $errors->first('language','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Number of Songs</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('no_of_songs',null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Number of Songs</span>
                                    {!! $errors->first('no_of_songs','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group">                                        
                                <label class="col-md-2 control-label">New till date of album</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        @if(isset($stores->new_till_date))
                                        <?php $date = date('Y-m-d', $stores->new_till_date); ?>
                                            {!! Form::text('new_till_date',$date, array('class' => 'form-control datepicker'))  !!}
                                        @else
                                            {!! Form::text('new_till_date',null, array('class' => 'form-control datepicker'))  !!}
                                        @endif
                                    </div>
                                    <span class="help-block">Click on input field to get calender</span>
                                    {!! $errors->first('new_till_date','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="reset" class="btn btn-default">Clear Form</button>                                    
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>            
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER -->