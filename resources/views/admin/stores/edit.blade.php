@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($stores,array('route' => array('admin.online-store.update',$stores->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.stores._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
