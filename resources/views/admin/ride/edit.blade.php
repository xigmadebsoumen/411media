@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($ride,array('route' => array('admin.ride.update',$ride->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.ride._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop

