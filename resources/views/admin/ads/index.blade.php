
<?php //echo"hi";die;  ?>

@extends('_layouts.admin')
@section('content')             
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Advertisement</h3>
                <!--<a href="{{URL::to('admin/ads/create')}}" class="btn btn-default btn-rounded pull-right">Add Advertisement</a></li>-->
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Page</th>
                                <th>Position</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $c=1; foreach ($dt as $det){ ?>
                            <tr>
                                <td><?= $c++ ?></td>
                                <td>{!! $det->page !!}</td>
                                <td>{!! $det->position !!}</td>
                                <td>
                                    @if($det->status == 0)
                                        <p class="btn btn-danger btn-rounded">Inactive</p>
                                    @else
                                        <p class="btn btn-success btn-rounded">Active</p>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ URL::to('admin/ads/'.$det->id.'/edit') }}" class="btn btn-info btn-rounded"><i title="Edit" style="margin-right: 0;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    @if($det->status == 0)
                                        <a href="{{ URL::to('admin/ads/change-status/'.$det->id) }}" class="btn btn-warning btn-rounded"><i title="Change Status" style="margin-right: 0;" class="fa fa-unlock" aria-hidden="true"></i></a>
                                    @else
                                        <a href="{{ URL::to('admin/ads/change-status/'.$det->id) }}" class="btn btn-warning btn-rounded"><i title="Change Status" style="margin-right: 0;" class="fa fa-lock" aria-hidden="true"></i></a>
                                    @endif
                                    <!--<a class="btn btn-xs btn-danger" data-record-id="" data-toggle="modal" data-target="#confirm-delete{{$det->id}}">
                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                    </a>                                                    
                                    <div class="modal fade" id="confirm-delete{{$det->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" style="width: 32%;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>You are about to delete <b><i class="title"></i></b> record, this procedure is irreversible.</p>
                                                    <p>Do you want to proceed?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::open(array('route' => ['admin.ads.destroy',$det->id],'method'=>'delete')) !!}
                                                    {!! Form::hidden('id',$det->id) !!}
                                                    <button type="submit" class="btn btn-danger btn-ok" href="">Delete</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->
        </div>
    </div>                                
    
</div>
<!-- PAGE CONTENT WRAPPER -->                                
<!-- Modal Dialog for Delete -->
<div class="modal fade" id="formConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <!-- <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="frm_title">Delete</h4>
      </div>
      <div class="modal-body" id="frm_body">Are you sure, you want to delete this Topic ?</div>
      <div class="modal-footer">
        <button style='margin-left:10px;' type="button" class="btn btn-danger col-sm-2 pull-right" id="frm_submit">Delete</button>
        <button type="button" class="btn btn-primary col-sm-2 pull-right" data-dismiss="modal" id="frm_cancel">Cancel</button>-->
      </div>
    </div>
  </div>
</div>
<!-- Dialog show event handler for delete -->
<script type="text/javascript">
$(document).ready(function(){
  $('.formConfirm').on('click', function(e) {
        e.preventDefault();
        var el = $(this).parent();
        var title = el.attr('data-title');
        var msg = el.attr('data-message');
        var dataForm = el.attr('data-form');
        
        $('#formConfirm')
        .find('#frm_body').html(msg)
        .end().find('#frm_title').html(title)
        .end().modal('show');
        
        $('#formConfirm').find('#frm_submit').attr('data-form', dataForm);
  });
  $('#formConfirm').on('click', '#frm_submit', function(e) {
        var id = $(this).attr('data-form');
        $(id).submit();
  });
});
</script>
@stop