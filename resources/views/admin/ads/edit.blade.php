@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($ads,array('route' => array('admin.ads.update',$ads->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.ads._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop

