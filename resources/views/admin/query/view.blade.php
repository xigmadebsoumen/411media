@extends('_layouts.admin')
@section('content') 
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    @if (session('message'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <strong>{{ session('message') }}</strong>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Query</strong></h3>
                </div>
               
                <div class="panel-body">                                                                        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-9">                                            
                                    {!! $queries->name !!}
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                            <div class="form-group ">
                                <label class="col-md-2 control-label">Email</label>
                                <div class="col-md-9"> 
                                    {!! $queries->email !!}
                                </div>  
                            </div> 
                            <div class="clearfix"> </div>
                            <div class="form-group ">
                                <label class="col-md-2 control-label">Contact Number</label>
                                <div class="col-md-9"> 
                                    {!! $queries->phone_number !!}
                                </div>  
                            </div>
                            <div class="clearfix"> </div>
                            <div class="form-group ">
                                <label class="col-md-2 control-label">Message</label>
                                <div class="col-md-9"> 
                                    {!! $queries->message !!}
                                </div>  
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="{{URL::to('admin/query')}}" class="btn btn-primary pull-right">Back</a>
                </div>
            </div>            
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER -->
@stop