@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($ourbus,array('route' => array('admin.ourbus.update',$ourbus->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.ourbus._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop

