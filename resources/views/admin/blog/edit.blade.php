@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($blogs,array('route' => array('admin.blog.update',$blogs->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.blog._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
