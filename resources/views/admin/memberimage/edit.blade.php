@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($memberimage,array('route' => array('admin.member.photos.update',$memberimage->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.memberimage._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
