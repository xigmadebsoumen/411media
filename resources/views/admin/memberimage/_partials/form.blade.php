<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    @if (session('message'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <strong>{{ session('message') }}</strong>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Member</strong> Form</h3>
                </div>
               
                <div class="panel-body">                                                                        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('name',null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Nmae of the member</span>
                                    {!! $errors->first('name','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="col-md-2 control-label">Designation</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('designation',null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Designation of the member</span>
                                    {!! $errors->first('designation','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('image','Image',array('class' => 'col-md-2 control-label')) !!}  
                                <div class="col-md-9">
                                    {!! Form::hidden('image', null, array('class' => 'control-label')) !!}
                                    {!! Form::file('image', null, array('class' => 'fileinput btn-primary')) !!}
                                    <span class="help-block">Input type JPG, JPEG, PNG</span>
                                    <br>
                                    {!! isset($blogs->image) ? Html::image('products/crop/members/'.$blogs->image, $blogs->title)  : ' '  !!}
                                    {!! $errors->first('image','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="reset" class="btn btn-default">Clear Form</button>                                    
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>            
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER -->