@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::open(array('route'=>'admin.member.photos.store', 'role'=>'form', 'class'=>'form-horizontal', 'method' => 'post','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body">
	@include('admin.memberimage._partials.form')	
</div>                                               
{!! Form::close() !!}
</div>
@stop

