        <button class="btn btn-danger btn-rounded" data-form="#frmDelete-{{$member->id}}" data-title="Delete member" data-message="Are you sure, you want to delete this member ?" >
                                        <a class = "formConfirm" href="" style="color: #e04b4a;">
                                            <i title="Delete" style="margin-right: 0;" class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>