<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>411 Media</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="{{URL::to('theme/admin/css/theme-default.css')}}"/>
        <!-- EOF CSS INCLUDE -->                                     
    </head>
    <body>
        
        <div class="login-container lightmode">
        
            <div class="login-box animated fadeInDown">
                <div class="login-logo">411 Media| Admin Panel</div>
                @if (Session::has('flash_message'))
                <div style="text-align: center;">
                   <span class="control-group label label-important" style="color:red; font-size:15px;">
                   {!! Session::get('flash_message') !!}
                   </span>
                </div>
                @endif 
                <div class="login-body">
                    <div class="login-title"><strong>Log In</strong> to your account</div>
                    <!-- <form action="" class="form-horizontal" method="post"> -->
                    {!! Form::open(array('route'=>'admin.auth.login','class'=>'form-horizontal')) !!}
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" name="email" class="form-control" placeholder="E-mail"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" name="password" class="form-control" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Log In</button>
                        </div>
                    </div>
                    <!-- <div class="login-subtitle">
                        Don't have an account yet? <a href="#">Create an account</a>
                    </div> -->
                    {!! Form::close() !!}
                    <!-- </form> -->
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2017 411 Media
                    </div>
                </div>

            </div>
            
        </div>
        
    </body>
</html>






