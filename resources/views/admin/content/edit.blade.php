@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($content,array('route' => array('admin.contents.update',$content->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.content._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop

