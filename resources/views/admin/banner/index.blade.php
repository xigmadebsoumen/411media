@extends('_layouts.admin')
@section('content')
<!-- PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Banner</h3>
                    <a href="{{URL::to('admin/settings/site-banner/create')}}" class="btn btn-default btn-rounded pull-right">Add Banner</a></li>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($banners as $banner)
                            <tr>
                                <td>{!! $banner->name !!}</td>
                                <td>
                                    {!! Html::image('products/crop/banner/'.$banner->image, $banner->name) !!} 
                                </td>
                                <td>
                                    <a href="{{ URL::to('admin/settings/site-banner/'.$banner->id.'/edit') }}" class="btn btn-info btn-rounded"><i title="Edit" style="margin-right: 0;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    
                                    
                                    <a class="btn btn-xs btn-danger" data-record-id="" data-toggle="modal" data-target="#confirm-delete{{$banner->id}}">
                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                    </a>                                                    
                                    <div class="modal fade" id="confirm-delete{{$banner->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" style="width: 32%;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>You are about to delete <b><i class="title"></i></b> record, this procedure is irreversible.</p>
                                                    <p>Do you want to proceed?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::open(array('route' => ['admin.settings.site-banner.destroy',$banner->id],'method'=>'delete')) !!}
                                                    {!! Form::hidden('id',$banner->id) !!}
                                                    <button type="submit" class="btn btn-danger btn-ok" href="">Delete</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                    
                                    
                                    <!--
                                    <button class="btn btn-danger btn-rounded" data-form="#frmDelete-{{$banner->id}}" data-title="Delete Image" data-message="Are you sure, you want to delete this Image ?" >
                                        <a class = "formConfirm" href="" style="color: #e04b4a;">
                                            <i title="Delete" style="margin-right: 0;" class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    </button>
                                    {!! Form::open(array(
                                            'url' => route('admin.settings.site-banner.destroy', array($banner->id)),
                                            'method' => 'delete',
                                            'style' => 'display:none',
                                            'id' => 'frmDelete-' . $banner->id
                                        ))
                                    !!}
                                    {!! Form::submit('Submit') !!}
                                    {!! Form::close() !!}
                                    -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->
        </div>
    </div>                                
    
</div>
<!-- PAGE CONTENT WRAPPER -->                                
<!-- Modal Dialog for Delete -->
<div class="modal fade" id="formConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="frm_title">Delete</h4>
      </div>
      <div class="modal-body" id="frm_body">Are you sure, you want to delete this Topic ?</div>
      <div class="modal-footer">
        <button style='margin-left:10px;' type="button" class="btn btn-danger col-sm-2 pull-right" id="frm_submit">Delete</button>
        <button type="button" class="btn btn-primary col-sm-2 pull-right" data-dismiss="modal" id="frm_cancel">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Dialog show event handler for delete -->
<script type="text/javascript">
$(document).ready(function(){
  $('.formConfirm').on('click', function(e) {
        e.preventDefault();
        var el = $(this).parent();
        var title = el.attr('data-title');
        var msg = el.attr('data-message');
        var dataForm = el.attr('data-form');
        
        $('#formConfirm')
        .find('#frm_body').html(msg)
        .end().find('#frm_title').html(title)
        .end().modal('show');
        
        $('#formConfirm').find('#frm_submit').attr('data-form', dataForm);
  });
  $('#formConfirm').on('click', '#frm_submit', function(e) {
        var id = $(this).attr('data-form');
        $(id).submit();
  });
});
</script>
@stop