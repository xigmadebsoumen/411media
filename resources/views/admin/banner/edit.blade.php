@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($banners,array('route' => array('admin.settings.site-banner.update',$banners->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.banner._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
