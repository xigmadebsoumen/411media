@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::open(array('route'=>'admin.settings.site-banner.store', 'role'=>'form', 'class'=>'form-horizontal', 'method' => 'post','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body">
	@include('admin.banner._partials.form')	
</div>                                               
{!! Form::close() !!}
</div>
@stop

