@extends('_layouts.admin')
@section('content')             
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Order</h3>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                            	<th>#</th>
                                <th>Member Name</th>
                                <th>Member Email</th>
                                <th>Product</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php $c=1; foreach ($orders as $order){ ?>
                            <tr>
                            	<td><?= $c++ ?></td>
                                <td>{!! $order->user_name !!}</td>
                                <td>{!! $order->user_email !!}</td>
                                <td>
                                    {!! Html::image('products/crop/stores/'.$order->album_cover_image, $order->user_name) !!} 
                                </td>
                                <td>
                                    @if($order->status=='pending')
                                    <label class="btn btn-warning">{!! ucfirst($order->status) !!}</label>
                                    @endif
                                    @if($order->status=='failure')
                                    <label class="btn btn-danger">{!! ucfirst($order->status) !!}</label>
                                    @endif
                                    @if($order->status=='success')
                                    <label class="btn btn-success">{!! ucfirst($order->status) !!}</label>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ URL::to('admin/order/'.$order->order_id) }}" class="btn btn-info btn-rounded"><i title="Edit" style="margin-right: 0;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <button class="btn btn-danger btn-rounded" data-form="#frmDelete-{{$order->id}}" data-title="Delete order" data-message="Are you sure, you want to delete this order ?" >
                                        <a class = "formConfirm" href="" style="color: #e04b4a;">
                                            <i title="Delete" style="margin-right: 0;" class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    </button>
                                    {!! Form::open(array(
                                            'url' => route('admin.order.destroy', array($order->id)),
                                            'method' => 'delete',
                                            'style' => 'display:none',
                                            'id' => 'frmDelete-' . $order->id
                                        ))
                                    !!}
                                    {!! Form::submit('Submit') !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->
        </div>
    </div>                                
    
</div>
<!-- PAGE CONTENT WRAPPER -->                                
<!-- Modal Dialog for Delete -->
<div class="modal fade" id="formConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="frm_title">Delete</h4>
      </div>
      <div class="modal-body" id="frm_body">Are you sure, you want to delete this Topic ?</div>
      <div class="modal-footer">
        <button style='margin-left:10px;' type="button" class="btn btn-danger col-sm-2 pull-right" id="frm_submit">Delete</button>
        <button type="button" class="btn btn-primary col-sm-2 pull-right" data-dismiss="modal" id="frm_cancel">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Dialog show event handler for delete -->
<script type="text/javascript">
$(document).ready(function(){
  $('.formConfirm').on('click', function(e) {
        e.preventDefault();
        var el = $(this).parent();
        var title = el.attr('data-title');
        var msg = el.attr('data-message');
        var dataForm = el.attr('data-form');
        
        $('#formConfirm')
        .find('#frm_body').html(msg)
        .end().find('#frm_title').html(title)
        .end().modal('show');
        
        $('#formConfirm').find('#frm_submit').attr('data-form', dataForm);
  });
  $('#formConfirm').on('click', '#frm_submit', function(e) {
        var id = $(this).attr('data-form');
        $(id).submit();
  });
});
</script>
@stop