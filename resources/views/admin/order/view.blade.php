@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($orders[0],array('route' => array('admin.order.update',$orders[0]->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body">
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        @if (session('message'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{ session('message') }}</strong>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Order:</strong> Form</h3>
                    </div>
                   
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-6">
                            	<h3 class="panel-title"><strong>Member</strong> Details</h3>
                            	<div class='clearfix'></div>  
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Member Name</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('user_name',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Name who create order</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email ID</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon">@</span>
                                            {!! Form::text('user_email',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Email who create order</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Contact Number</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('user_phone_number',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Contact Number of the member</span>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    {!! Form::label('user_address','Member Address',array('class' => 'col-md-3 control-label'))  !!}
                                    <div class="col-lg-9">
                                        {!! Form::textarea('user_address',null,array('class'=>'form-control','placeholder'=>'Post Content here')) !!}
                                    </div> 
                                </div> 
                                <div class='clearfix'></div>                                
                            </div>
                            <div class="col-md-6">
                            	<h3 class="panel-title"><strong>Product</strong> Details</h3>
                            	<div class='clearfix'></div>  
                            	<div class="form-group">
                                    <label class="col-md-3 control-label">Album Name</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('album_name',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Album Name</span>
                                    </div>
                                </div>
                                <div class="form-group">                                        
	                                <label class="col-md-3 control-label">Date of Album Launch</label>
	                                <div class="col-md-9">
	                                    <div class="input-group">
	                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
	                                        <?php $date = date('Y-m-d', $orders[0]->album_launch_date); ?>
	                                        {!! Form::text('album_launch_date',$date, array('class' => 'form-control'))  !!}
	                                    </div>
	                                    <span class="help-block">Album Launch Date</span>
	                                </div>
	                            </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Album Price</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('price',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Album Price</span>
                                    </div>
                                </div>
                                <div class="form-group">
	                                {!! Form::label('album_cover_image','Album Cover Image',array('class' => 'col-md-3 control-label')) !!}  
	                                <div class="col-md-9">
	                                    {!! isset($orders[0]->album_cover_image) ? Html::image('products/crop/stores/'.$orders[0]->album_cover_image, $orders[0]->album_name)  : ' '  !!}
	                                </div>
	                            </div>
                            </div>
                            <div class='clearfix'></div>  
                            <div class="col-md-12">
                            	<h3 class="panel-title"><strong>Transaction</strong> Details</h3>
                            	<div class='clearfix'></div>  
                            	<div class="panel-body">
				                    <table class="table">
				                        <thead>
				                            <tr>
				                            	<th>#</th>
				                                <th>Member Name</th>
				                                <th>Transaction ID</th>
				                                <th>Status</th>
				                                <th>Action</th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                            <tr>
				                            	<td>1</td>
				                                <td>{!! $orders[0]->user_name !!}</td>
				                                <td>{!! $orders[0]->transaction_id !!}</td>
				                                <td>
				                                    @if($orders[0]->status=='pending')
				                                    <label class="btn btn-warning">{!! ucfirst($orders[0]->status) !!}</label>
				                                    @endif
				                                    @if($orders[0]->status=='failure')
				                                    <label class="btn btn-danger">{!! ucfirst($orders[0]->status) !!}</label>
				                                    @endif
				                                    @if($orders[0]->status=='success')
				                                    <label class="btn btn-success">{!! ucfirst($orders[0]->status) !!}</label>
				                                    @endif
				                                </td>
				                                <td>
				                                	@if($orders[0]->status=='pending')
				                                    <a href="#" class="btn btn-warning btn-rounded">No Transaction</a>
				                                    @endif
				                                    @if($orders[0]->status=='failure')
				                                    <a href="#" class="btn btn-danger btn-rounded">Waiting for Payment</a>
				                                    @endif
				                                    @if($orders[0]->status=='success')
				                                    <a href="#" class="btn btn-info btn-rounded">Send Mail</a>
				                                    @endif
				                                	
				                                </td>
				                            </tr>
				                        </tbody>
				                    </table>
				                </div>
                            </div>

                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{URL::to('admin/order')}}" class="btn btn-default">Back</a>                                    
                    </div>
                </div>            
            </div>
        </div>                    
        
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>              
{!! Form::close() !!}
</div>
@stop
