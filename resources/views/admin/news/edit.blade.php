@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($newss,array('route' => array('admin.news.update',$newss->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.news._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
