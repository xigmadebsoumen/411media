@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($videos,array('route' => array('admin.gallery.videos.update',$videos->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.video._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
