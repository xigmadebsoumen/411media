@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($settings,array('route' => array('admin.settings.update',$settings->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body">
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        @if (session('message'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{ session('message') }}</strong>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Site</strong> Information</h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Page Name</label> 
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('site_name',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Site Name</span>
                                        {!! $errors->first('site_name','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                {!! Form::label('site_logo','Site Logo',array('class' => 'col-md-3 control-label')) !!}  
                                <div class="col-md-9">
                                    {!! Form::hidden('site_logo', null, array('class' => 'control-label')) !!}
                                    {!! Form::file('site_logo', null, array('class' => 'fileinput btn-primary')) !!}
                                    <span class="help-block">Input type JPG, JPEG, PNG</span>
                                    <br>
                                    {!! isset($settings->site_logo) ? Html::image('products/thumb/settings/'.$settings->site_logo, $settings->site_name, array('height' => 60))  : ' '  !!}
                                    {!! $errors->first('site_logo','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Page Slogan</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('slogan_text',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Site slogan</span>
                                        {!! $errors->first('slogan_text','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Site</strong> Social Media Information</h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Facebook</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-facebook"></span></span>
                                            {!! Form::text('facebook_url',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Facebook URL</span>
                                        {!! $errors->first('facebook_url','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Twitter</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-twitter"></span></span>
                                            {!! Form::text('twitter_url',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Twitter URL</span>
                                        {!! $errors->first('twitter_url','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">LinkdIn</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-linkedin"></span></span>
                                            {!! Form::text('linkedin_url',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">LinkdIn URL</span>
                                        {!! $errors->first('linkedin_url','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">YouTube</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-youtube"></span></span>
                                            {!! Form::text('youtube_url',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">YouTube URL</span>
                                        {!! $errors->first('youtube_url','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div> 

                            </div>
                        </div>
                    </div>
                    -->
                    
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Site</strong> Other Information</h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Captcha</label>
                                    <div class="col-md-9">
                                        <label class="switch">
                                            <input type="checkbox" name="is_captcha" id="is_captcha" class="switch" <?php if($settings->is_captcha == '1') {echo "checked";}?>/>
                                            <span></span>
                                        </label>
                                        <br><span> Enable captcha at Registration.</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Facebook Login</label>
                                    <div class="col-md-9">
                                        <label class="switch">
                                            <input type="checkbox" name="is_facebooklgin" id="is_facebooklgin" class="switch" <?php if($settings->is_facebooklgin == '1') {echo "checked";}?>/>
                                            <span> </span>
                                        </label>
                                        <br><span> Allow User To Login Ustng Facebook. </span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group ">
                                    {!! Form::label('censor','Censor',array('class' => 'col-md-3 control-label'))  !!}
                                    <div class="col-lg-9">
                                        {!! Form::textarea('censor',null,array('class'=>'form-control','placeholder'=>'Censor', 'size'=>'30x5')) !!}
                                        {!! $errors->first('censor','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>                    
                    
                    
                    
                    
                    
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Office</strong> Information</h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                             <div class="clearfix"></div>
                               <div class="form-group ">
                                <!--
                                <div class="form-group ">
                                    {!! Form::label('office_address','Office Address',array('class' => 'col-md-3 control-label'))  !!}
                                    <div class="col-lg-9">
                                        {!! Form::textarea('office_address',null,array('class'=>'form-control','placeholder'=>'Office Address', 'size'=>'30x5')) !!}
                                        {!! $errors->first('office_address','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div> 
                                </div>
                                -->
                          
                                <div class="clearfix"></div>
                                <!--
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Address Latitude</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
                                            {!! Form::text('address_lat',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Address Latitude</span>
                                        {!! $errors->first('address_lat','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Address Longitude</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
                                            {!! Form::text('address_lang',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Address Longitude</span>
                                        {!! $errors->first('address_lang','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>  
                               -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Admin Phone Number</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                            {!! Form::text('admin_ph_no',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Admin Phone Number</span>
                                        {!! $errors->first('admin_ph_no','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                     <div class="form-group">
                                    <label class="col-md-3 control-label">Admin Fax Number</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                            {!! Form::text('admin_fax_no',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Admin Fax Number</span>
                                        {!! $errors->first('admin_fax_no','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Admin Email ID</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon">@</span>
                                            {!! Form::text('admin_email',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Admin Email ID</span>
                                        {!! $errors->first('admin_email','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div> 
                                                              
                            </div>
                        </div>
                    </div>

                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>SEO</strong> Information</h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meta Description</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon">D</span>
                                            {!! Form::text('meta_description',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Meta Description</span>
                                        {!! $errors->first('meta_description','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meta Keywords</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon">K</span>
                                            {!! Form::text('meta_keywords',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Meta Keywords</span>
                                        {!! $errors->first('meta_keywords','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Site Title</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon">T</span>
                                            {!! Form::text('site_title',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Site Title</span>
                                        {!! $errors->first('site_title','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="reset" class="btn btn-default">Clear Form</button>                                    
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>            
            </div>
        </div>                    
        
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>              
{!! Form::close() !!}
</div>
@stop
