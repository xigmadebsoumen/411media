@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($photos,array('route' => array('admin.gallery.photos.update',$photos->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.photos._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
