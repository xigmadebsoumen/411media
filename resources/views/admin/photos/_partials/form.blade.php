<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    @if (session('message'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <strong>{{ session('message') }}</strong>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Photos</strong> Form</h3>
                </div>
               
                <div class="panel-body">                                                                        
                    <div class="row">
                        <div class="col-md-12">
                            @if(!isset($photos))
                            <div class="form-group">
                                <label class="col-md-2 control-label">Album Type</label>
                                <div class="form-group" style="font-size: 20px;font-style: normal;">
                                    &nbsp;
                                    {{ Form::radio('album_type','existing_album',true, array('onclick'=>'checkAlbum("existing_album")')) }} Existing Album
                                    &nbsp;&nbsp;
                                    {{ Form::radio('album_type','new_album',null, array('onclick'=>'checkAlbum("new_album")')) }} New Album
                                </div>
                                {{ $errors->first('album_type')}}
                            </div>
                            @endif
                            <div class="clearfix"></div>
                            <div class="form-group" id="new_album">
                                <label class="col-md-2 control-label">Album Name</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('name',null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Name of the member</span>
                                    {!! $errors->first('name','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group" id="existing_album">
                                <label class="col-md-2 control-label">Choose Album</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::select('album_name',$album, null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">Name of the member</span>
                                    {!! $errors->first('album_name','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('image','Image',array('class' => 'col-md-2 control-label')) !!}  
                                <div class="col-md-9">
                                    {!! Form::hidden('image', null, array('class' => 'control-label')) !!}
                                    {!! Form::file('image', null, array('class' => 'fileinput btn-primary')) !!}
                                    <span class="help-block">Input type JPG, JPEG, PNG</span>
                                    <br>
                                    {!! isset($photos->image) ? Html::image('products/thumb/photos/'.$photos->image, $photos->name)  : ' '  !!}
                                    {!! $errors->first('image','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="reset" class="btn btn-default">Clear Form</button>                                    
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>            
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">
function checkAlbum(e) {
    id = e;
    if (id=='new_album') {
        $('#existing_album').hide();
        $('#new_album').show();        
    } else if(id=='existing_album'){

        $('#new_album').hide();
        $('#existing_album').show();
    }
}

$( document ).ready(function() {
    $('#new_album').hide();
});
</script>