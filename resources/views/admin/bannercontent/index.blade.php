@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($bannercontents,array('route' => array('admin.bannercontent.update',$bannercontents->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body">
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        @if (session('message'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{ session('message') }}</strong>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Our Routes</strong></h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Title</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('title',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block"></span>
                                        {!! $errors->first('title','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                            <div class="form-group">
                               {!! Form::label('description','Description',array('class' => 'col-md-3 control-label'))  !!}
                                <div class="col-lg-9">
                                    {!! Form::textarea('description',null,array('class'=>'form-control','placeholder'=>'description here', 'size' => '30x50')) !!}
                                    {!! $errors->first('description','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                      
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>            
            </div>
        </div>                    
        
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>              
{!! Form::close() !!}
</div>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">
$('#Formvalidate').validate({
    focusInvalid: false,
    rules: {
        confirm_password: {
            equalTo : "#new_password"
        }
    }
});
</script>
@stop
