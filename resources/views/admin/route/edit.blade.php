@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($route,array('route' => array('admin.route.update',$route->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.route._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop

