@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($content,array('route' => array('admin.contents.update',$content->id, $type),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body">
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        @if (session('message'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{ session('message') }}</strong>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php
                        $arr = '-';
                        $arr1 = ' ';
                        $string = str_replace($arr, $arr1, $type)
                        ?>
                        <h3 class="panel-title"><strong>Content:</strong> {!! ucwords($string) !!}</h3>
                    </div>
                   
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                            
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Page Title</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('title',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Page Title</span>
                                        {!! $errors->first('title','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Page Sub Title</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('subtitle',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Page Sub Title</span>
                                        {!! $errors->first('subtitle','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                   
                                <div class="form-group ">
                                    {!! Form::label('description','Page Content',array('class' => 'col-md-2 control-label'))  !!}
                                    <div class="col-lg-9">
                                        {!! Form::textarea('description',null,array('class'=>'form-control','placeholder'=>'Post Content here')) !!}
                                        {!! $errors->first('description','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div> 
                                </div> 
                                <div class='clearfix'></div>
                                <div class="form-group">
                                    {!! Form::label('image','Banner Image',array('class' => 'col-md-2 control-label')) !!}  
                                    <div class="col-md-9">
                                        {!! Form::hidden('image', null, array('class' => 'control-label')) !!}
                                        {!! Form::file('image', null, array('class' => 'fileinput btn-primary')) !!}
                                        <span class="help-block">Input type JPG, JPEG, PNG</span>
                                        <span class="help-block" style="color:red;">*Minimum upload size( 1400 * 500 )</span>
                                        <br>
                                        {!! isset($content->image) ? Html::image('products/crop/content/'.$content->image)  : ' '  !!}
                                        {!! $errors->first('image','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                <div class='clearfix'></div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Page SEO Name</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('seo_name',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Page SEO Name</span>
                                        {!! $errors->first('seo_name','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Page SEO Description</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('seo_description',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Page SEO Description</span>
                                        {!! $errors->first('seo_description','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Page SEO Keywords</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('seo_keywords',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Page SEO Keywords</span>
                                        {!! $errors->first('seo_keywords','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                <div class='clearfix'></div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label text-left">Page Status</label>
                                    <div class="col-md-9">
                                        <label class="switch">
                                            <input type="checkbox" name="status" id="status" class="switch" <?php if($content->status == '1') {echo "checked";}?>/>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="reset" class="btn btn-default">Clear Form</button>                                    
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>            
            </div>
        </div>                    
        
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>              
{!! Form::close() !!}
</div>
<script type="text/javascript">
$(document.body).on("change",":checkbox", function() {
        variable =  this.checked ? '1' : '0';
        $('#status').val(variable);
});
</script> 
@stop
