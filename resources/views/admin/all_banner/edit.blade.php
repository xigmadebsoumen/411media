@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($all_banners,array('route' => array('admin.settings.all-banner.update',$all_banners->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.all_banner._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
