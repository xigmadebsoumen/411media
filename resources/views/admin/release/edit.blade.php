@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($releases,array('route' => array('admin.release.update',$releases->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.release._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
