@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($users,array('route' => array('admin.user.updateprofile',$users->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body">
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        @if (session('message'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>{{ session('message') }}</strong>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Admin Login</strong> Information</h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Name</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('name',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Admin Name</span>
                                        {!! $errors->first('name','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('email',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Admin Username</span>
                                        {!! $errors->first('username','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                {!! Form::label('image','Image',array('class' => 'col-md-3 control-label')) !!}  
                                <div class="col-md-9">
                                    {!! Form::hidden('image', null, array('class' => 'control-label')) !!}
                                    {!! Form::file('image', null, array('class' => 'fileinput btn-primary')) !!}
                                    <span class="help-block">Input type JPG, JPEG, PNG</span>
                                    <br>
                                    {!! isset($users->image) ? Html::image('products/thumb/members/'.$users->image, $users->site_name, array('height' => 60))  : ' '  !!}
                                    {!! $errors->first('image','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                                <div class="clearfix"></div>
                                
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Admin Change</strong> Password</h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Current Password</label>
                                    <div class="col-md-9"> 
                                        
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                                            <!--{!! Form::password('current_password',null, array('class' => 'form-control'))  !!}-->
                                            <input class="form-control valid" name="current_password" value="" type="password">
                                        </div>                                            
                                        <span class="help-block">Current Password of Admin</span>
                                        {!! $errors->first('current_password','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">New Password</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                                            <!--{!! Form::password('new_password',null, array('class' => 'form-control','id'=>'new_password'))  !!}-->
                                            <input class="form-control valid" name="new_password" id="new_password" value="" type="password">
                                        </div>                                            
                                        <span class="help-block">New Password of Admin</span>
                                        {!! $errors->first('new_password','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Confirm Password</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                                            <!--{!! Form::password('confirm_password',null, array('class' => 'form-control'))  !!}-->
                                            <input class="form-control valid" name="confirm_password" value="" type="password">
                                        </div>                                            
                                        <span class="help-block">Same as New Password </span>
                                        {!! $errors->first('confirm_password','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <button type="reset" class="btn btn-default">Clear Form</button>                                    
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>            
            </div>
        </div>                    
        
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>              
{!! Form::close() !!}
</div>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">
$('#Formvalidate').validate({
    focusInvalid: false,
    rules: {
        confirm_password: {
            equalTo : "#new_password"
        }
    }
});
</script>
@stop
