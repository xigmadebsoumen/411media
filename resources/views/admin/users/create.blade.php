@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::open(array('route'=>'admin.users.store', 'role'=>'form', 'class'=>'form-horizontal', 'method' => 'post','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body">
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                @if (session('message'))
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <strong>{{ session('message') }}</strong>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>User Login</strong>Information</h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">First Name</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('fname',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">First Name</span>
                                        {!! $errors->first('fname','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Last Name</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('lname',null, array('class' => 'form-control'))  !!}
                                        </div>                                            
                                        <span class="help-block">Last Name</span>
                                        {!! $errors->first('lname','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>                                
                                
                                <div class="form-group">
                                    <label class="col-md-2 control-label">E-mail</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            {!! Form::text('email',null, array('class' => 'form-control','autocomplete'=>'off'))  !!}
                                        </div>                                            
                                        <span class="help-block">Customer email</span>
                                        {!! $errors->first('email','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Password</label>
                                    <div class="col-md-9">                                            
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
<!--                                            {!! Form::text('password',null, array('class' => 'form-control','autocomplete'=>'off'))  !!}-->
                                            <input class="form-control" autocomplete="off" name="password" type="password">
                                        </div>                                            
                                        <span class="help-block">Customer password</span>
                                        {!! $errors->first('password','<span class="label label-important" style="color: red">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="reset" class="btn btn-default">Clear Form</button>                                    
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>            
            </div>
        </div>                    
        
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>              
{!! Form::close() !!}
</div>
@stop
