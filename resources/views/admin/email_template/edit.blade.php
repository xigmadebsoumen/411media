@extends('_layouts.admin')
@section('content')
<div class="block">
{!! Form::Model($templates,array('route' => array('admin.settings.email-template.update',$templates->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
<div class="panel-body"> 
   	  @include('admin.email_template._partials.form')			
</div>				
{!! Form::close() !!}
</div>
@stop
