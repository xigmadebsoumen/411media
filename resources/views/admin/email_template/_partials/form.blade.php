<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Email:</strong> Template</h3>
                </div>
               
                <div class="panel-body">                                                                        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Mail Subject</label>
                                <div class="col-md-9">                                            
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        {!! Form::text('subject',null, array('class' => 'form-control'))  !!}
                                    </div>                                            
                                    <span class="help-block">subject of the member</span>
                                    {!! $errors->first('subject','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div>
                            </div>

               
                            <div class="form-group ">
                                {!! Form::label('description','Mail Body',array('class' => 'col-md-3 control-label'))  !!}
                                <div class="col-lg-9">
                                    {!! Form::textarea('description',null,array('class'=>'form-control','placeholder'=>'Post Content here', 'size' => '30x50')) !!}
                                    {!! $errors->first('description','<span class="label label-important" style="color: red">:message</span>') !!}
                                </div> 
                            </div> 

                            <div class='clearfix'></div>

                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="reset" class="btn btn-default">Clear Form</button>                                    
                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                </div>
            </div>            
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER -->