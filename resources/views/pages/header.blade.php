
<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{!! asset('theme/site/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('theme/site/css/style.css') !!}"> <!-- Resource style -->
    <link rel="stylesheet" href="{!! asset('theme/site/css/main.css') !!}">
    <link href="{!! asset('theme/site/css/owl.carousel.css') !!}" rel="stylesheet">
    <link href="{!! asset('theme/site/css/owl.theme.css') !!}" rel="stylesheet">
    <script src="{!! asset('theme/site/js/modernizr.js') !!}"></script> <!-- Modernizr -->
    
    <title>Corporate Elevator Consultants</title>
</head>
<body>
<nav class="cd-vertical-nav">
<div class="container">
<div class="col-xs-12 col-sm-4 col-md-4 mobile">
<div class="logo"><a href="{{ URL::route('site') }}"><img src="{!! asset('theme/site/img/logo.png') !!}" class="img-responsive"></a></div>
</div>

<div class="col-xs-12 col-sm-5 col-md-5">
    <ul class="menu">
        <li><a href="#section1" class="active">Home</a></li>
        <li><a href="#section2">About Us</a></li>
        <li><a href="#section3">Services</a></li>
        <li><a href="#section4">Clients</a></li>
        <li><a href="#section5">Contact</a></li>
    </ul>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 mobile">

    <ul class="social-list">
            <li><a href="{{$settings->facebook_url}}" target="_blank"><i class="fa fa-facebook" aria-hidden="true" target="_blank"></i></a></li>
            <li><a href="{{$settings->twitter_url}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="{{$settings->youtube_url}}" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            <li><a href="{{$settings->linkedin_url}}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
           </ul>
      </div>
</div>