<footer id="section6" class="cd-section footer text-center">
    <div class="content-wrapper">
        <a href="{{ URL::route('site') }}"><img src="{!! asset('theme/site/img/footer-logo.png') !!}" class="img-responsive footer-logo"></a>
        <ul class="footer-address list-unstyled">
            <li><h4>{{$settings->site_name}}</h4></li>
            <li>Voice:{{$settings->admin_ph_no}}</li>
            <li>Fax:{{$settings->admin_fax_no}}</li>
            {!! $settings->office_address !!}
            <li><a href="mailto:{{$settings->admin_email}}">{{$settings->admin_email}}</a></li>
        </ul>
    </div>
<div class="overlay"></div>
</footer><!-- cd-section -->
    
<script src="{!! asset('theme/site/js/jquery-2.1.4.js') !!}"></script>
<script src="{!! asset('theme/site/js/main.js') !!}"></script> <!-- Resource jQuery -->
<script src="{!! asset('theme/site/js/owl.carousel.js') !!}"></script>
<script>
$(document).ready(function() {

  var owl = $("#owl-demo");

  owl.owlCarousel({
     
      itemsCustom : [
        [0, 1],
        [450, 2],
        [600, 3],
        [700, 4],
        [1000, 5],
        [1200, 6],
        [1400, 7],
        [1600, 8]
      ],
      navigation : false,
      autoPlay: 1000

  });
  var win  = jQuery(window),
    fxel     = jQuery('.cd-vertical-nav'),
    eloffset = fxel.offset().top;
    win.scroll(function() {
        if (eloffset < win.scrollTop()) {
            fxel.addClass("fixed");
        } else {
            fxel.addClass("fixed");
        }
    });

});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.social-list li a').click(function(){
      return true;
    });
  });
</script>
</body>
</html>