@extends('_layouts.post')
@section('content')
<div id="embed-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content modal-style">
            <span class="cross-btm flaticon-error" class="close" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <input type="text" class="form-control inputstyle" id="embedv" placeholder="Embed code here ">
                    </div>
                </div>
                <input type="button" onclick="getEmbedVal()" value="Save" class="pop-button">
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="mibble-short">
            <ul class="post-linl-list">
                <li class="active"><a href="{!! URL::to('addpost') !!}"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addphoto') !!}"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addlink') !!}"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addblog') !!}"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addvideo') !!}"><i class="fa fa-video-camera" aria-hidden="true"></i></a></li>
            </ul>
            <form id="data">
                <div class="form-place">
                    <h3 class="short-area-heading">Video</h3>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="row">
                            <span class="upload-left" id="fileselector">
                                <label class="btn up-btm" for="upload-file-selector">
                                    <input id="upload-file-selector" name="video" accept="video/mp4,video/MP4" id="video" value="" type="file">
                                    <i class="fa fa-upload" aria-hidden="true"></i> <span>Upload</span>
                                </label>
                            </span>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $('input[type=file]').change(function(){
                                var file = this.files[0];
                                //var name = file.name;
                                //var size = file.size;
                                var type = file.type;
                                if(type != 'video/mp4'){
                                    $('#video').val('');
                                    $('input[type=file]').val('');
                                    $('#vid-extvalidation').modal('show');
                                }
                            });                            
                        });
                    </script>
                    
                    <div class="modal fade" id="vid-extvalidation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 32%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Upload Vedio Errore</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Your uploaded video file extension not valid!!. <b><i class="title"></i></b> Please upload .mp4 extension file.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="row">
                            <span class="cross-icon orblack">Or</span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="row">
                            <a href="#" class="emb-btm" data-toggle="modal" data-target="#embed-modal"><i class="fa fa-link" aria-hidden="true"></i> <span>Embed </span></a>
                        </div>
                    </div>
                    <input type="hidden" name="embed" id="embed" value="" >
                    
                    <script>
                     function getEmbedVal(){
                         var embedval = $('#embedv').val();
                         $('#embed').val(embedval);
                         $('#embed-modal').modal('hide');
                     }
                    </script>
                    
                    
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <input type="text" class="form-control inputstyle" name="title" id="title" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control inputstyle" name="location" id="location" placeholder="Locations">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control textarea-style" rows="5" name="caption" id="caption" placeholder="Captions"></textarea>
                    </div>
                </div>
                <span class="postcontent-list">
                    <div class="radio">
                        <label class="checkbox"><input type="checkbox" name="is_adult" id="is_adult" value="1"><i></i>Post contains adult content</label>
                    </div>

                </span>


                <div class="channel-dropdown">
                    <a href="#" data-toggle="collapse" data-target="#demo">
                        <div class="col-xs-8 col-sm-8 col-md-8">Add to channel</div>
                        <div class="col-xs-4 col-sm-4col-md-4 text-right"><i class="fa fa-bars" aria-hidden="true"></i>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </div>
                <ul id="demo" class="channel-list">
                    <?php foreach ($chanel as $ch) { ?>
                        <li>
                            <div class="radio">
                                <label class="checkbox">
                                    <input type="checkbox" name="category[]" value="<?php echo $ch['name'] ?>"><i></i><?php echo $ch['name'] ?></label>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <script>
                    $(document).ready(function () {
                        $("input[name='category[]']").change(function () {
                            var maxAllowed = 3;
                            var cnt = $("input[name='category[]']:checked").length;
                            if (cnt > maxAllowed) {
                                $(this).prop("checked", "");
                                //alert('You can select maximum ' + maxAllowed + ' technologies!!');
                                $('#checkbox-limit').modal('show');
                            }
                        });
                    });
                </script>                        
                <div class="modal fade" id="checkbox-limit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 32%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Category Limit</h4>
                            </div>
                            <div class="modal-body">
                                <p>You are reached limit to choose category. <b><i class="title"></i></b> Maximum limit of category is three.</p>
                                <p>Delete one to add new category?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="social-place">
                            <ul class="social-list">
                                <!--<li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon.jpg') !!}"></span>411 Media</label>
                                    </div>
                                </li>-->
                                <li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" name="twittershare" id="twittershare" onclick="twitterShare(this, 'http://twitter.com/share?url=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>', 'Twitter', '500', '258')" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon3.jpg') !!}"></span>Twitter</label>
                                            <!--<input type="checkbox" name="twittershare" onclick="popWindow('http://twitter.com/share?url=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>','Twitter','500', '258')" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon2.jpg') !!}"></span>Twitter</label>-->
                                    </div>
                                </li>
                                <li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" name="facebookshare" id="facebookshare" onclick="facebookShare(this, 'http://www.facebook.com/sharer.php?u=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>', 'Facebook', '500', '400')" value="1" ><i></i><span><img src="{!! asset('theme/site/images/social-icon2.jpg') !!}"></span>Facebook</label>
                                            <!--<input type="checkbox" name="facebookshare" onclick="popWindow('http://www.facebook.com/sharer.php?u=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>', 'Facebook','500','400')" value="1" ><i></i><span><img src="{!! asset('theme/site/images/social-icon3.jpg') !!}"></span>Facebook</label>-->
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="top-addvertisement margin-half"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
                </div>
                <ul class="button-list button-list-big">
                    
                    <li>
                        <!-- <a href="javascript:void(0)" onclick="sharePostVideo();" class="pop-button login-buttom">Share Post</a> -->
                        <input type="submit" class="pop-button login-buttom" value="Share Post">
                    </li>
                </ul>
            </form>
        </div>
    </div>
</section>
<script>
    function twitterShare(vardt, url, winName, w, h) {
        if ($('#twittershare').is(':checked')) {
            //alert("Checkbox is checked.");
            popWindow(url, winName, w, h);
        }
    }
    function facebookShare(vardt, url, winName, w, h) {
        if ($('#facebookshare').is(':checked')) {
            //alert("Checkbox is checked.");
            popWindow(url, winName, w, h);
        }
    }

    function popWindow(url, winName, w, h) {
        if (window.open) {
            if (poppedWindow) {
                poppedWindow = '';
            }
            windowW = w;
            windowH = h;
            var windowX = (screen.width / 2) - (windowW / 2);
            var windowY = (screen.height / 2) - (windowH / 2);
            var myExtra = "status=no,menubar=no,resizable=yes,toolbar=no,scrollbars=yes,addressbar=no";
            var poppedWindow = window.open(url, winName, 'width=' + w + ',height=' + h + ',top=' + windowY + ',left=' + windowX + ',' + myExtra + '');
        } else {
            alert('Your security settings are not allowing our popup windows to function. Please make sure your security software allows popup windows to be opened by this web application.');
        }
        return false;
    }

    function sharePostVideo() {
        alert(1111);

        url = "{{ Config::get('config.baseurl') }}savevideo";
        $.ajax({
            url: url,
            type: 'POST',
            cache: false,
            processData: false,
            contentType: 'multipart/form-data',
            data: {file: $('#video').attr('files'), title: title},
        }).done(function (response) {
            alert(response);
        });



    }


    $("form#data").submit(function (event) {

        //disable the default form submission
        event.preventDefault();

        //grab all form data  
        var formData = new FormData($(this)[0]);
        alert(formData);
        $.ajax({
            url: "{{ Config::get('config.baseurl') }}savevideo",
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (returndata) {
                alert(returndata);
            }
        });

        return false;
    });

</script>
@stop