@extends('_layouts.post')
@section('content')
<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide" 
  });
});
</script>
<style type="text/css">
.flex-caption {
  width: 96%;
  padding: 2%;
  left: 0;
  bottom: 0;
  background: rgba(0,0,0,.5);
  color: #fff;
  text-shadow: 0 -1px 0 rgba(0,0,0,.3);
  font-size: 14px;
  line-height: 18px;
}
</style>
<section class="body-section">
    <div class="container-fluid">
        <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="post-place time-line-area">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <div class="row">
                    <div class="mobile-view">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <div class="row">
                                <div class="post-profile-details">
                                    <?php if(isset($udt->id) && $udt->id != $dt['User']->id){ ?>
                                        <a href="{{ URL::to('searchprofile/'.$dt['User']->pname) }}">
                                    <?php } else { ?>
                                        <a href="javascript:void(0)">
                                    <?php } ?>
                                        <div class="message-pic">
                                        <?php if($dt['User']->image != ""){ ?>
                                            <img src="{!! asset('products/crop/user/'.$dt['User']->image) !!}">
                                        <?php } else { ?>
                                            <img src="{!! asset('devimg/noimg.png') !!}">
                                        <?php } ?>
                                        </div>
                                        <div class="messagedetails">
                                            <span class="messagename"><?php echo $dt['User']->pname;?></span>
                                            <span class="message-short black margin-top"><?php echo  date("M d Y",strtotime($dt['Post']->created_at));?></span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <?php if (Auth::check()) { ?>
                        <?php $udt = Auth::user(); ?>
                        <?php if($udt->id == $dt['User']->id){ ?>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <a href="javascript:void(0)" class="three-dropdown text-right" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ URL::to('delete/'.$dt['Post']->id) }}">Delete</a></li>
                                    <?php if($dt['Post']->is_turnoff_comments == 0){ ?>
                                        <li><a href="{{ URL::to('turnoffcomments/'.$dt['Post']->id) }}">Turn off Comments</a></li>
                                    <?php } else { ?>
                                        <li><a href="{{ URL::to('turnoncomments/'.$dt['Post']->id) }}">Turn on Comments</a></li>
                                    <?php } ?>                                

                                    <?php if($dt['Post']->is_turnoff_share == 0){ ?>
                                        <li><a href="{{ URL::to('turnoffshare/'.$dt['Post']->id) }}">Turn off Sharing</a></li>
                                    <?php } else { ?>
                                        <li><a href="{{ URL::to('turnonshare/'.$dt['Post']->id) }}">Turn on Sharing</a></li>
                                    <?php } ?>
                                    <li><a href="javascript:void(0)">Monetize</a></li>
                            </ul>
                            <span class="views text-right"><?php echo $views?> Views</span>
                        </div>
                        <?php } else { ?>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <a href="javascript:void(0)" class="three-dropdown text-right" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php if(empty($getBlock)){ ?>
                                <li><a href="javascript:void(0)" onclick="blockThisUser(<?php echo $dt['User']->id?>,<?php echo $udt->id?>);">Block User</a></li>
                                <?php } else { ?>
                                <li><a href="javascript:void(0)" >User Blocked</a></li>
                                <?php } ?>
                                
                                <?php if(empty($getReport) && !empty($udt)){ ?>
                                <li><a href="javascript:void(0)" onclick="reportThisPost(<?php echo $dt['Post']->id?>,<?php echo $dt['User']->id?>,<?php echo $udt->id?>);">Report</a></li>
                                <?php } else { ?>
                                <li><a href="javascript:void(0)" >Reported</a></li>
                                <?php } ?> 
                                
                                
                                <li><a href="javascript:void(0)" onclick="shareThisUser(<?php echo $dt['User']->id?>,<?php echo $udt->id?>);">Share This User</a></li>
                                
                                
                                
                                <li><a href="{{ URL::to('messageuser/'.base64_encode($dt['Post']->id)) }}">Send Message</a></li>
                                <li><a href="javascript:void(0)" onclick="getNotification(<?php echo $dt['User']->id?>,<?php echo $udt->id?>);">Get Notification</a></li>
                                <li><a href="javascript:void(0)" onclick="seeFirstPost(<?php echo $dt['Post']->id?>,<?php echo $udt->id?>);">See First</a></li>
                            </ul>
                            <span class="views text-right"><?php echo $views?> Views</span>
                        </div>                  
                        <?php } }?>                        

                        <div class="clearfix"></div>
                    </div>
                    <div class="short-port">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                            <div class="mob-logo"><img src="{!! asset('theme/site/images/social-icon.jpg') !!}">
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8  ">
                            <span class="messagename "><?php echo $dt['User']->pname;?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!--short-port-->
                    <div class="blog-heading">
                        <?php //echo $dt['Post']->location;?>
                    </div>
                    <figure class="photo-image">
                        <div class="flexslider">
                            <ul class="slides">
                                <?php foreach($dt['Album'] as $alb){ ?>
                                <li>
                                    <img src="{!! asset('products/main/post/'.$alb->img) !!}">
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </figure>
                    <div class="mobile-view">
                        <ul class="three-list">
                            <?php if (empty($getLike) && !empty($udt)) { ?>
                            <li><a href="javascript:void(0)" onclick="likenow('<?php echo $id?>','<?php echo $dt['User']->id?>','<?php echo $udt->id?>')" >Like</a></li>
                            <?php } else { ?>
                            <li><a href="javascript:void(0)" >Like</a></li>
                            <?php } ?>

                            <?php if($dt['Post']->is_turnoff_comments == 0 && !empty($udt)){ ?>
                                <li><a href="{{ URL::to('comment/'.base64_encode($dt['Post']->id)) }}">Comment</a></li>
                            <?php } else { ?>
                                <li><a href="javascript:void(0)">Comment</a></li>
                            <?php } ?>                    

                            <?php if($dt['Post']->is_turnoff_share == 0 && !empty($udt)){ ?>
                                <li><a href="javascript:void(0)" onclick="sharenow('<?php echo $dt['Post']->pid != 0 ? $dt['Post']->pid : $id?>','<?php echo $udt->id?>')" >Share</a></li>
                            <?php } else { ?>
                            <li><a href="javascript:void(0)">Share</a></li>
                            <?php } ?>
                        </ul>
                        <ul class="three-list-pic"> 
                            <li><a href="{{ URL::to('likelist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-smile-o" aria-hidden="true"></i> <?php echo $like;?></a></li>
                            <li><a href="{{ URL::to('commentlist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-comments" aria-hidden="true"></i> <?php echo $comments;?></a></li>
                            <li><a href="{{ URL::to('sharelist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-share-alt" aria-hidden="true"></i> <?php echo $share;?></a></li>
                        </ul>                        
                    </div>
                    <figcaption class="photo-caption">
                        <?php echo $dt['Post']->caption;?>
                    </figcaption>

                        <script type="text/javascript">
                            
                            
                            function reportThisPost(pid,uid,ruser){
                                url = "{!! URL::to('reportpost') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", pid: pid, uid:uid, ruser:ruser},
                                }).done(function (response) { 
                                    //alert(response);
                                    window.location.reload();
                                });                            
                            }                            
                            
                            
                            function blockThisUser(buid,uid){
                                url = "{!! URL::to('blockuser') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", buid: buid, uid:uid},
                                }).done(function (response) {
                                    //alert(response);
                                    //$('#confirm-share').modal('show');
                                    window.location.reload();
                                });                            
                            }
                             
                            function shareThisUser(uid,share_by){
                                //alert(11);
                                url = "{!! URL::to('sharethisuser') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", uid: uid, share_by: share_by},
                                }).done(function (response) {
                                    //alert(response);
                                    $('#confirm-share').modal('show');
                                    
                                });                            
                            }                             
                            function setNotification(nid,uid){
                                url = "{!! URL::to('setnotification') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", nid: nid, uid: uid},
                                }).done(function (response) {
                                    //alert(response);
                                    window.location.reload();
                                });                            
                            }

                            function unsetNotification(nid,uid){
                                url = "{!! URL::to('unsetnotification') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", nid: nid, uid: uid},
                                }).done(function (response) {
                                    window.location.reload();
                                });                           
                            }
                            
                            
                            function likenow(pid,puid,uid){
                                url = "{!! URL::to('likenow') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", pid: pid, puid:puid, uid: uid},
                                }).done(function (response) {
                                    //alert(response);
                                    window.location.reload();
                                });                            
                            }

                            function unlikenow(pid,uid){
                                url = "{!! URL::to('unlikenow') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", pid: pid, uid: uid},
                                }).done(function (response) {
                                    window.location.reload();
                                });                           
                            } 
                            
                            function sharenow(pid,uid){
                                //alert(11);
                                url = "{!! URL::to('sharenow') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", pid: pid, uid: uid},
                                }).done(function (response) {
                                    //alert(response);
                                    $('#confirm-share').modal('show');
                                    
                                });                            
                            }            
                            
                            function closeModal(){
                                $('#confirm-share').modal('hide');
                                window.location.reload();
                            }                           
                        </script>                     

                        <div class="modal fade" id="confirm-share" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" style="width: 32%;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                                        <h4 class="modal-title" id="myModalLabelhead">Shared Successfully </h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Shared Successfully.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" onclick="closeModal()" class="btn btn-default">Exit</button>
                                    </div>
                                </div>
                            </div>
                        </div>                        

                    <div class="single-add">
                        <img src="{!! asset('theme/site/images/add02.jpg') !!}">
                    </div>
	
                    <a href="{{ URL::to('commentlist/'.base64_encode($dt['Post']->id)) }}" class="comments-button"> <i class="fa fa-comments" aria-hidden="true"></i>Show Comments</a>

                    <div class="sponser-place sponser-place-add">
                        <ul class="sponser-list">
                            <li><img src="{!! asset('theme/site/images/add01.jpg') !!}" class="img-responsive">
                            </li>
                            <li><img src="{!! asset('theme/site/images/add01.jpg') !!}" class="img-responsive">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="post-top">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <div class="row">
                            <div class="post-profile-details">
                                <?php if(!empty($udt) && $udt->id != $dt['User']->id){ ?>
                                    <a href="{{ URL::to('searchprofile/'.$dt['User']->pname) }}">
                                <?php } else { ?>
                                    <a href="javascript:void(0)">
                                <?php } ?>
                                    <div class="message-pic">
                                    <?php if($dt['User']->image != ""){ ?>
                                        <img src="{!! asset('products/crop/user/'.$dt['User']->image) !!}">
                                    <?php } else { ?>
                                        <img src="{!! asset('devimg/noimg.png') !!}">
                                    <?php } ?>
                                    </div>
                                        <div class="messagedetails">
                                             <span class="messagename"><?php echo $dt['User']->pname;?></span>
                                            <span class="message-short black margin-top"><?php echo  date("M d Y",strtotime($dt['Post']->created_at));?></span>
                                        </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php if (Auth::check()) { ?>
                    <?php $udt = Auth::user(); ?>
                    <?php if(!empty($udt) && $udt->id == $dt['User']->id){ ?>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <a href="javascript:void(0)" class="three-dropdown text-right" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ URL::to('delete/'.$dt['Post']->id) }}">Delete</a></li>
                                <?php if($dt['Post']->is_turnoff_comments == 0){ ?>
                                    <li><a href="{{ URL::to('turnoffcomments/'.$dt['Post']->id) }}">Turn off Comments</a></li>
                                <?php } else { ?>
                                    <li><a href="{{ URL::to('turnoncomments/'.$dt['Post']->id) }}">Turn on Comments</a></li>
                                <?php } ?>                                
                                
                                <?php if($dt['Post']->is_turnoff_share == 0){ ?>
                                    <li><a href="{{ URL::to('turnoffshare/'.$dt['Post']->id) }}">Turn off Sharing</a></li>
                                <?php } else { ?>
                                    <li><a href="{{ URL::to('turnonshare/'.$dt['Post']->id) }}">Turn on Sharing</a></li>
                                <?php } ?>
                                <li><a href="javascript:void(0)">Monetize</a></li>
                        </ul>
                        <span class="views text-right"><?php echo $views?> Views</span>
                    </div>
                    <?php } else { ?>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <a href="javascript:void(0)" class="three-dropdown text-right" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php if(empty($getBlock) && !empty($udt)){ ?>
                                <li><a href="javascript:void(0)" onclick="blockThisUser(<?php echo $dt['User']->id?>,<?php echo $udt->id?>);">Block User</a></li>
                                <?php } else { ?>
                                <li><a href="javascript:void(0)" >Blocked User</a></li>
                                <?php } ?>
                                
                                <?php if(empty($getReport) && !empty($udt)){ ?>
                                <li><a href="javascript:void(0)" onclick="reportThisPost(<?php echo $dt['Post']->id?>,<?php echo $dt['User']->id?>,<?php echo $udt->id?>);">Report</a></li>
                                <?php } else { ?>
                                <li><a href="javascript:void(0)" >Reported</a></li>
                                <?php } ?>                                
                                
                                <?php if(empty($isShared) && !empty($udt) && $dt['User']->id != $udt->id){ ?>
                                <li><a href="javascript:void(0)" onclick="shareThisUser(<?php echo $dt['User']->id?>,<?php echo $udt->id?>);">Share This User</a></li>
                                <?php } else { ?>
                                <li><a href="javascript:void(0)" >Alredy Shared</a></li>
                                <?php } ?>
                                
                                
                                <li><a href="{{ URL::to('messageuser/'.base64_encode($dt['Post']->id)) }}">Send Message</a></li>
                                
                                <?php if(empty($getnotyfyme)){ ?>
                                <li><a href="javascript:void(0)" onclick="setNotification(<?php echo $dt['User']->id?>,<?php echo $udt->id?>);">Get Notification</a></li>
                                <?php } else { ?>
                                <li><a href="javascript:void(0)" onclick="unsetNotification(<?php echo $dt['User']->id?>,<?php echo $udt->id?>);">Unset Notification</a></li>
                                <?php } ?> 
                                
                                <li><a href="javascript:void(0)" onclick="seeFirstPost(<?php echo $dt['Post']->id?>,<?php echo $udt->id?>);">See First</a></li>
                            </ul>
                        <span class="views text-right"><?php echo $views?> Views</span>
                    </div>                  
                    <?php } }?>
                </div>
                <div class="clearfix"></div> 
                <ul class="three-list">
                    <?php if (empty($getLike) && !empty($udt)) { ?>
                    <li><a href="javascript:void(0)" onclick="likenow('<?php echo $id?>','<?php echo $dt['User']->id?>','<?php echo $udt->id?>')" >Like</a></li>
                    <?php } else { ?>
                    <li><a href="javascript:void(0)" >Like</a></li>
                    <?php } ?>
                    
                    <?php if($dt['Post']->is_turnoff_comments == 0 && !empty($udt)){ ?>
                        <li><a href="{{ URL::to('comment/'.base64_encode($dt['Post']->id)) }}">Comment</a></li>
                    <?php } else { ?>
                        <li><a href="javascript:void(0)">Comment</a></li>
                    <?php } ?>                    

                    <?php if($dt['Post']->is_turnoff_share == 0  && !empty($udt)){ ?>
                        <li><a href="javascript:void(0)" onclick="sharenow('<?php echo $dt['Post']->pid != 0 ? $dt['Post']->pid : $id?>','<?php echo $udt->id?>')" >Share</a></li>
                    <?php } else { ?>
                    <li><a href="javascript:void(0)">Share rr</a></li>
                    <?php } ?>
                </ul>
                <ul class="three-list-pic"> 
                    <li><a href="{{ URL::to('likelist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-smile-o" aria-hidden="true"></i> <?php echo $like;?></a></li>
                    <li><a href="{{ URL::to('commentlist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-comments" aria-hidden="true"></i> <?php echo $comments;?></a></li>
                    <li><a href="{{ URL::to('sharelist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-share-alt" aria-hidden="true"></i> <?php echo $share;?></a></li>
                </ul>
                <hr>
                <ul class="add-list">
                    <li><img src="{!! asset('theme/site/images/add01.jpg') !!}" class="img-responsive">
                    </li>
                    <li><img src="{!! asset('theme/site/images/add02.jpg') !!}" class="img-responsive">
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<script>
    jQuery(document).ready(function ($) {
        $("#photo-slider").owlCarousel({
            items: 1,
            navigation: true,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            autoPlay: false
        });
    });
</script>
@stop