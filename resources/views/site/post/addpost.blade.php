@extends('_layouts.post')
@section('content')
<?php //echo "<pre>"; print_r($baseUser); exit; ?>

<section class="body-section">
    <div class="container">
        <!--        <object type="text/html" data="http://validator.w3.org/" width="250px" height="200px" style="overflow:auto;border:5px ridge blue">
            </object>-->
        <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="mibble-short">
            <ul class="post-linl-list">
                <li class="active"><a href="{!! URL::to('addpost') !!}"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addphoto') !!}"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addlink') !!}"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addblog') !!}"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addvideo') !!}"><i class="fa fa-video-camera" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addtwitterlink') !!}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addinstagramlink') !!}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addmultipleimg') !!}"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></li>               
            </ul>
            <script>
            //$(document).ready(function() {
            //  $("#description").on('keyup', function() {
            //    var words = this.value.match(/\S+/g).length;
            //
            //    if (words > 140) {
            //      // Split the string on first 200 words and rejoin on spaces
            //      var trimmed = $(this).val().split(/\s+/, 140).join(" ");
            //      // Add a space at the end to make sure more typing creates new words
            //      $(this).val(trimmed + " ");
            //    }
            //    else {
            //      $('#display_count').text(140 - words);
            //      $('#word_left').text(140-words);
            //    }
            //  });
            //});   
            </script>            
            <script>
                function countChar(val) {
                  var text_max = 140;
                  $('#display_count').html(text_max);
                  var len = val.value.length;
                  if (len > text_max) {
                    val.value = val.value.substring(0, text_max);
                    $('#display_count').html(0);
                  } else {
                    //$('#charNum').text(20 - len);
                    $('#display_count').html(text_max - len);
                  }
                };
            </script>
            <form id="data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" >    
                <div class="form-place">
                    <h3 class="short-area-heading">Add Text</h3>
                    <div class="form-group">
                        <textarea class="form-control textarea-style" rows="5" onkeyup="countChar(this)" id="description" name="description" placeholder="What's Good"></textarea>
                        <span id="display_count">140</span> Character Remaining<br>
                        <span style="display: none; color: red; font-weight: bold" id="descriptionErrore" >Give Your Post.</span>
                    </div>
                    <div class="color-pic-place">
                        <div class="form-group">
                            <div class="col-sm-7 col-md-7">
                                <label for="email">Text Color</label>
                            </div>
                            <div class="col-sm-5 col-md-5">
                                <div class="form-group">
                                    <select class="form-control select-style" name="textcol" id="textcol" onchange="showValue(this)">
                                        <option value="" data-image="">Select</option>
                                        <option value="black" data-image="{!! asset('theme/site/images/black.gif') !!}">black</option>
                                        <option value="brown" data-image="{!! asset('theme/site/images/brown.gif') !!}">brown</option>
                                        <option value="burgundy" data-image="{!! asset('theme/site/images/burgundy.gif') !!}" name="cd">burgundy</option>
                                        <option value="cardinal" data-image="{!! asset('theme/site/images/cardinal.gif') !!}">cardinal</option>
                                        <option value="columbia_blue" data-image="{!! asset('theme/site/images/columbia_blue.gif') !!}">columbia blue</option>
                                        <option value="crimson" data-image="{!! asset('theme/site/images/crimson.gif') !!}">crimson</option>
                                        <option value="forest_green" data-image="{!! asset('theme/site/images/forest_green.gif') !!}">forest green</option>
                                        <option value="gold" data-image="{!! asset('theme/site/images/gold.gif') !!}">gold</option>
                                        <option value="gray" data-image="{!! asset('theme/site/images/gray.gif') !!}">gray</option>
                                        <option value="hunter_green" data-image="{!! asset('theme/site/images/hunter_green.gif') !!}">hunter green</option>
                                        <option value="kelly_green" data-image="{!! asset('theme/site/images/kelly_green.gif') !!}">kelly green</option>
                                        <option value="lime" data-image="{!! asset('theme/site/images/lime.gif') !!}" name="cd">lime</option>
                                        <option value="maroon" data-image="{!! asset('theme/site/images/maroon.gif') !!}" name="cd">maroon</option>
                                        <option value="orange" data-image="{!! asset('theme/site/images/orange.gif') !!}" name="cd">orange</option>
                                        <option value="pink" data-image="{!! asset('theme/site/images/pink.gif') !!}" name="cd">pink</option>
                                        <option value="purple" data-image="{!! asset('theme/site/images/purple.gif') !!}" name="cd">purple</option>
                                        <option value="red" data-image="{!! asset('theme/site/images/red.gif') !!}" name="cd">red</option>
                                        <option value="royal" data-image="{!! asset('theme/site/images/royal.gif') !!}" name="cd">royal</option>
                                        <option value="scarlet" data-image="{!! asset('theme/site/images/scarlet.gif') !!}" name="cd">scarlet</option>
                                        <option value="silver" data-image="{!! asset('theme/site/images/silver.gif') !!}" name="cd">silver</option>
                                        <option value="teal" data-image="{!! asset('theme/site/images/teal.gif') !!}" name="cd">teal</option>
                                        <option value="texas_orange" data-image="{!! asset('theme/site/images/texas_orange.gif') !!}" name="cd">texas orange</option>
                                        <option value="turquoise" data-image="{!! asset('theme/site/images/turquoise.gif') !!}" name="cd">turquoise</option>
                                        <option value="vegas_gold" data-image="{!! asset('theme/site/images/vegas_gold.gif') !!}" name="cd">vegas gold</option>
                                        <option value="white" data-image="{!! asset('theme/site/images/white.gif') !!}" name="cd">white</option>
                                        <option value="yellow" data-image="{!! asset('theme/site/images/yellow.gif') !!}" name="cd">yellow</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-7 col-md-7">
                                <label for="email">Background Color</label>
                            </div>
                            <div class="col-sm-5 col-md-5">
                                <div class="form-group">
                                    <select class="form-control select-style" name="bgcol" id="bgcol" onchange="showValue(this)">
                                        <option value="" data-image="">Select</option>
                                        <option value="black" data-image="{!! asset('theme/site/images/black.gif') !!}">black</option>
                                        <option value="brown" data-image="{!! asset('theme/site/images/brown.gif') !!}">brown</option>
                                        <option value="burgundy" data-image="{!! asset('theme/site/images/burgundy.gif') !!}" name="cd">burgundy</option>
                                        <option value="cardinal" data-image="{!! asset('theme/site/images/cardinal.gif') !!}">cardinal</option>
                                        <option value="columbia_blue" data-image="{!! asset('theme/site/images/columbia_blue.gif') !!}">columbia blue</option>
                                        <option value="crimson" data-image="{!! asset('theme/site/images/crimson.gif') !!}">crimson</option>
                                        <option value="forest_green" data-image="{!! asset('theme/site/images/forest_green.gif') !!}">forest green</option>
                                        <option value="gold" data-image="{!! asset('theme/site/images/gold.gif') !!}">gold</option>
                                        <option value="gray" data-image="{!! asset('theme/site/images/gray.gif') !!}">gray</option>
                                        <option value="hunter_green" data-image="{!! asset('theme/site/images/hunter_green.gif') !!}">hunter green</option>
                                        <option value="kelly_green" data-image="{!! asset('theme/site/images/kelly_green.gif') !!}">kelly green</option>
                                        <option value="lime" data-image="{!! asset('theme/site/images/lime.gif') !!}" name="cd">lime</option>
                                        <option value="maroon" data-image="{!! asset('theme/site/images/maroon.gif') !!}" name="cd">maroon</option>
                                        <option value="orange" data-image="{!! asset('theme/site/images/orange.gif') !!}" name="cd">orange</option>
                                        <option value="pink" data-image="{!! asset('theme/site/images/pink.gif') !!}" name="cd">pink</option>
                                        <option value="purple" data-image="{!! asset('theme/site/images/purple.gif') !!}" name="cd">purple</option>
                                        <option value="red" data-image="{!! asset('theme/site/images/red.gif') !!}" name="cd">red</option>
                                        <option value="royal" data-image="{!! asset('theme/site/images/royal.gif') !!}" name="cd">royal</option>
                                        <option value="scarlet" data-image="{!! asset('theme/site/images/scarlet.gif') !!}" name="cd">scarlet</option>
                                        <option value="silver" data-image="{!! asset('theme/site/images/silver.gif') !!}" name="cd">silver</option>
                                        <option value="teal" data-image="{!! asset('theme/site/images/teal.gif') !!}" name="cd">teal</option>
                                        <option value="texas_orange" data-image="{!! asset('theme/site/images/texas_orange.gif') !!}" name="cd">texas orange</option>
                                        <option value="turquoise" data-image="{!! asset('theme/site/images/turquoise.gif') !!}" name="cd">turquoise</option>
                                        <option value="vegas_gold" data-image="{!! asset('theme/site/images/vegas_gold.gif') !!}" name="cd">vegas gold</option>
                                        <option value="white" data-image="{!! asset('theme/site/images/white.gif') !!}" name="cd">white</option>
                                        <option value="yellow" data-image="{!! asset('theme/site/images/yellow.gif') !!}" name="cd">yellow</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!--color-pic-place-->
                    <div class="channel-dropdown">
                        <a href="#" data-toggle="collapse" data-target="#demo">
                            <div class="col-xs-8 col-sm-8 col-md-8">Add to channel</div>
                            <div class="col-xs-4 col-sm-4col-md-4 text-right"><i class="fa fa-bars" aria-hidden="true"></i>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    </div>
                    <ul id="demo" class="channel-list collapse" aria-expanded="false" style="height: 0px;">
                        <?php foreach ($chanel as $ch) { ?>
                            <li>
                                <div class="radio">
                                    <label class="checkbox">
                                        <input type="checkbox" name="category[]" value="<?php echo $ch['name'] ?>"><i></i><?php echo $ch['name'] ?></label>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                    <span style="display: none; color: red; font-weight: bold" id="categoryErrore" >Check Your Category.</span>
                    <script>
                        $(document).ready(function () {
                            $("#description").focus();
                            $("input[name='category[]']").change(function () {
                                var maxAllowed = 3;
                                var cnt = $("input[name='category[]']:checked").length;
                                if (cnt > maxAllowed) {
                                    $(this).prop("checked", "");
                                    //alert('You can select maximum ' + maxAllowed + ' technologies!!');
                                    $('#checkbox-limit').modal('show');
                                }
                            });
                        });
                    </script>                    
                    <div class="modal fade" id="checkbox-limit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 32%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Category Limit</h4>
                                </div>
                                <div class="modal-body">
                                    <p>You are reached limit to choose category. <b><i class="title"></i></b> Maximum limit of category is three.</p>
                                    <p>Delete one to add new category?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <!--<div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="row">
                            <div class="social-place">
                                <ul class="social-list">
                                    <li>
                                        <div class="radio">
                                            <label class="checkbox">
                                                <input type="checkbox" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon.jpg') !!}"></span>411 Media</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="radio">
                                            <label class="checkbox">
                                                <input type="checkbox" name="twittershare" id="twittershare" onclick="twitterShare(this, 'http://twitter.com/share?url=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>', 'Twitter', '500', '258')" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon3.jpg') !!}"></span>Twitter</label>
                                                <input type="checkbox" name="twittershare" onclick="popWindow('http://twitter.com/share?url=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>','Twitter','500', '258')" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon2.jpg') !!}"></span>Twitter</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="radio">
                                            <label class="checkbox">
                                                <input type="checkbox" name="facebookshare" id="facebookshare" onclick="facebookShare(this, 'http://www.facebook.com/sharer.php?u=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>', 'Facebook', '500', '400')" value="1" ><i></i><span><img src="{!! asset('theme/site/images/social-icon2.jpg') !!}"></span>Facebook</label>
                                                <input type="checkbox" name="facebookshare" onclick="popWindow('http://www.facebook.com/sharer.php?u=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>', 'Facebook','500','400')" value="1" ><i></i><span><img src="{!! asset('theme/site/images/social-icon3.jpg') !!}"></span>Facebook</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class="top-addvertisement margin-half"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
                    </div>
                    <ul class="button-list button-list-big">
                        <li>
                            <input type="submit" class="pop-button login-buttom" value="Share Post">
                        </li>
                    </ul>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="modal fade" id="confirm-post" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 32%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabelhead">Share Your Post</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="social-place">
                            <ul class="social-list">
                                <li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" name="twittershare" id="twittershare" value="1">
                                            <i></i><span><img src="{!! asset('theme/site/images/social-icon3.jpg') !!}"></span>Twitter</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" name="facebookshare" id="facebookshare" value="1" >
                                            <i></i><span><img src="{!! asset('theme/site/images/social-icon2.jpg') !!}"></span>Facebook</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="confirmpost" value="" >
            <div class="modal-footer">
                <button type="button" onclick="goReturnUrlLink()" class="btn btn-default" data-dismiss="modal">Share</button>
            </div>
        </div>
    </div>
</div>
<script>
    function twitterShareNow(url, winName, w, h) {
        if ($('#twittershare').is(':checked')) {
            //alert("Checkbox is checked.");
            popWindow(url, winName, w, h);
        }
    }
    
    function goReturnUrlLink() {
        var confirmpost = $('#confirmpost').val();
        window.location.href = confirmpost;
    }    
 
    function facebookShareNow(url, winName, w, h) {
        if ($('#facebookshare').is(':checked')) {
            popWindow(url, winName, w, h);
        }
    }

    function popWindow(url, winName, w, h) {
        if (window.open) {
            if (poppedWindow) {
                poppedWindow = '';
            }
            windowW = w;
            windowH = h;
            var windowX = (screen.width / 2) - (windowW / 2);
            var windowY = (screen.height / 2) - (windowH / 2);
            var myExtra = "status=no,menubar=no,resizable=yes,toolbar=no,scrollbars=yes,addressbar=no";
            var poppedWindow = window.open(url, winName, 'width=' + w + ',height=' + h + ',top=' + windowY + ',left=' + windowX + ',' + myExtra + '');
        } else {
            alert('Your security settings are not allowing our popup windows to function. Please make sure your security software allows popup windows to be opened by this web application.');
        }
        return false;
    }

    $("form#data").submit(function (event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var description = $('#description').val();
        var category = $("[name='category[]']:checked").length;
        var adult =  $("[name='is_adult']:checked").length;
        //alert(category);
        var conf = true;
        if (description == '') {
            conf = false;
            $("#descriptionErrore").show("slow");
        } else {
            conf = true;
            $("#descriptionErrore").hide("slow");
        }
        //if(category < 1){ conf = false; $("#categoryErrore").show("slow"); } else { conf = true; $("#categoryErrore").hide("slow"); }
        if (conf) {
            $.ajax({
                url: "{{ Config::get('config.baseurl') }}savepost",
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (returndata) {

                    if(adult != 1){
                        var shurl = "http://xigma.website/dev10/4eleven/public/showpost/" + returndata; 
                    } else {
                        var shurl = "http://xigma.website/dev10/4elevenadult/public/adult/showpost/" + returndata; 
                    }                    

                    //var shurl = "http://ornointeriors.com.au/product/detail/1086";
                    //var shurl = "http://xigma.website/dev10/4eleven/showphoto/NzQ="; 
                    $('#twittershare').attr("onclick", "twitterShareNow('http://twitter.com/share?url=" + shurl + "', 'Twitter', '500', '258')");
                    $('#facebookshare').attr("onclick", "facebookShareNow('http://www.facebook.com/sharer.php?u=" + shurl + "', 'Facebook', '500', '400')");
                    $('#description').val('');
                    $('#textcol').val('');
                    $('#bgcol').val('');
                    $("input[name='category[]']:checkbox").removeAttr('checked');
                    $('#confirmpost').val(shurl);
                    $('#confirm-post').modal('show');
                }
            });
        } 
        return false;
    });
</script>
<script>
    $(document).ready(function (e) {
        //convert
        $("select").msDropdown({roundedBorder: false});
        createByJson();
        $("#tech").data("dd");
        $("#tech2").data("dd");
    });
</script>
@stop