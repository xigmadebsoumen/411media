@extends('_layouts.post')
@section('content')
<?php

function getUrlData($url)
{
    $result = false;
   
    $contents = getUrlContents($url);

    if (isset($contents) && is_string($contents))
    {
        $title = null;
        $metaTags = null;
       
        preg_match('/<title>([^>]*)<\/title>/si', $contents, $match );

        if (isset($match) && is_array($match) && count($match) > 0)
        {
            $title = strip_tags($match[1]);
        }
       
        preg_match_all('/<[\s]*meta[\s]*name="?' . '([^>"]*)"?[\s]*' . 'content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', $contents, $match);
       

   


        if (isset($match) && is_array($match) && count($match) == 3)
        {
            $originals = $match[0];
            $names = $match[1];
            $values = $match[2];
           
            if (count($originals) == count($names) && count($names) == count($values))
            {
                $metaTags = array();
               
                for ($i=0, $limiti=count($names); $i < $limiti; $i++)
                {
                    $metaTags[$names[$i]] = array (
                        'html' => htmlentities($originals[$i]),
                        'value' => $values[$i]
                    );
                }
            }
        }
       
        $result = array (
            'title' => $title,
            'metaTags' => $metaTags
        );
    }
   
    return $result;
}

function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0)
{
    $result = false;
   
    $contents = @file_get_contents($url);
   
    // Check if we need to go somewhere else
   
    if (isset($contents) && is_string($contents))
    {
        preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);
       
        if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1)
        {
            if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections)
            {
                return getUrlContents($match[1][0], $maximumRedirections, ++$currentRedirection);
            }
           
            $result = false;
        }
        else
        {
            $result = $contents;
        }
    }

    return $contents;
}
?>
<section class="body-section">
    <div class="container-fluid">
        <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="post-place time-line-area">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <div class="row">
                    <div class="mobile-view">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <div class="row">
                                <div class="post-profile-details">
                                    <a href="javascript:void(0)">
                                        <div class="message-pic">
                                            <?php if($dt['User']->image != ""){ ?>
                                                <img src="{!! asset('products/crop/user/'.$dt['User']->image) !!}">
                                            <?php } else { ?>
                                                <img src="{!! asset('devimg/noimg.png') !!}">
                                            <?php } ?>
                                        </div>
                                        <div class="messagedetails">
                                             <span class="messagename"><?php echo $dt['User']->fname." ".$dt['User']->lname;?></span>
                                            <span class="message-short black margin-top"><?php echo  date("d M Y",strtotime($dt['Post']->created_at));?></span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php if (Auth::check()) { ?>
                        <?php $udt = Auth::user(); ?>
                        <?php if($udt->id == $dt['User']->id){ ?>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <a href="javascript:void(0)" class="three-dropdown text-right" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ URL::to('delete/'.$dt['Post']->id) }}">Delete</a></li>
                                    <?php if($dt['Post']->is_turnoff_comments == 0){ ?>
                                        <li><a href="{{ URL::to('turnoffcomments/'.$dt['Post']->id) }}">Turn off Comments</a></li>
                                    <?php } else { ?>
                                        <li><a href="{{ URL::to('turnoncomments/'.$dt['Post']->id) }}">Turn on Comments</a></li>
                                    <?php } ?>                                

                                    <?php if($dt['Post']->is_turnoff_share == 0){ ?>
                                        <li><a href="{{ URL::to('turnoffshare/'.$dt['Post']->id) }}">Turn off Sharing</a></li>
                                    <?php } else { ?>
                                        <li><a href="{{ URL::to('turnonshare/'.$dt['Post']->id) }}">Turn on Sharing</a></li>
                                    <?php } ?>
                                    <li><a href="javascript:void(0)">Monetize</a></li>
                            </ul>
                            <span class="views text-right"><?php echo $views?> Views</span>
                        </div>
                        <?php } else { ?>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <a href="javascript:void(0)" class="three-dropdown text-right" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ URL::to('report/'.$dt['Post']->id) }}">Report</a></li>
                            </ul>
                            <span class="views text-right"><?php echo $views?> Views</span>
                        </div>                  
                        <?php } }?> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="short-port">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                            <div class="mob-logo"><img src="{!! asset('theme/site/images/social-icon.jpg') !!}">
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8  ">
                            <span class="messagename "><?php echo $dt['User']->pname;?></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!--short-port-->
<?php
$image_or_video='';
$imgpatharry = array();

$linkurl=trim($dt['Post']->link);
//$linkurl="https://www.instagram.com/p/BQTReqtgNkR/?tagged=bryanadamsgetup";

$result = getUrlData($linkurl);


$stitle = @$result['title'];
$sdescription = @$result['metaTags']['description']['value'];  
$hostname=@parse_url($linkurl, PHP_URL_HOST);


$forimages= @file_get_contents($linkurl);
preg_match_all( '|<img.*?src=[\'"](.*?)[\'"].*?>|i',$forimages, $allimages);

//print_r($allimages);

//echo '<img src="' .$linkurl. $allimages[ 1 ][ 0 ] . '" />';
$totalimgscount=count($allimages[ 0 ]);

if($totalimgscount>0){
for($i=0;$i<$totalimgscount;$i++){
//echo '<pre>';
//$img = str_replace(":/","://",str_replace("//","/",$linkurl.$allimages[ 1 ][ $i ]));
$img = @$allimages[ 1 ][ $i ];
$imgdtls=@getimagesize($img);
//print_r($imgdtls);
//echo '</pre>';
if($imgdtls[0]>=150 && $imgdtls[1]>=150){
//$imgpath = $img;
array_push($imgpatharry,$img);
$image_or_video='itsaimage';
}

}
}
else{

$image_or_video='itsavdo';

}
//print_r($imgpatharry);
//echo $image_or_video;
?>

                    <figure class="photo-image">
                        
<?php
if(count($imgpatharry)>0 && $image_or_video=="itsaimage"){

?>
                            <div class="first-half-image-post">
                                <!--<img src="images/o-TEENS-HAVI.jpg">-->
                                <!--<object type="text/html" data="<?php echo $dt['Post']->link;?>" width="100%" height="100%" style="overflow:hidden;">
    </object>
<iframe border=0 frameborder=0 width="100%" height="600"  scrolling="no" src="<?php echo $dt['Post']->link;?>"></iframe>-->

<img src="<?php echo $imgpatharry[ 0 ] ?>" />
                            </div>
<?php
}
else{
?>
<!--twitter:player <meta name="twitter:player"-->

<iframe width="100%" height="100%" style="width:550px;height:450px" scrolling="no" src="<?php //echo $linkurl;?>" frameborder="0" allowfullscreen></iframe>

<?php
}
?>

                            <div class="first-half-content-post">
<h2 class="first-half-heading"><?php echo $stitle ;?> </h2>
                                <?php echo $sdescription;?>
<div class="post-link"><a href="<?php echo $linkurl;?>" target="_blank"><?php echo $hostname;?></a></div>
                            </div>
                        
                    </figure>
                    <div class="mobile-view">
                        <ul class="three-list">
                            <?php if (empty($getLike)) { ?>
                            <li><a href="javascript:void(0)" onclick="likenow('<?php echo $id?>','<?php echo $dt['User']->id?>','<?php echo $users->id?>')" >Like</a></li>
                            <?php } else { ?>
                            <li><a href="javascript:void(0)" >Like</a></li>
                            <?php } ?>

                            <?php if($dt['Post']->is_turnoff_comments == 0){ ?>
                                <li><a href="{{ URL::to('comment/'.base64_encode($dt['Post']->id)) }}">Comment</a></li>
                            <?php } else { ?>
                                <li><a href="javascript:void(0)">Comment</a></li>
                            <?php } ?>                    

                            <?php if($dt['Post']->is_turnoff_share == 0){ ?>
                                <li><a href="javascript:void(0)" onclick="sharenow('<?php echo $dt['Post']->pid != 0 ? $dt['Post']->pid : $id?>','<?php echo $users->id?>')" >Share</a></li>
                            <?php } else { ?>
                            <li><a href="javascript:void(0)">Share</a></li>
                            <?php } ?>
                        </ul>
                        <ul class="three-list-pic"> 
                            <li><a href="{{ URL::to('likelist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-smile-o" aria-hidden="true"></i> <?php echo $like;?></a></li>
                            <li><a href="{{ URL::to('commentlist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-comments" aria-hidden="true"></i> <?php echo $comments;?></a></li>
                            <li><a href="{{ URL::to('sharelist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-share-alt" aria-hidden="true"></i> <?php echo $share;?></a></li>
                        </ul> 
                    </div>
                    <figcaption class="photo-caption">
                        <?php echo $dt['Post']->description?>
                    </figcaption>
<!--                        <div class="sharing-section">
                            <ul>
                                <?php if (empty($getLike)) { ?>
                                <li><a href="javascript:void(0)" onclick="likenow('<?php echo $id?>','<?php echo $dt['User']->id?>','<?php echo $users->id?>')"> <i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
                                <?php } else { ?>
                                <li><a href="javascript:void(0)" onclick="unlikenow('<?php echo $id?>','<?php echo $users->id?>')"><i class="fa fa-thumbs-down" aria-hidden="true"></i> Unlike</a></li>
                                <?php } ?>
                                <?php if($dt['Post']->is_turnoff_share == 0){ ?>
                                    <li><a href="javascript:void(0)" onclick="sharenow('<?php echo $dt['Post']->pid != 0 ? $dt['Post']->pid : $id?>','<?php echo $users->id?>')"><i class="fa fa-share" aria-hidden="true"></i> Share</a></li>
                                <?php } ?>
                            </ul>
                        </div>-->
                        <script type="text/javascript">
                            function likenow(pid,puid,uid){
                                url = "{!! URL::to('likenow') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", pid: pid, puid:puid, uid: uid},
                                }).done(function (response) {
                                    //alert(response);
                                    window.location.reload();
                                });                            
                            }

                            function unlikenow(pid,puid,uid){
                                url = "{!! URL::to('unlikenow') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", pid: pid,puid:puid, uid: uid},
                                }).done(function (response) {
                                    window.location.reload();
                                });                           
                            } 
                            
                            function sharenow(pid,uid){
                                //alert(11);
                                url = "{!! URL::to('sharenow') !!}";
                                $.ajax({
                                    url: url,
                                    async: false,
                                    type: 'POST',
                                    data: {_token: "{{ csrf_token() }}", pid: pid, uid: uid},
                                }).done(function (response) {
                                    //alert(response);
                                    $('#confirm-share').modal('show');
                                    
                                });                            
                            }            
                            
                            function closeModal(){
                                $('#confirm-share').modal('hide');
                                window.location.reload();
                            }
                            
                        </script>                     
                    
                        
                        <div class="modal fade" id="confirm-share" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" style="width: 32%;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                                        <h4 class="modal-title" id="myModalLabelhead">Shared Successfully </h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Post Shared Successfully.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" onclick="closeModal()" class="btn btn-default">Exit</button>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    <div class="single-add">
                        <img src="{!! asset('theme/site/images/add02.jpg') !!}">
                    </div>
	
                    <a href="{{ URL::to('commentlist/'.base64_encode($dt['Post']->id)) }}" class="comments-button"> <i class="fa fa-comments" aria-hidden="true"></i>Show Comments</a>
<!--                    <?php if($dt['Post']->is_turnoff_comments == 0){ ?>
                    {!! Form::open(array('route'=>'site.post.savecomment', 'role'=>'form', 'class'=>'form-horizontal', 'method' => 'post','id' => 'Formvalidate','files' => true)) !!}
                    <input type="hidden" name="pid" value="<?php echo $dt['Post']->id;?>" >
                    <input type="hidden" name="uid" value="<?php echo $users->id;?>" >
                    <input type="hidden" name="redirect" value="showalbum" >
                    <figcaption class="writw-comments">
                        
                             <div class="comment-img">
                                 {!! (isset($users->image) && $users->image != "") ? Html::image('products/main/user/'.$users->image, $users->site_name, array('width' => 30, 'height' => 30))  : Html::image('devimg/blankuser.png', $users->site_name, array('width' => 30, 'height' => 30))  !!}
                             </div>
                            <div class="comment-writen"> 
                                <textarea id="comment" name="comment" rows="2" cols="25" ></textarea>  
                            </div>
                            <input type="submit" class="comment-btm" value="Comment">
                        
                    </figcaption>
                    {!! Form::close() !!} 
                    <?php } ?>                   -->             
                    <div class="sponser-place sponser-place-add">
                        <ul class="sponser-list">
                            <li><img src="{!! asset('theme/site/images/add01.jpg') !!}" class="img-responsive">
                            </li>
                            <li><img src="{!! asset('theme/site/images/add01.jpg') !!}" class="img-responsive">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="post-top">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <div class="row">
                            <div class="post-profile-details">
                                <a href="javascript:void(0)">
                                    <div class="message-pic">
                                    <?php if($dt['User']->image != ""){ ?>
                                        <img src="{!! asset('products/crop/user/'.$dt['User']->image) !!}">
                                    <?php } else { ?>
                                        <img src="{!! asset('devimg/noimg.png') !!}">
                                    <?php } ?>
                                    </div>
                                    <div class="messagedetails">
                                        <span class="messagename"><?php echo $dt['User']->fname." ".$dt['User']->lname;?></span>
                                        <span class="message-short black margin-top"><?php echo  date("d M Y",strtotime($dt['Post']->created_at));?></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                        <?php if (Auth::check()) { ?>
                        <?php $udt = Auth::user(); ?>
                        <?php if($udt->id == $dt['User']->id){ ?>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <a href="javascript:void(0)" class="three-dropdown text-right" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ URL::to('delete/'.$dt['Post']->id) }}">Delete</a></li>
                                    <?php if($dt['Post']->is_turnoff_comments == 0){ ?>
                                        <li><a href="{{ URL::to('turnoffcomments/'.$dt['Post']->id) }}">Turn off Comments</a></li>
                                    <?php } else { ?>
                                        <li><a href="{{ URL::to('turnoncomments/'.$dt['Post']->id) }}">Turn on Comments</a></li>
                                    <?php } ?>                                

                                    <?php if($dt['Post']->is_turnoff_share == 0){ ?>
                                        <li><a href="{{ URL::to('turnoffshare/'.$dt['Post']->id) }}">Turn off Sharing</a></li>
                                    <?php } else { ?>
                                        <li><a href="{{ URL::to('turnonshare/'.$dt['Post']->id) }}">Turn on Sharing</a></li>
                                    <?php } ?>
                                    <li><a href="javascript:void(0)">Monetize</a></li>
                            </ul>
                            <span class="views text-right"><?php echo $views?> Views</span>
                        </div>
                        <?php } else { ?>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <a href="javascript:void(0)" class="three-dropdown text-right" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ URL::to('report/'.$dt['Post']->id) }}">Report</a></li>
                            </ul>
                            <span class="views text-right"><?php echo $views?> Views</span>
                        </div>                  
                        <?php } }?> 
                </div>
                <div class="clearfix"></div>
                <ul class="three-list">
                    <?php if (empty($getLike)) { ?>
                    <li><a href="javascript:void(0)" onclick="likenow('<?php echo $id?>','<?php echo $dt['User']->id?>','<?php echo $users->id?>')" >Like</a></li>
                    <?php } else { ?>
                    <li><a href="javascript:void(0)" >Like</a></li>
                    <?php } ?>
                    
                    <?php if($dt['Post']->is_turnoff_comments == 0){ ?>
                        <li><a href="{{ URL::to('comment/'.base64_encode($dt['Post']->id)) }}">Comment</a></li>
                    <?php } else { ?>
                        <li><a href="javascript:void(0)">Comment</a></li>
                    <?php } ?>                    

                    <?php if($dt['Post']->is_turnoff_share == 0){ ?>
                        <li><a href="javascript:void(0)" onclick="sharenow('<?php echo $dt['Post']->pid != 0 ? $dt['Post']->pid : $id?>','<?php echo $users->id?>')" >Share</a></li>
                    <?php } else { ?>
                    <li><a href="javascript:void(0)">Share</a></li>
                    <?php } ?>
                </ul>
                <ul class="three-list-pic"> 
                    <li><a href="{{ URL::to('likelist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-smile-o" aria-hidden="true"></i> <?php echo $like;?></a></li>
                    <li><a href="{{ URL::to('commentlist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-comments" aria-hidden="true"></i> <?php echo $comments;?></a></li>
                    <li><a href="{{ URL::to('sharelist/'.base64_encode($dt['Post']->id)) }}"><i class="fa fa-share-alt" aria-hidden="true"></i> <?php echo $share;?></a></li>
                </ul>
                <hr>
                <ul class="add-list">
                    <li><img src="{!! asset('theme/site/images/add01.jpg') !!}" class="img-responsive">
                    </li>
                    <li><img src="{!! asset('theme/site/images/add02.jpg') !!}" class="img-responsive">
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
@stop