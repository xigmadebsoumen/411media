@extends('_layouts.post')
@section('content')
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="mibble-short">
            <div class="sear-area">
                <div class="form-group">
                    <div class="col-xs-1 col-sm-1 col-md-1">
                        <div class="row">
                            <!--<label for="email" class="label-style mediun-size">To</label>-->
                            <a href="{{ URL::previous() }}">Go Back</a>
                            <span> Comments </span>
                            <span> <?php echo count($dtarr)?> Shares  </span>
                        </div>
                    </div>
                    <!--
                    <div class="col-xs-11 col-sm-11 col-md-11">
                        <div class="row">
                            <input type="text" class="form-control inputstyle" id="email" placeholder="Search...">
                        </div>
                    </div>-->
                    <div class="clearfix"></div>
                </div>
            </div>
            <ul class="fl-search-list">
                <?php foreach($dtarr as $arr){ ?>
                <li>
                    <?php if($udt->id != $arr['You']->id){ ?>
                        <a href="{{ URL::to('searchprofile/'.$arr['You']->pname) }}">
                    <?php } else { ?>
                        <a href="javascript:void(0)">
                    <?php } ?>
                        <div class="message-pic">
                            <?php if($arr['You']->image != ""){ ?>
                                <?php if (file_exists(public_path('products/main/user/' . $arr['You']->image))) { ?>
                                    <img src="{!! asset('products/main/user/'.$arr['You']->image) !!}">
                                <?php } else { ?>
                                    <img src="{!! asset('devimg/noimg.png') !!}">
                                <?php } ?>
                            <?php } else { ?>
                                <img src="{!! asset('devimg/noimg.png') !!}">
                            <?php } ?>                            
                        </div>
                        <div class="messagedetails">
                            <span class="messagename"><?php echo $arr['You']->pname; ?></span>
                        </div>
                        
                    </a>
					<div class="messagedetails-comments">
					
                        <?php echo $arr['Comment']; ?>
						
						<ul class="three-share">
								<li><a href="#">like</a></li>
								<li><a href="#">Comment</a></li>
								<li><a href="#">Share</a></li>
						</ul>
                    </div>
					
					<div class="flo-btm">
                    <?php if($arr['Isme'] != 1){ ?>
                        <?php if($arr['Following'] != 0){ ?>
                            <a href="javascript:void(0)" onclick="unfollownow('<?php echo $arr['You']->id?>','<?php echo $arr['Me']->id?>')" class="folow-button color-green"><span>Following</span></a>
                        <?php } else { ?>
                            <a href="javascript:void(0)" onclick="follownow('<?php echo $arr['You']->id?>','<?php echo $arr['Me']->id?>')" class="folow-button color-green"><span>Follow</span></a>
                        <?php } ?>

                        <?php if($arr['Follower'] != 0){ ?> 
                            <a href="javascript:void(0)" class="folow-button color-green"><span>Follower</span></a>
                        <?php } ?>                    
                    <?php } ?>
					</div>
					
                </li>
                <?php } ?>
                <script type="text/javascript">
                    function follownow(followto,followby){
                        //alert(11);
                        url = "{!! URL::to('follownow') !!}";
                        $.ajax({
                            url: url,
                            async: false,
                            type: 'POST',
                            data: {_token: "{{ csrf_token() }}", followto: followto, followby: followby},
                        }).done(function (response) {
                            //alert(response);
                            window.location.reload();
                        });                            
                    }

                    function unfollownow(followto,followby){
                        url = "{!! URL::to('unfollownow') !!}";
                        $.ajax({
                            url: url,
                            async: false,
                            type: 'POST',
                            data: {_token: "{{ csrf_token() }}", followto: followto, followby: followby},
                        }).done(function (response) {
                            window.location.reload();
                        });                           
                    }                        
                </script>                
                
<!--                <a href="javascript:void(0)" class="folow-button color-green"><span> Follow</span></a>
                <a href="javascript:void(0)" class="folow-button"><span> Following</span></a>-->
                
                <!-- <li>
                    <a href="javascript:void(0)">
                        <div class="message-pic">
                            <img src="{!! asset('theme/site/images/profile01.jpg') !!}">
                        </div>
                        <div class="messagedetails">
                            <span class="messagename">Ladla Malik</span>
                        </div>
                    </a>
                    <a href="javascript:void(0)" class="folow-button"><span> Following</span></a>
                </li> -->
            </ul>
        </div>
    </div>
</section>
@stop