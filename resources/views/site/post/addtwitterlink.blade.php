@extends('_layouts.post')
@section('content')
    <section class="body-section">
        <div class="container">
            <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
            </div>
            <div class="mibble-short">
            <ul class="post-linl-list">
                <li class="active"><a href="{!! URL::to('addpost') !!}"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addphoto') !!}"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addlink') !!}"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addblog') !!}"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addvideo') !!}"><i class="fa fa-video-camera" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addtwitterlink') !!}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addinstagramlink') !!}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addmultipleimg') !!}"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></li>               
            </ul>
            <form id="data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                    <div class="form-place">
                        <h3 class="short-area-heading"> Add Tweet </h3>
                        <div class="form-group">
                            <input type="text" class="form-control inputstyle" id="link" name="link" placeholder="Paste tweet embed code.">
                            <span style="display: none; color: red; font-weight: bold" id="linkErrore" >Give Your Link.</span>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control textarea-style" onkeyup="countChar(this)" rows="5" id="description" name="description" placeholder="Description"></textarea>
                            <span id="display_count">140</span> Character Remaining<br>
                            <span style="display: none; color: red; font-weight: bold" id="descriptionErrore" >Give Your Description.</span>
                        </div>
                        
                        <script>
                        //$(document).ready(function() {
                        //  $("#description").on('keyup', function() {
                        //    var words = this.value.match(/\S+/g).length;
                        //
                        //    if (words > 140) {
                        //      // Split the string on first 200 words and rejoin on spaces
                        //      var trimmed = $(this).val().split(/\s+/, 140).join(" ");
                        //      // Add a space at the end to make sure more typing creates new words
                        //      $(this).val(trimmed + " ");
                        //    }
                        //    else {
                        //      $('#display_count').text(140 - words);
                        //      $('#word_left').text(140-words);
                        //    }
                        //  });
                        //});   
                        </script>          
                        <script>
                            function countChar(val) {
                              var text_max = 140;
                              $('#display_count').html(text_max);
                              var len = val.value.length;
                              if (len > text_max) {
                                val.value = val.value.substring(0, text_max);
                                $('#display_count').html(0);
                              } else {
                                //$('#charNum').text(20 - len);
                                $('#display_count').html(text_max - len);
                              }
                            };
                        </script>                        
                        
                        
                        <div class="clearfix"></div>
                        <span class="postcontent-list">
                            <div class="radio">
                                <label class="checkbox"><input type="checkbox" name="is_adult" id="is_adult" value="1"><i></i>Post contains adult content</label>
                            </div>
			</span>
                        <div class="channel-dropdown">
                            <a href="#" data-toggle="collapse" data-target="#demo">
                                <div class="col-xs-8 col-sm-8 col-md-8">Add to channel</div>
                                <div class="col-xs-4 col-sm-4col-md-4 text-right"><i class="fa fa-bars" aria-hidden="true"></i>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </div>
                        <ul id="demo" class="channel-list collapse" aria-expanded="false" style="height: 0px;">
                            <?php foreach($chanel as $ch){?>
                            <li>
                                <div class="radio">
                                    <label class="checkbox">
                                        <input type="checkbox" name="category[]" value="<?php echo $ch['name']?>"><i></i><?php echo $ch['name']?></label>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <span style="display: none; color: red; font-weight: bold" id="categoryErrore" >Check Your Category.</span>
                        <script>
                        $(document).ready(function () {
                            $("input[name='category[]']").change(function () {
                                var maxAllowed = 3;
                                var cnt = $("input[name='category[]']:checked").length;
                                if (cnt > maxAllowed) {
                                    $(this).prop("checked", "");
                                    //alert('You can select maximum ' + maxAllowed + ' technologies!!');
                                    $('#checkbox-limit').modal('show');
                                }
                            });
                        });
                        </script>
                        <div class="modal fade" id="checkbox-limit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" style="width: 32%;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Category Limit</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>You are reached limit to choose category. <b><i class="title"></i></b> Maximum limit of category is three.</p>
                                        <p>Delete one to add new category?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>                         
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="row">
                                <div class="social-place">
                                    <ul class="social-list">
                                        <li>
                                            <div class="radio">
                                                <label class="checkbox">
                                                    <input type="checkbox" id="is_timeline" name="is_timeline" value="n"><i></i><span><img src="{!! asset('theme/site/images/social-icon.jpg') !!}"></span>411 Media</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="top-addvertisement margin-half"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
                        </div>
                        <ul class="button-list button-list-big">
                            <li>
                                <input type="submit" class="pop-button login-buttom" value="Share Post">
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </section>
<div class="modal fade" id="confirm-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 32%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabelhead">Success Message</h4>
            </div>
            <div class="modal-body">
<!--                <h4 class="modal-title" id="myModalLabelhead">Link Posted Successfully.</h4>-->
                <!--<div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="social-place">
                            <ul class="social-list">                     
                                <li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" name="twittershare" id="twittershare" value="1">
                                            <i></i><span><img src="{!! asset('theme/site/images/social-icon3.jpg') !!}"></span>Twitter</label>

                                    </div>
                                </li>
                                <li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" name="facebookshare" id="facebookshare" value="1" >
                                            <i></i><span><img src="{!! asset('theme/site/images/social-icon2.jpg') !!}"></span>Facebook</label>
                                    </div>
                                </li>                                
                                
                            </ul>
                        </div>
                    </div>
                </div>-->
            </div>
            <div class="modal-footer">
                <button type="button" onclick="location.reload()" class="btn btn-default" data-dismiss="modal">Share</button>
            </div>
        </div>
    </div>
</div>
<script>
    function twitterShareNow(url, winName, w, h) {
        if ($('#twittershare').is(':checked')) {
            //alert("Checkbox is checked.");
            popWindow(url, winName, w, h);
        }
    }
    function facebookShareNow(url, winName, w, h) {
        if ($('#facebookshare').is(':checked')) {
            //alert("Checkbox is checked.");
            popWindow(url, winName, w, h);
        }
    }

    function popWindow(url,winName,w,h) {
        if (window.open) {
            if (poppedWindow) { poppedWindow = ''; }
            windowW = w;
            windowH = h;
            var windowX = (screen.width/2)-(windowW/2);
            var windowY = (screen.height/2)-(windowH/2);
            var myExtra = "status=no,menubar=no,resizable=yes,toolbar=no,scrollbars=yes,addressbar=no";
            var poppedWindow = window.open(url,winName,'width='+w+',height='+h+',top='+windowY+',left=' + windowX + ',' + myExtra + '');
        } else {
            alert('Your security settings are not allowing our popup windows to function. Please make sure your security software allows popup windows to be opened by this web application.');
        }
        return false;
    }
    
    $("form#data").submit(function (event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var link = $('#link').val();
        var description = $('#description').val();
        var category = $("[name='category[]']:checked").length;
        //alert(category);
        var conf = true;
        if(link == ''){ conf = false; $("#linkErrore").show("slow"); } else { conf = true; $("#linkErrore").hide("slow"); }
        if(description == ''){ conf = false; $("#descriptionErrore").show("slow"); } else { conf = true; $("#descriptionErrore").hide("slow"); }
        //if(category < 1){ conf = false; $("#categoryErrore").show("slow"); } else { conf = true; $("#categoryErrore").hide("slow"); }
        if(conf){
            $.ajax({
                url: "{{ Config::get('config.baseurl') }}savetwitterlink",
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (returndata) {
                    //alert(returndata);
                    //var shurl = "{!! URL::to('showpost') !!}/" + returndata; 
                    //$('#twittershare').attr("onclick", "twitterShareNow('http://twitter.com/share?url=" + shurl + "', 'Twitter', '500', '258')");
                    //$('#facebookshare').attr("onclick", "facebookShareNow('http://www.facebook.com/sharer.php?u=" + shurl + "', 'Facebook', '500', '400')");                    
                    //$('#twittershare').attr("onclick", "twitterShare(this, 'http://twitter.com/share?url=<?php echo 'http://ornointeriors.com.au/product/index/section/315'; ?>', 'Twitter', '500', '258')");
                    //$('#facebookshare').attr("onclick", "facebookShare(this, 'http://www.facebook.com/sharer.php?u=<?php echo 'http://ornointeriors.com.au/product/index/section/315'; ?>', 'Facebook', '500', '400')");
                    $('#link').val('');
                    $('#description').val('');
                    $("input[name='is_adult']:checkbox").removeAttr('checked');
                    $("input[name='category[]']:checkbox").removeAttr('checked');
                    $("input[name='is_timeline']:checkbox").removeAttr('checked');
                    $('#confirm-link').modal('show');
                }
            });
        }
        return false;
    });
    
</script>
@stop