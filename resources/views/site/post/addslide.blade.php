@extends('_layouts.post')
@section('content')
<?php $udt = Auth::user(); ?>
<div id="embed-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content modal-style">
            <span class="cross-btm flaticon-error" class="close" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <input type="text" class="form-control inputstyle" onkeyup="fetchData(this.value)" id="embedv" placeholder="Embed code here ">
                    </div>
                </div>
                <input type="button" onclick="getEmbedVal()" value="Save" class="pop-button">
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="mibble-short">
            <ul class="post-linl-list">
                <li class="active"><a href="{!! URL::to('addpost') !!}"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addphoto') !!}"><i class="fa fa-camera" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addlink') !!}"><i class="fa fa-link" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addblog') !!}"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addvideo') !!}"><i class="fa fa-video-camera" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addtwitterlink') !!}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addinstagramlink') !!}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="{!! URL::to('addmultipleimg') !!}"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></li>               
            </ul>
            <form id="data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                <div class="form-place">
                    <h3 class="short-area-heading">Add Slide Show</h3>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="row">
                            <span class="upload-left" id="fileselector">
                                <label class="btn up-btm" for="upload-file-selector">
                                    <input id="img" name="image[]" accept="image/png,image/jpg,image/jpeg,image/bmp" multiple="multiple" type="file">
                                    <i class="fa fa-upload" aria-hidden="true"></i> <span id="imgDtVal">Upload </span>
                                </label>
                                
                                <span style="display: none; color: red; font-weight: bold" id="imageErrore" >Upload Image.</span>
                            </span>
                        </div>
                    </div><br><br>
                    <span font-weight: bold" id="imageErrore" >Upload Multiple Images by holding SHIFT button.</span>
                    <script>
//                        document.getElementById("img").onchange = function () {
//                           $('#imgDtVal').html("Uploaded");
//                        };                         
                        $(document).ready(function () {
                            $('input[type=file]').change(function(){
                                var file = this.files[0];
                                //var name = file.name;
                                //var size = file.size;
                                var type = file.type;
                                //alert(type);
                                var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                                    //$('#img').val('');
                                    //$('input[type=file]').val('');
                                    $('#img-extvalidation').modal('show');
                                } else {
                                    $('#imgDtVal').html("Uploaded");
                                }
                            });                            
                        });
                    </script>
                    <div class="modal fade" id="img-extvalidation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 32%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Upload Image Errore</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Your uploaded image file extension not valid!!. <b><i class="title"></i></b> Please upload .png, .jpg, .jpeg, .bmp extension file.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <!--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="row">
                            <span class="cross-icon orblack">Or</span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="row">
                            <a href="javascript:void(0)" class="emb-btm" data-toggle="modal" data-target="#embed-modal"><i class="fa fa-link" aria-hidden="true"></i> <span>Embed </span></a>
                        </div>
                    </div>
                     <input type="hidden" name="embed" id="embed" value="" >
                    <span style="display: none; color: red; font-weight: bold" id="imageErrore" >Upload Image or Embed Image.</span> -->
                    <div class="clearfix"></div>
                    <script>
                     function fetchData(dt){
                         $('#embed').val(dt);
                     }  
                     function getEmbedVal(){
                         var embedval = $('#embedv').val();
                         $('#embed').val(embedval);
                         $('#embed-modal').modal('hide');
                     }
                    </script>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <input type="text" class="form-control inputstyle" id="location" name="location" placeholder="Location">
                        <span style="display: none; color: red; font-weight: bold" id="locationErrore" >Give Your Location.</span>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control textarea-style" rows="5" onkeyup="countChar(this)" name="caption" id="caption" placeholder="Caption"></textarea>
                        <span id="display_count">140</span> Character Remaining<br>
                        <span style="display: none; color: red; font-weight: bold" id="captionErrore" >Give Your Caption.</span>
                    </div>
                        <script>
                        //$(document).ready(function() {
                        //  $("#description").on('keyup', function() {
                        //    var words = this.value.match(/\S+/g).length;
                        //
                        //    if (words > 140) {
                        //      // Split the string on first 200 words and rejoin on spaces
                        //      var trimmed = $(this).val().split(/\s+/, 140).join(" ");
                        //      // Add a space at the end to make sure more typing creates new words
                        //      $(this).val(trimmed + " ");
                        //    }
                        //    else {
                        //      $('#display_count').text(140 - words);
                        //      $('#word_left').text(140-words);
                        //    }
                        //  });
                        //});   
                        </script>          
                        <script>
                            function countChar(val) {
                              var text_max = 140;
                              $('#display_count').html(text_max);
                              var len = val.value.length;
                              if (len > text_max) {
                                val.value = val.value.substring(0, text_max);
                                $('#display_count').html(0);
                              } else {
                                //$('#charNum').text(20 - len);
                                $('#display_count').html(text_max - len);
                              }
                            };
                        </script>                    
                    
                    <div class="clearfix"></div>
                    <span class="postcontent-list">
                        <div class="radio">
                            <label class="checkbox"><input type="checkbox" name="is_adult" id="is_adult" value="1"><i></i>Post contains adult content</label>
                        </div>
                    </span> 
                    <div class="channel-dropdown">
                        <a href="javascript:void(0)" data-toggle="collapse" data-target="#demo">
                            <div class="col-xs-8 col-sm-8 col-md-8">Add to channel</div>
                            <div class="col-xs-4 col-sm-4col-md-4 text-right"><i class="fa fa-bars" aria-hidden="true"></i>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    </div>
                    <ul id="demo" class="channel-list collapse" aria-expanded="false" style="height: 0px;">
                        <?php foreach ($chanel as $ch) { ?>
                            <li>
                                <div class="radio">
                                    <label class="checkbox">
                                        <input type="checkbox" name="category[]" value="<?php echo $ch['name'] ?>"><i></i><?php echo $ch['name'] ?></label>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                    <span style="display: none; color: red; font-weight: bold" id="categoryErrore" >Check Your Category.</span>
                    <script>
                        $(document).ready(function () {
                            $("input[name='category[]']").change(function () {
                                var maxAllowed = 3;
                                var cnt = $("input[name='category[]']:checked").length;
                                if (cnt > maxAllowed) {
                                    $(this).prop("checked", "");
                                    //alert('You can select maximum ' + maxAllowed + ' technologies!!');
                                    $('#checkbox-limit').modal('show');
                                }
                            });
                        });
                    </script>                        
                    <div class="modal fade" id="checkbox-limit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 32%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Category Limit</h4>
                                </div>
                                <div class="modal-body">
                                    <p>You are reached limit to choose category. <b><i class="title"></i></b> Maximum limit of category is three.</p>
                                    <p>Delete one to add new category?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>                        
                    <!--<div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="row">
                            <div class="social-place">
                                <ul class="social-list">
                                    <li>
                                        <div class="radio">
                                            <label class="checkbox">
                                                <input type="checkbox" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon.jpg') !!}"></span>411 Media</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="radio">
                                            <label class="checkbox">
                                                <input type="checkbox" name="twittershare" id="twittershare" onclick="twitterShare(this, 'http://twitter.com/share?url=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>', 'Twitter', '500', '258')" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon3.jpg') !!}"></span>Twitter</label>
                                                <input type="checkbox" name="twittershare" onclick="popWindow('http://twitter.com/share?url=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>','Twitter','500', '258')" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon2.jpg') !!}"></span>Twitter</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="radio">
                                            <label class="checkbox">
                                                <input type="checkbox" name="facebookshare" id="facebookshare" onclick="facebookShare(this, 'http://www.facebook.com/sharer.php?u=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>', 'Facebook', '500', '400')" value="1" ><i></i><span><img src="{!! asset('theme/site/images/social-icon2.jpg') !!}"></span>Facebook</label>
                                                <input type="checkbox" name="facebookshare" onclick="popWindow('http://www.facebook.com/sharer.php?u=<?php echo 'http://ornointeriors.com.au/product/index/section/319'; ?>', 'Facebook','500','400')" value="1" ><i></i><span><img src="{!! asset('theme/site/images/social-icon3.jpg') !!}"></span>Facebook</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class="top-addvertisement margin-half"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
                    </div>
                    <ul class="button-list button-list-big">
                        <li>
                            <input type="submit" class="pop-button login-buttom" value="Share Post">
                        </li>
                    </ul>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="modal fade" id="confirm-photo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 32%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabelhead">Share Your Post</h4>
            </div>
            <div class="modal-body">
<!--                <h4 class="modal-title" id="myModalLabelhead">Photo Posted Successfully.</h4>-->
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="social-place">
                            <ul class="social-list">
                                <!--<li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" value=""><i></i><span><img src="{!! asset('theme/site/images/social-icon.jpg') !!}"></span>411 Media</label>
                                    </div>
                                </li>-->
                                <li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" name="twittershare" id="twittershare" value="1">
                                            <i></i><span><img src="{!! asset('theme/site/images/social-icon3.jpg') !!}"></span>Twitter</label>

                                    </div>
                                </li>
                                <li>
                                    <div class="radio">
                                        <label class="checkbox">
                                            <input type="checkbox" name="facebookshare" id="facebookshare" value="1" >
                                            <i></i><span><img src="{!! asset('theme/site/images/social-icon2.jpg') !!}"></span>Facebook</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="confirmpost" value="" >
            <div class="modal-footer">
                <button type="button" onclick="goReturnUrlLink()" class="btn btn-default" data-dismiss="modal">Share</button>
            </div>
        </div>
    </div>
</div>
<script>
    
    function goReturnUrlLink() {
        var confirmpost = $('#confirmpost').val();
        window.location.href = confirmpost;
    } 
    
    function twitterShareNow(url, winName, w, h) {
        if ($('#twittershare').is(':checked')) {
            //alert("Checkbox is checked.");
            popWindow(url, winName, w, h);
        }
    }
    function facebookShareNow(url, winName, w, h) {
        if ($('#facebookshare').is(':checked')) {
            //alert("Checkbox is checked.");
            popWindow(url, winName, w, h);
        }
    }

    function popWindow(url, winName, w, h) {
        if (window.open) {
            if (poppedWindow) {
                poppedWindow = '';
            }
            windowW = w;
            windowH = h;
            var windowX = (screen.width / 2) - (windowW / 2);
            var windowY = (screen.height / 2) - (windowH / 2);
            var myExtra = "status=no,menubar=no,resizable=yes,toolbar=no,scrollbars=yes,addressbar=no";
            var poppedWindow = window.open(url, winName, 'width=' + w + ',height=' + h + ',top=' + windowY + ',left=' + windowX + ',' + myExtra + '');
        } else {
            alert('Your security settings are not allowing our popup windows to function. Please make sure your security software allows popup windows to be opened by this web application.');
        }
        return false;
    }
    
    $("form#data").submit(function (event) {
        //alert(11);
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var img = $('#img').val();
        //alert(img);
        //var embed = $('#embed').val();
        var location = $('#location').val();
        var caption = $('#caption').val();
        var category = $("[name='category[]']:checked").length;
        var adult =  $("[name='is_adult']:checked").length;
        //alert(category);
        var conf = true;
        if(img == ''){ conf = false; $("#imageErrore").show("slow"); } else { conf = true; $("#imageErrore").hide("slow"); }
        if(location == ''){ conf = false; $("#locationErrore").show("slow"); } else { conf = true; $("#locationErrore").hide("slow"); }
        if(caption == ''){ conf = false; $("#captionErrore").show("slow"); } else { conf = true; $("#captionErrore").hide("slow"); }
        //if(category < 1){ conf = false; $("#categoryErrore").show("slow"); } else { conf = true; $("#categoryErrore").hide("slow"); }
        //alert(vid); alert(embed);
        //alert($('#data').serialize());
        //alert(formData);
        if(conf){
            $.ajax({
                url: "{{ Config::get('config.baseurl') }}saveslide",
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (returndata) {
                    //alert(returndata);
                    var shurl = "{!! URL::to('showpost') !!}/" + returndata; 
                    
                    if(adult != 1){
                        var shurl = "http://xigma.website/dev10/4eleven/public/showalbum/" + returndata; 
                    } else {
                        var shurl = "http://xigma.website/dev10/4elevenadult/public/adult/showalbum/<?php echo base64_encode($udt->id)?>/" + returndata; 
                    }                     
                    
                    
                    $('#twittershare').attr("onclick", "twitterShareNow('http://twitter.com/share?url=" + shurl + "', 'Twitter', '500', '258')");
                    $('#facebookshare').attr("onclick", "facebookShareNow('http://www.facebook.com/sharer.php?u=" + shurl + "', 'Facebook', '500', '400')");                    
                    //$('#twittershare').attr("onclick", "twitterShare(this, 'http://twitter.com/share?url=<?php echo 'http://ornointeriors.com.au/product/index/section/315'; ?>', 'Twitter', '500', '258')");
                    //$('#facebookshare').attr("onclick", "facebookShare(this, 'http://www.facebook.com/sharer.php?u=<?php echo 'http://ornointeriors.com.au/product/index/section/315'; ?>', 'Facebook', '500', '400')");
                    $('#img').val('');
                    $('#embed').val('');
                    $('#embedv').val('');
                    $('#location').val('');
                    $('#caption').val('');
                    $("input[name='is_adult']:checkbox").removeAttr('checked');
                    $("input[name='category[]']:checkbox").removeAttr('checked');
                    $('#confirmpost').val(shurl);
                    $('#confirm-photo').modal('show');
                }
            });
        }
        return false;
    });    

</script>
@stop