@extends('_layouts.site')
@section('content')
<div class="buyonline">
	<div class="container">
		<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-12">
				<div class="artistprofile-cont">
				<h2>Buy Online</h2>
					
					<div class="row">
						<div class="col-md-12">
							@if (session('message'))
						        <div class="alert alert-success" role="alert">
						            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						            <strong>{{ session('message') }}</strong>
						        </div>
						    @endif
							@foreach($stores as $store)
							<div class="album-sec">
								<div class="row">
									<div class="col-md-4 col-sm-4">
										<?php $today = strtotime(date('Y-m-d H:i:s'));
										if($store->new_till_date > $today){?>

											{!! Html::image('theme/site/images/new.png','new product', array('class'=>'new-preduct')) !!} 
										<?php } ?>
										<div class="album-img ">
											{!! Html::image('products/thumb/stores/'.$store->album_cover_image) !!} 
										</div>	
										<div class="clearfix"></div>
									</div>	
									<div class="col-md-8 col-sm-8 ">
										<h3>{!! $store->album_name !!}</h3>
										<h4><span class="coly">Music Dsigned By:</span> {!! $store->music_designed !!}</h4>
										<h4><span class="coly">Album Launch By:</span> {!! $store->album_launch_by !!}, On {!! date('jS F, Y',$store->album_launch_date) !!}</h4>
										<div class="album-list">
											<h3>Song List</h3>
											<?php $songLists = explode(',', $store->songs_list);
											$halfList = (int)(count($songLists)/2) ;
											?>
											<div class="row">
												<div class="col-md-6 col-sm-6">
													<ul>
														<?php for ($i=0; $i <$halfList ; $i++) { ?>
														<li>{{$songLists[$i]}}</li>
														<?php } ?>
													</ul>
												</div>	
												<div class="col-md-6 col-sm-6">
													<ul>
														<?php for ($i=$halfList; $i <count($songLists) ; $i++) { ?>
														<li>{{$songLists[$i]}}</li>
														<?php } ?>
													</ul>
												</div>
											</div>
											<div class="clearfix">&nbsp;</div>
											<h3>Play Song</h3>
												<div class="row">
													<div class="col-md-6 col-sm-6 audio">
														<audio controls>
															<source src=" {{URL::to('products/main/audio/'.$store->songs)}}" type="audio/mpeg">
														</audio>
													</div>	
													<div class="col-md-6 col-sm-6">
														<a href="{{URL::to($store->id.'/online-form')}}" class="buy-now">Buy Now</a>
														<!-- <input type="submit" value="Buy Now"> -->
													</div>
												</div>		
										</div>	
									</div>
								</div>
							</div>
							@endforeach
						</div>
						
					</div>
					
				</div>
			</div>	
		</div>
	</div>
</div>
@stop