@extends('_layouts.site')
@section('content')
<!--our-service-open-->
<div class="our-service">
	<h1 class="text-center">Our Service Areas</h1>
	<div class="our-routes">
		<div class="row">
			<div class="col-md-5">
				<div class="our-routes-map" style="background: url({!! asset('theme/site/images/map-bg.jpg') !!}) no-repeat top center; background-size:cover; ">
					<div class="map-text">
						<h3>Our Routes SF</h3>
						<h5>Daily: 4:30am – 10:30pm</h5>
						<h5><span>Servicing:</span> Marina, Cow Hollow, Pacific Heights,Financial District, SOMA 
							and 4th Street Caltrain</h5>
					</div>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d100939.98555000965!2d-122.50764073526796!3d37.757814997324196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1482317078760" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-md-7">
				<div class="our-routes-text">
					<img src="{!! asset('theme/site/images/ourroutes-bg.png') !!}" class="img-responsive">
					<h2>{{ $route1->title  }}</h2>
				       {{ $route1->description  }}
			</div>
		</div>
	</div>
	<div class="our-routes">
		<div class="row">
			<div class="col-md-5">
				<div class="our-routes-map" style="background: url({!! asset('theme/site/images/map-bg.jpg') !!}) no-repeat top center; background-size:cover; ">
					<div class="map-text">
						<h3>{{ $route2->title  }}</h3>
						   {{ $route2->description  }}
					</div>
					<iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d404740.31517187564!2d-122.43042432876614!3d37.57782383963862!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco!3m2!1d37.7749295!2d-122.4194155!4m5!1s0x808fcae48af93ff5%3A0xb99d8c0aca9f717b!2ssan+jose!3m2!1d37.3382082!2d-121.8863286!5e0!3m2!1sen!2sin!4v1482322948976" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-md-7">
				<div class="our-routes-Peninsular-text">
					<img src="{!! asset('theme/site/images/routes-bg.png') !!}" class="img-responsive">
					<h2>Our Route- Peninsular </h2>
					<p>4th Street Caltrain Station to Mountain View Station</p>
					<h5><i class="fa fa-calendar" aria-hidden="true"></i> DAILY 4:30AM – 10:30PM</h5>
					<p>Stopping is kept to a minimum to keep you moving. </p>
				</div>
			</div>
		</div>
	</div>
	<div class="our-routes-form">
		<div class="row">
			<div class="col-md-5">
				<div class="form-map">
					<img src="{!! asset('theme/site/images/map-image.png') !!}" class="img-responsive">
					<h2 class="text-center">Suggest a Route</h2>
					<h4 class="text-center">Enter start and end addresses or cross streets</h4>
					{!! Form::open(array('route'=>'site.contact', 'role'=>'form', 'method'=> 'post','name'=>'contactfrm')) !!}
						<div class="map-form-area">
						  <div class="form-group">
							<input type="text" class="form-control" id="home"  name="home"placeholder="Home" required="required">
						  </div>
						  <div class="form-group">
							<input type="text" class="form-control" id="work" name="work"placeholder="Work" required="required">>
						  </div>
							<div class="map-button text-center">
								  <button type="submit" class="btn btn-default">Submit</button>

							</div>
						</div>
				 {!! Form::close() !!}
				</div>
			</div>
			<div class="col-md-7">
				<div class="our-routes-map-text">
					<img src="{!! asset('theme/site/images/routesmap-bg.png') !!}" class="img-responsive">
					<h2>Next Stop </h2>
					<p>Tell us where you live and where you work so we can bring 
						Leap to your neighborhood. </p>
				</div>
			</div>
		</div>
	</div>
</div>

<!--our-service-closed-->
@stop
