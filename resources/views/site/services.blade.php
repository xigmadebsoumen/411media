@extends('_layouts.site')
@section('content')
<button class="cd-nav-trigger cd-image-replace">Open navigation<span aria-hidden="true"></span></button>

<section class="services-area first-blocks">
	<div class="container">
		<h1 class="heading">{!! $consult->title !!}</h1>

		{!! $consult->description !!}

		<div class="services-list">
			<ul class="list-unstyled">
				<li>
					<a href="{{ URL::route('site.maintenance') }}" >
						<img src="{!! asset('theme/site/img/maintenance.png') !!}" class="img-responsive">
						<h4>Maintenance</h4>
					</a>
				</li>
				<li>
					<a href="{{ URL::route('site.modernization') }}" >
						<img src="{!! asset('theme/site/img/modernization.png') !!}" class="img-responsive">
						<h4>Modernization</h4>
					</a>
				</li>
				<li>
					<a href="{{ URL::route('site.prepurchase') }}" >
						<img src="{!! asset('theme/site/img/Pre-Purchase-Assessments.png') !!}" class="img-responsive">
						<h4>Pre-Purchase Assessments</h4>
					</a>
				</li>
				<li>
					<a href="{{ URL::route('site.equipment') }}" >
						<img src="{!! asset('theme/site/img/New-Equipment-Review.png') !!}" class="img-responsive">
						<h4>New Equipment Review</h4>
					</a>
				</li>
				<li>
					<a href="{{ URL::route('site.consultation') }}" >
						<img src="{!! asset('theme/site/img/On-Going-Consultation.png') !!}" class="img-responsive">
						<h4>On-Going Consultation</h4>
					</a>
				</li>
				<li>
					<a href="{{ URL::route('site.conclusion') }}" >
						<img src="{!! asset('theme/site/img/conclusion.png') !!}" class="img-responsive">
						<h4>Conclusion</h4>
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>
@stop
