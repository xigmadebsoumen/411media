@extends('_layouts.site')
@section('content')
<div class="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-5">

			</div>
			<div class="col-md-7">
				<div class="artistprofile-cont">
				<h2>Contact me</h2>
					<div class="contact-details">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<h3>Office Address :</h3>
								<p>{!! $setting->office_address !!}</p>
							</div>	
							<div class="col-md-6 col-sm-6">
								<h3>Residence :</h3>
								<p>{!! $setting->residencial_address !!}</p>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-6 col-sm-6">
								<h3>Email ID :</h3>
								<p>{!! $setting->admin_email !!}</p>
							</div>
							<div class="col-md-6 col-sm-6">
								<h3>Contact No :</h3>
								<p>+{!! $setting->admin_ph_no !!}</p>
							</div>	
						</div>
						<div class="clearfix">&nbsp;</div>
						<h3>Query :</h3>
						<hr/>
						{!! Form::open(array('route'=>'query.contact', 'role'=>'form', 'method' => 'post')) !!}
							<div class="row">
								<div class="col-md-4 congap">
									<input type="text" name="name" id="name" class="form-control" placeholder="Name">
								</div>
								<div class="col-md-4 congap">
									<input type="email" name="email" id="email" class="form-control" placeholder="Email">
								</div>	
								<div class="col-md-4 congap">
									<input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Contact No">
								</div>	
								<div class="col-md-12 congap">
									<textarea placeholder="Message" name="message" id="message" class="form-control"></textarea>
								</div>
								<div class="col-md-12 congap">
									<input type="submit" id="submit">
								</div>
							</div>
						{!! Form::close() !!}
						<div class="clearfix">&nbsp;</div>
						@if (session('message'))
					        <div class="alert alert-success" role="alert">
					            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					            <strong>{{ session('message') }}</strong>
					        </div>
					    @endif
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
$('#submit').on('click', function(e) {

	if($('#name').commonCheck() & $('#email').validateEmail() &
		$('#phone_number').commonCheck() & $('#message').commonCheck()){
		return true;
	} else {
		return false;
	}
	e.preventDefault();
})
</script>
@stop