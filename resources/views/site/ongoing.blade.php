<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<link rel="stylesheet" href="css/main.css">
	<link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
  	
	<title>Corporate Elevator Consultants</title>
</head>
<body>
<nav class="cd-vertical-nav">
<div class="container">
<div class="col-xs-12 col-sm-4 col-md-4 mobile">
<div class="logo"><a href="#"><img src="img/logo.png" class="img-responsive"></a></div>
</div>

<div class="col-xs-12 col-sm-5 col-md-5">
	<ul>
		<li><a href="#section1" class="active">Home</a></li>
		<li><a href="#section2">About Us</a></li>
		<li><a href="#section3">Services</a></li>
		<li><a href="#section4">Clients</a></li>
		<li><a href="#section5">Contact</a></li>
	</ul>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 mobile">

	<ul class="social-list">
		<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
		<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
		<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
	</ul>

</div>
</div>
</nav><!-- .cd-vertical-nav -->

<button class="cd-nav-trigger cd-image-replace">Open navigation<span aria-hidden="true"></span></button>

<section class="services-details-area first-blocks">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<img src="img/Maintenance1.jpg" class="img-responsive">			
			</div>
			<div class="col-md-6">
				<h1 class="heading">Maintenance</h1>
				<h1>Services includes:</h1>
				<ul class="services-details-list list-unstyled">
					<li>
						Maintenance Specification – development of a detailed Full Maintenance Agreement to direct the 
						elevator maintenance contractor in the proper performance of maintenance on the 
						vertical transportation equipment
					</li>
					<li>
						Routine performance audits with solutions for improving your elevator/escalator service
					</li>
					<li>
						Determine the performance of the service you are receiving from your elevator contractor
					</li>
					<li>
						Evaluate the equipment and assess conditions that may contribute to excessive and expensive unscheduled shutdowns
					</li>
					<li>
						Inspect component wear to ensure repair and replacement is completed in accordance 
						with industry standards and your service providers contract
					</li>
					<li>
						Provide reports to ensure state and local code compliance is met
					</li>
					<li>
						Offer suggestions and recommendations for writing customized maintenance contracts for your elevator/escalator equipment
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<footer id="section6" class="cd-section footer text-center">
	<div class="content-wrapper">
		<a href="#"><img src="img/footer-logo.png" class="img-responsive footer-logo"></a>
		<ul class="footer-address list-unstyled">
			<li><h4>Corporate Elevator Consultants, Inc.</h4></li>
			<li>Voice: (313) 961-5701</li>
			<li>Fax: (313) 961-5712</li>
			<li>Marquette Building</li>
			<li>243 W. Congress, Suite 350</li>
			<li>Detroit, MI 48226</li>
			<li><a href="mailto:info@corporateelevatorconsultants.com">info@corporateelevatorconsultants.com</a></li>
		</ul>
	</div>
<div class="overlay"></div>
</footer><!-- cd-section -->
	
<script src="js/jquery-2.1.4.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="js/owl.carousel.js"></script>
<script>
$(document).ready(function() {

  var owl = $("#owl-demo");

  owl.owlCarousel({
     
      itemsCustom : [
        [0, 1],
        [450, 2],
        [600, 3],
        [700, 4],
        [1000, 5],
        [1200, 6],
        [1400, 7],
        [1600, 8]
      ],
      navigation : false,
	  autoPlay: 1000

  });
 
	
	var win  = jQuery(window),
    fxel     = jQuery('.cd-vertical-nav'),
    eloffset = fxel.offset().top;
	win.scroll(function() {
		if (eloffset < win.scrollTop()) {
			fxel.addClass("fixed");
		} else {
			fxel.removeClass("fixed");
		}
	});

});
</script>
</body>
</html>