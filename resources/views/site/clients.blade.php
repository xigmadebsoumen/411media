
@extends('pages.header')
<button class="cd-nav-trigger cd-image-replace">Open navigation<span aria-hidden="true"></span></button>

<section class="services-area first-blocks">
	<div class="container">
		<h1 class="heading">Services Overview</h1>
		<div class="row">
			<div class="col-md-4">
				<p>
					Are your building personnel sufficiently trained to understand elevator safety and identify 
					potential hazards? Who will assist you with determining if you are receiving the maintenance 
					you are paying for in accordance with your service provider contract? Who will determine your 
					modernization needs and write specifications and bid documentation to realize the best value 
					and bidding competiveness considering all the modernization products available, based on your 
					criteria? You need an unbiased third party elevator expert to help you with these decisions.
				</p>								
			</div>
			<div class="col-md-4">
				<p>
					Corporate Elevator Consultants, Inc. provides vertical transportation evaluation and 
					consulting services for our clients who own or manage buildings and facilities involving 
					elevators/lifts, escalators, moving walkways, etc. We will evaluate your current system and 
					provide solutions that are effective and capable of optimizing the value of your present 
					elevator system and ensure the safety of your passengers.
				</p>
			</div>
			<div class="col-md-4">
				<p>
					In addition, we provide the building owner with the latest and most proven technologies 
					and techniques. We are experienced consultants offering valuation-producing solutions 
					for high-rise and complex systems including modernization and redesign. Corporate Elevator 
					Consultants, Inc. adds value by remaining independent of elevator companies and by providing 
					the owner with elevator system solutions. Please select an option on 
					the left to learn more about us.
				</p>
			</div>
		</div>
		<div class="services-list">
			<ul class="list-unstyled">
				<li>
					<a href="#">
						<img src="img/maintenance.png" class="img-responsive">
						<h4>Maintenance</h4>
					</a>
				</li>
				<li>
					<a href="#">
						<img src="img/modernization.png" class="img-responsive">
						<h4>Modernization</h4>
					</a>
				</li>
				<li>
					<a href="#">
						<img src="img/Pre-Purchase-Assessments.png" class="img-responsive">
						<h4>Pre-Purchase Assessments</h4>
					</a>
				</li>
				<li>
					<a href="#">
						<img src="img/New-Equipment-Review.png" class="img-responsive">
						<h4>New Equipment Review</h4>
					</a>
				</li>
				<li>
					<a href="#">
						<img src="img/On-Going-Consultation.png" class="img-responsive">
						<h4>On-Going Consultation</h4>
					</a>
				</li>
				<li>
					<a href="#">
						<img src="img/conclusion.png" class="img-responsive">
						<h4>Conclusion</h4>
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>

