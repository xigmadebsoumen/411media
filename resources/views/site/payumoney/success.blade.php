﻿<?php

$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$order_id=$_POST["txnid"];
$transaction_id = $_POST['mihpayid'];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="aPa4Blsnuo";

if(isset($_POST["additionalCharges"])) {
      $additionalCharges=$_POST["additionalCharges"];
      $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$order_id.'|'.$key;
}
else{
      $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$order_id.'|'.$key;
    }
$hash = hash("sha512", $retHashSeq);

if ($hash == $posted_hash) {
   $status = 'failure';    
   echo "Invalid Transaction. Please try again";
}
else{
    $status = 'success';  
    echo "Transaction Complete. Please wait some time";   
 } 
?>  
<form method="post" name="redirect" action="{{URL::to('payment/response')}}">
  <input type="hidden" name="status" value="{{$status}}">
  <input type="hidden" name="order_id" value="{{$order_id}}" > 
  <input type="hidden" name="transaction_id" value="{{$transaction_id}}" >   
  <input type="hidden" name="productinfo" value="{{$productinfo}}" >
</form>
<script language='javascript'>document.redirect.submit();</script>
