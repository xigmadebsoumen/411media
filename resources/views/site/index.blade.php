@extends('_layouts.home')
@section('content')
<style>
    .header{background-color:#4c66a4;padding:20 0 5 200px}
</style>
<section class="banner vunit vw100" style="background-image: url({!! asset('theme/site/images/banner.jpg') !!});">                
    <div class=middle-part>
        <div class=logo>
            <a href="{!! URL::to('') !!}"><img src="{!! asset('theme/site/images/logo.png') !!}" class=img-responsive>
            </a>
        </div>
        <div class=search>
            <form>
                <div class="form-group form-icon">
                    <input class="form-control search-style" id="search_box" placeholder="Search...." autocomplete="off">
                    <input class=searchplane id=button type=button>
                    <div id="searchres" class="searchres" style="display: none;"></div>
                </div>
            </form>
        </div>
        <div class=button-area>
            <div class="col-xs-12 col-lg-4 col-md-4 col-sm-4">
                <div class=row><a href="javascript:void(0)" data-toggle=modal data-target=#signup class=blk-button>sign up</a>
                </div>
            </div>
            <div class="col-xs-12 col-lg-4 col-md-4 col-sm-4">
                <div class=row><span class=cross-icon><img src="{!! asset('theme/site/images/zigzak-arrow.png') !!}" ></span>
                </div>
            </div>
            <div class="col-xs-12 col-lg-4 col-md-4 col-sm-4">
                <div class=row><a href="javascript:void(0)" onclick="openLoginModal();" class=blk-button>Log in</a>
                </div>
            </div>
        </div>
        <div class=clearfix></div>
    </div>
</section>
<div class="fade modal" id=signup role=dialog>
    <div class=modal-dialog>
        <div class="modal-content modal-style"><span class="cross-btm flaticon-error" class=close data-dismiss=modal></span>  
            <h2 class=modal-title>Let's Get Strated!</h2>
            <div class=modal-body>
                <form>
                    <input type="hidden" name="fbid" id="fbid" value="" > 
                    <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6"> 
                        <div class=form-group>
                            <input class="form-control inputstyle" name="fname" id=fname placeholder="First name">
                            <span style="display: none; color: red; font-weight: bold" id="fnameErrore" >Give Your First Name.</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6">
                        <div class=form-group>
                            <input class="form-control inputstyle" name="lname" id=lname placeholder="Last name">
                            <span style="display: none; color: red; font-weight: bold" id="lnameErrore" >Give Your Last Name.</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                        <div class=form-group>
                            <input class="form-control inputstyle" name="semail" id=semail placeholder="Email address" type=email>
                            <span style="display: none; color: red; font-weight: bold" id="semail1Errore" >Give Your Email.</span>
                            <span style="display: none; color: red; font-weight: bold" id="semail2Errore" >Give Proper Email.</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                        <div class=form-group>
                            <input class="form-control inputstyle" name="spass" id=spass placeholder=Password type=password>
                            <span style="display: none; color: red; font-weight: bold" id="spassErrore" >Give Your Password.</span>
                        </div>
                    </div>
                    <!-- <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                        <label class=label-style>Profile Name</label>
                        <div class=form-group>
                            <input class="form-control inputstyle" name="pname" id=pname>
                            <span style="display: none; color: red; font-weight: bold" id="pnameErrore" >Give Your Profile Name.</span>
                        </div>
                    </div> -->
                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                        <div class=modal-bottom>
                            <div class=radio>
                                <label class=checkbox>
                                    <input id="terms" type=checkbox><i></i>I agree to connect Terns and service and Privacy policy</label>
                                <span style="display: none; color: red; font-weight: bold" id="termsErrore" >Check and agree with Site Terms and Conditions.</span>
                            </div>

                            <div class="radio" style="display: none" id="signupFormErroreemail" >
                                <span style="color: red; font-weight: bold">Email Aready Exist Try Another.</span>
                            </div>

                            <div class="radio" style="display: none" id="signupFormErrorepname" >
                                <span style="color: red; font-weight: bold">Profile Name Aready Exist Try Another.</span>
                            </div>                            

                            <div id="signupbutton"> <input class=pop-button type=button onclick="SignUpNow()" value="Continue"> </div>
                            <div id="signuplodder" style="display : none"> <img src="{!! asset('devimg/progress_bar.gif') !!}" style="width:250px" > </div>

                            <div class="face-share-button" id="fbsignupbutton">
                                <a href="javascript:void(0)" onclick="facebookSignUp();" id="fbSignupHref" ><img src="{!! asset('theme/site/images/facebookbutton.png') !!}" >
                                </a>
                            </div>
                            <span class=sml-text>Already have an account? <a href="javascript:void(0)" onclick="openLoginModal()" >Click here to log in</a></span>
                        </div>
                    </div>
                </form>
                <div class=clearfix></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    //     facebook login start here  d4f992fb67bbf2bac1761fbad8c91f03
    
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '446831955704773',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

    function facebookSignUp() {
        $("#fbsignupbutton").hide();
        $("#signuplodder").show();
        FB.login(function (response) {
            if (response.authResponse) {
                //console.log('Welcome!  Fetching your information.... ');
                FB.api('/me?fields=email,first_name,last_name,id', function (response) {
                    if (response) {
                        console.log(response);
                        var fname = response.first_name;
                        var lname = response.last_name;
                        var id = response.id;
                        var email = response.email;
                        url = "{{ Config::get('config.baseurl') }}checkfbuser";
                        $.ajax({
                            url: url,
                            async: false,
                            type: 'POST',
                            data: {fname: fname, lname: lname, email: email, id: id, ltype: 'facebook'},
                        }).done(function (response) {
                            //alert(response);
                            if(response == 1){
                                $('#fblogid').val(id);
                                $("#fblogin").submit();
                            } else if(response == 0){
                                $('#fbid').val(id);
                                $('#fname').val(fname);
                                $('#lname').val(lname);
                                if(email){ $('#semail').val(email); $('#semail').prop('readonly', true); } 
                                $('#spass').val(id);
                                $('#fname').prop('readonly', true);
                                $('#lname').prop('readonly', true);
                                $('#spass').prop('readonly', true);
                                $("#signuplodder").hide(); 
                                $("#fbsignupbutton").show();  
                                $('#fbSignupHref').prop('onclick',null).off('click');
                            } else if(response == 2){
                                $('#fbid').val(id);
                                $('#fname').val(fname);
                                $('#lname').val(lname); 
                                $('#fname').prop('readonly', true);
                                $('#lname').prop('readonly', true);
                                $("#signuplodder").hide(); 
                                $("#fbsignupbutton").show();  
                                $('#fbSignupHref').prop('onclick',null).off('click');
                            }                                
                        });
                    }
                    console.log(response);
                    //console.log('Good to see you, ' + response.name + '.');
                });
            } else {
                //console.log('User cancelled login or did not fully authorize.');
            }
        }, {scope: 'email'});
    }
    
    function facebookLogin() {  
        //alert("loginDt");
        $("#fbLoginHref").hide();
        $("#loginlodder2").show();
        FB.login(function (response) {
                //console.log('Welcome!  Fetching your information.... ');
                FB.api('/me?fields=email,first_name,last_name,id', function (response) {
                    if (response) {
                        console.log(response);
                        var fname = response.first_name;
                        var lname = response.last_name;
                        var id = response.id;
                        var email = response.email;
                        url = "{{ Config::get('config.baseurl') }}checkloginfbuser";
                        $.ajax({
                            url: url,
                            async: false,
                            type: 'POST',
                            data: {fname: fname, lname: lname, email: email, id: id, ltype: 'facebook'},
                        }).done(function (response) {
                            //(response);
                            if(response == 1){
                                $('#fblogid').val(id);
                                $("#fblogin").submit();
                            } else if(response == 0){
                                $("#loginlodder2").hide();
                                $("#fbLoginHref").show();                                
                                $('#login').modal('hide');
                                $('#signup').modal('show');
                                $('#fbid').val(id);
                                $('#fname').val(fname);
                                $('#lname').val(lname);
                                if(email){ $('#semail').val(email); $('#semail').prop('readonly', true); } 
                                $('#spass').val(id);
                                $('#fname').prop('readonly', true);
                                $('#lname').prop('readonly', true);
                                $('#spass').prop('readonly', true);
                                $("#signuplodder").hide(); 
                                $("#fbsignupbutton").show();  
                                $('#fbSignupHref').prop('onclick',null).off('click');
                            } else if(response == 2){
                                $("#loginlodder2").hide();
                                $("#fbLoginHref").show();
                                $('#login').modal('hide');
                                $('#signup').modal('show');
                                $('#fbid').val(id);
                                $('#fname').val(fname);
                                $('#lname').val(lname); 
                                $('#fname').prop('readonly', true);
                                $('#lname').prop('readonly', true);
                                $("#signuplodder").hide(); 
                                $("#fbsignupbutton").show();  
                                $('#fbSignupHref').prop('onclick',null).off('click');
                            }                                
                        });
                    }
                    console.log(response);
                    //console.log('Good to see you, ' + response.name + '.');
                });
        }, {scope: 'email'});
    }    
    
    
</script>

{!! Form::open(array('route'=>'site.auth.fblogin', 'id'=>'fblogin')) !!}
<input value="" name="fblogid" id="fblogid"  type="hidden" >
{!! Form::close() !!} 

<div class="fade modal" id=login role=dialog>
    <div class=modal-dialog>
        <div class="modal-content modal-style"><span class="cross-btm flaticon-error" class=close data-dismiss=modal></span>
            <div class="modal-body tab-content">
                <div class="tab-pane active" id=innerlogin>
                    <h2 class=modal-title>Existing Account?</h2><span class=log-caption>If you already have an account, Please log in below.</span>
                    {!! Form::open(array('route'=>'site.auth.login', 'id'=>'frlogin')) !!} 
                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                        <div class=form-group>
                            <input value="" class="form-control inputstyle" name="email" id="loginemail" placeholder="Email address" type=email>
                            <span style="display: none; color: red; font-weight: bold" id="loginFormEmailVal">Enter proper email address.</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                        <div class=form-group>
                            <input value="" class="form-control inputstyle" id="loginpass" name="password" placeholder=Password type=password>
                            <span style="display: none; color: red; font-weight: bold" id="loginFormPasswordVal">Enter password.</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12" style="display: none" id="loginFormErrore">
                        <div class=form-group>
                            <span style="color: red; font-weight: bold" id="loginFormErroreVal">Login Email or Password is wrong. Try it Correctly.</span>
                        </div>
                    </div>                        
                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6">
                            <div class=row>
                                <div class=radio>
                                    <label class=checkbox>
                                        <input name="remember" id="remember" value="1" type=checkbox><i></i>Remember me?</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6">
                            <div class="row" id="loginbutton">
                                <input class="pop-button login-buttom" onclick="loginNow();" type=button value="log in">
                            </div>                                
                            <div class="row" id="loginlodder1" style="display : none">
                                <img src="{!! asset('devimg/progress_bar.gif') !!}" style="width:250px" > 
                            </div>
                        </div>
                    </div>
                    <div class=clearfix></div>
                    <span class=sml-text><a href="#forgotpassword" data-toggle=tab>Forgot Your Password?</a></span>
                    <span class=sml-text><a href="javascript:void(0)" onclick="openSignupModal();" data-toggle=tab>Not Have Account?</a></span>
                    <div class=face-share-button>
                        <a href="javascript:void(0)" onclick="facebookLogin();" id="fbLoginHref" >
                            <img src="{!! asset('theme/site/images/facebookbutton.png') !!}" >
                        </a>
                        <div id="loginlodder2" style="display : none"> 
                            <img src="{!! asset('devimg/progress_bar.gif') !!}" style="width:250px" >  
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="fade tab-pane" id=forgotpassword>
                    <h2 class=modal-title>Forget Your Password?</h2><span class=log-caption>If you already have an account, Please enter email.</span>
                    <form>
                        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                            <div class=form-group>
                                <input class="form-control inputstyle" id=femail placeholder="Email address" type=email>
                                <span style="display: none; color: red; font-weight: bold" id="femailf1Errore" >Give Your Email.</span>
                                <span style="display: none; color: red; font-weight: bold" id="femailf2Errore" >Give Proper Email.</span>
                                <span style="display: none; color: red; font-weight: bold" id="femailf3Errore" >This Email not registered with us.</span>
                            </div>
                        </div>
                        <input class=pop-button type=button onclick="forgotPassNow();" value="Send Email"> 
                        <span class=sml-text>Already have an account?<a href="#innerlogin"  data-toggle=tab> Click here to log in</a></span>
                    </form>
                    <div class=clearfix></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 32%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabelhead">Let's Get Strated!</h4>
            </div>
            <div class="modal-body">
                <!--<p>Welcome To 411Media.</p>
                <p>You are Registered Successfully.</p>
                <p>Login To make your Updates ?</p>-->
                {!! Form::open(array('route'=>'site.auth.login', 'id'=>'signuplogin')) !!}
                <input value="" name="email" id="lsemail"  type="hidden" >
                <input value="" name="password" id="lspass"  type="hidden" >
                 <input value="" name="dbids" id="dbids"  type="hidden" >
                 <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                    <label class=label-style>Profile Name</label>
                    <div class=form-group>
                        <input class="form-control inputstyle" name="pname" id=pname>
                        <span style="display: none; color: red; font-weight: bold" id="pnameErrore" >Give Your Profile Name.</span>
                        <span style="display: none; color: red; font-weight: bold" id="pnameErrore1" >Profile Name Already used. Try another.</span>
                    </div>
                </div>                
                {!! Form::close() !!}                  
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Exit</button>-->
                <div id="signupbutton"> <input class=pop-button type=button onclick="loginAfterSignUp()" value="Continiue To Profile"> </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-signup1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 32%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabelhead">Let's Get Strated!</h4>
            </div>
            <div class="modal-body">
                <!--<p>Welcome To 411Media.</p>
                <p>You are Registered Successfully.</p>
                <p>Login To make your Updates ?</p>-->
                {!! Form::open(array('route'=>'site.auth.frfblogin', 'id'=>'signuplogin1')) !!}
                 <input value="" name="dbidf" id="dbidf"  type="hidden" >
                 <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                    <label class=label-style>Profile Name</label>
                    <div class=form-group>
                        <input class="form-control inputstyle" name="pnamef" id=pnamef>
                        <span style="display: none; color: red; font-weight: bold" id="pnameErrore2" >Give Your Profile Name.</span>
                        <span style="display: none; color: red; font-weight: bold" id="pnameErrore3" >Profile Name Already Exist Try with another.</span>
                    </div>
                </div>                 
                <input value="" name="fbid" id="lfbid"  type="hidden" >
                {!! Form::close() !!}                  
            </div>           
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-default" onclick="loginAfterFbSignUp();" >My Timeline</button>-->
                <div id="signupbutton"> <input class=pop-button type=button onclick="loginAfterFbSignUp()" value="Continiue To Profile"> </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-forgotpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 32%;">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> -->
                <h4 class="modal-title" id="myModalLabelhead">Forgot Password </h4>
            </div>
            <div class="modal-body">
                <p>Email sent to <b id="femailval"></b> with steps to restore your password.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Exit</button>
            </div>
        </div>
    </div>
</div>

<script>
    function loginNow() {
        email = $('#loginemail').val();
        password = $('#loginpass').val();
        remember = $('#remember').val();
        var flag = true;
        if (email == '') {
            $("#loginFormEmailVal").show("slow");
            flag = false;
        } else {
            $("#loginFormEmailVal").hide("slow");
            flag = true;
        }
        if (password == '') {
            $("#loginFormEmailVal").show("slow");
            flag = false;
        } else {
            $("#loginFormEmailVal").hide("slow");
            flag = true;
        }
        if (flag) {
            url1 = "{!! URL::to('timeline') !!}";
            url = "{{ Config::get('config.baseurl') }}frontlogin";
            $.ajax({
                url: url,
                async: false,
                type: 'POST',
                data: {_token: "{{ csrf_token() }}", email: email, password: password, remember: remember},
            }).done(function (response) {
                //alert(response);
                if (response == 1) {
                    //window.location.replace(url1);
                    $("#frlogin").submit();
                } else {
                    $("#loginFormErroreVal").html(response);
                    $("#loginFormErrore").show("slow");
                }
            });
        }
    }


    function loginAfterSignUp() {
        //alert("dff");
        var pname = $('#pname').val();
        var id = $('#dbids').val();
        flag = true;
        if (pname == ''){ $("#pnameErrore").show("slow"); flag = false; } else { $("#pnameErrore").hide("slow"); flag = true; }
        if (flag) {
            url = "{{ Config::get('config.baseurl') }}checkpname";
            $.ajax({
                url: url,
                async: false,
                type: 'POST',
                data: {_token: "{{ csrf_token() }}", id:id, pname:pname},
            }).done(function (response) {
                //alert(response);
                if (response == 1) {
                    $("#pnameErrore1").show("slow");
                } else {
                    $("#signuplogin").submit();
                }
            });
        }
    }
    
    function loginAfterFbSignUp() {
        
        var pname = $('#pnamef').val();
        var id = $('#dbidf').val();
        flag = true;
        if (pname == ''){ $("#pnameErrore2").show("slow"); flag = false; } else { $("#pnameErrore2").hide("slow"); flag = true; }
        if (flag) {
            url = "{{ Config::get('config.baseurl') }}checkpname";
            $.ajax({
                url: url,
                async: false,
                type: 'POST',
                data: {_token: "{{ csrf_token() }}", id:id, pname:pname},
            }).done(function (response) {
                //alert(response);
                if (response == 1) {
                    $("#pnameErrore3").show("slow");
                } else {
                    $("#signuplogin1").submit();
                }
            });
        }        
    }

    function SignUpNow() {
        var fbid = $('#fbid').val();
        var fname = $('#fname').val();
        var lname = $('#lname').val();
        var semail = $('#semail').val();
        var spass = $('#spass').val();
        var pname = $('#pname').val();
        var terms = $('#terms').val();
        var flag = true;
        if (fname == '') {
            $("#fnameErrore").show("slow");
            flag = false;
        } else {
            $("#fnameErrore").hide("slow");
            flag = true;
        }
        if (flag) {
            if (lname == '') {
                $("#lnameErrore").show("slow");
                flag = false;
            } else {
                $("#lnameErrore").hide("slow");
                flag = true;
            }
        }
        if (flag) {
            if (semail == '') {
                $("#semail1Errore").show("slow");
                flag = false;
            } else {
                $("#semail1Errore").hide("slow");
                flag = true;
            }
        }
        if (flag) {
            if (spass == '') {
                $("#spassErrore").show("slow");
                flag = false;
            } else {
                $("#spassErrore").hide("slow");
                flag = true;
            }
        }
//        if (flag) {
//            if (pname == '') {
//                $("#pnameErrore").show("slow");
//                flag = false;
//            } else {
//                $("#pnameErrore").hide("slow");
//                flag = true;
//            }
//        }
        if (flag) {
            if ($('#terms').is(':checked')) {
                $("#termsErrore").hide("slow");
                flag = true;
            } else {
                $("#termsErrore").show("slow");
                flag = false;
            }
        }
        if (flag) {
        $("#signupbutton").hide();
        $("#signuplodder").show();            
            url1 = "{!! URL::to('timeline') !!}";
            url = "{{ Config::get('config.baseurl') }}usersignup";
            $.ajax({
                url: url,
                async: false,
                type: 'POST',
                data: {_token: "{{ csrf_token() }}", fbid:fbid, fname: fname, lname: lname, email: semail, password: spass},
            }).done(function (response) {
                //alert(response);
                if (response == 'e') {
                    $("#signuplodder").hide();
                    $("#signupbutton").show();
                    $("#signupFormErroreemail").show("slow");
                    $("#signupFormErrorepname").hide("slow");
                } else {
                    $('#fbid').val('');
                    $('#fname').val('');
                    $('#lname').val('');
                    $('#semail').val('');
                    $('#spass').val('');
                    $("input[id='terms']:checkbox").removeAttr('checked');
                    $("#signuplodder").hide();
                    $("#signupbutton").show();
                    $('#signup').modal('hide');
                    if(fbid != ''){
                        $('#lfbid').val(fbid);
                        $('#dbidf').val(response);
                        $('#confirm-signup1').modal('show');
                    } else {
                        $('#lsemail').val(semail);
                        $('#lspass').val(spass);
                        $('#dbids').val(response);
                        $('#confirm-signup').modal('show');
                    }                    
                }

            });
        }
    }



//    function SignUpNow() {
//        var fbid = $('#fbid').val();
//        var fname = $('#fname').val();
//        var lname = $('#lname').val();
//        var semail = $('#semail').val();
//        var spass = $('#spass').val();
//        var pname = $('#pname').val();
//        var terms = $('#terms').val();
//        var flag = true;
//        if (fname == '') {
//            $("#fnameErrore").show("slow");
//            flag = false;
//        } else {
//            $("#fnameErrore").hide("slow");
//            flag = true;
//        }
//        if (flag) {
//            if (lname == '') {
//                $("#lnameErrore").show("slow");
//                flag = false;
//            } else {
//                $("#lnameErrore").hide("slow");
//                flag = true;
//            }
//        }
//        if (flag) {
//            if (semail == '') {
//                $("#semail1Errore").show("slow");
//                flag = false;
//            } else {
//                $("#semail1Errore").hide("slow");
//                flag = true;
//            }
//        }
//        if (flag) {
//            if (spass == '') {
//                $("#spassErrore").show("slow");
//                flag = false;
//            } else {
//                $("#spassErrore").hide("slow");
//                flag = true;
//            }
//        }
//        if (flag) {
//            if (pname == '') {
//                $("#pnameErrore").show("slow");
//                flag = false;
//            } else {
//                $("#pnameErrore").hide("slow");
//                flag = true;
//            }
//        }
//        if (flag) {
//            if ($('#terms').is(':checked')) {
//                $("#termsErrore").hide("slow");
//                flag = true;
//            } else {
//                $("#termsErrore").show("slow");
//                flag = false;
//            }
//        }
//        if (flag) {
//        $("#signupbutton").hide();
//        $("#signuplodder").show();            
//            url1 = "{!! URL::to('timeline') !!}";
//            url = "{{ Config::get('config.baseurl') }}usersignup";
//            $.ajax({
//                url: url,
//                async: false,
//                type: 'POST',
//                data: {_token: "{{ csrf_token() }}", fbid:fbid, fname: fname, lname: lname, email: semail, password: spass, pname: pname},
//            }).done(function (response) {
//                //alert(response);
//                if (response == 'y') {
//                    $('#fbid').val('');
//                    $('#fname').val('');
//                    $('#lname').val('');
//                    $('#semail').val('');
//                    $('#spass').val('');
//                    $('#pname').val('');
//                    $("input[id='terms']:checkbox").removeAttr('checked');
//                    $("#signuplodder").hide();
//                    $("#signupbutton").show();
//                    $('#signup').modal('hide');
//                    if(fbid != ''){
//                        $('#lfbid').val(fbid);
//                        $('#confirm-signup1').modal('show');
//                    } else {
//                        $('#lsemail').val(semail);
//                        $('#lspass').val(spass);                        
//                        $('#confirm-signup').modal('show');
//                    }
//                } else if (response == 'e') {
//                    $("#signuplodder").hide();
//                    $("#signupbutton").show();
//                    $("#signupFormErroreemail").show("slow");
//                    $("#signupFormErrorepname").hide("slow");
//                } else if (response == 'p') {
//                    $("#signuplodder").hide();
//                    $("#signupbutton").show();
//                    $("#signupFormErroreemail").hide("slow");
//                    $("#signupFormErrorepname").show("slow");
//                }
//            });
//        }
//    }


    function forgotPassNow() {
        var email = $('#femail').val();
        var flag = true;
        if (flag) {
            if (email == '') {
                $("#femailf1Errore").show("slow");
                flag = false;
            } else {
                $("#femailf1Errore").hide("slow");
                flag = true;
            }
        }
        //if(flag){ if (validateEmail(semail)){ alert("ok"); } else { alert("not ok"); }
        if (flag) {
            url1 = "{!! URL::to('timeline') !!}";
            url = "{{ Config::get('config.baseurl') }}forgotpassword";
            $.ajax({
                url: url,
                async: false,
                type: 'POST',
                data: {_token: "{{ csrf_token() }}", email: email},
            }).done(function (response) {
                //alert(response);
                if (response == '0') {
                    $("#femailf1Errore").hide("slow");
                    $("#femailf2Errore").hide("slow");
                    $("#femailf3Errore").show("slow");
                } else if (response == 1) {
                    $("#forgotpassword").removeClass("active");
                    $("#innerlogin").addClass("active");
                    $('#login').modal('hide');
                    $("#femailval").html(email);
                    $('#femail').val('');
                    $('#confirm-forgotpassword').modal('show');
                }
            });
        }
    }

    function  openLoginModal() {
        $("#forgotpassword").removeClass("active");
        $("#innerlogin").addClass("active");
        $('#signup').modal('hide');
        $('#login').modal('show');
    }

    function  openSignupModal() {
        $('#login').modal('hide');
        $('#signup').modal('show');
    }

</script>

<script type="text/javascript">
    $(document).on("click", function(e){
        if( !$("#search_box").is(e.target) ){ 
        //if your box isn't the target of click, hide it
            $("#searchres").hide();
        }
    });

    $(document).ready(function () {
        var urlsearch = "{{ Config::get('config.baseurl') }}searchfriends";
        $("#search_box").click(function() {
            //alert("click");
            var search_string = $("#search_box").val();
            if (search_string == '') {
                $("#searchres").html('');
                $("#searchres").hide("slow");
            } else {
                postdata = {'string': search_string}
                $.post(urlsearch, postdata, function (data) {
                    $("#searchres").html(data);
                    $("#searchres").show("slow");
                });
            }            
        });
        
        $("#search_box").keyup(function () {
            var search_string = $("#search_box").val();
            if (search_string == '') {
                $("#searchres").html('');
                $("#searchres").hide("slow");
            } else {
                postdata = {'string': search_string}
                $.post(urlsearch, postdata, function (data) {
                    $("#searchres").html(data);
                    $("#searchres").show("slow");
                });
            }
        });        
        
        
    });
    function fillme(name) {
        $("#search_box").val(name);
        $("#searchres").html('');
    }
</script>
@stop