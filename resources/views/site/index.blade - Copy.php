@extends('_layouts.home')
@section('content')
<!--banner open-->
<div class="banner-video clearfix">
	<video width="100%" height="100%" autoplay loop>
	  <source src="{!! asset('theme/site/images/banner-video.mp4') !!}" type="video/mp4">
	  <source src="{!! asset('theme/site/images/banner-video.ogg') !!}" type="video/ogg">
	  <source src="{!! asset('theme/site/images/banner-video.webm') !!}" type="video/webm">
	</video>
	<div class="video-text text-center clearfix">
		<h1>Reinventing Daily Commute</h1>
		<h2>Ride for as little as $2</h2>
		<h3>Download the free app and get on board</h3>
		<div class="video-icon">
		<ul class="list-unstyled list-inline">
			<li><img src="{!! asset('theme/site/images/google.png') !!}"></li>
			<li><img src="{!! asset('theme/site/images/apple.png') !!}"></li>
		</ul>
		</div>
	</div>
</div>
<!--banner open-->
<!--location-area-open-->
<div class="location-area">
	<div class="container">
		<h2>Ride and turn miles into stock equities</h2>
		
		<div class="row">
			<form       >
				<div class="col-md-4">
					<div class="form-group">
						<label for="exampleInputEmail1">Dateparture Date And Time</label>
						<input type="text" class="form-control" id="datetime" placeholder="Date and Time">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="exampleInputEmail1">Pickup Location</label>
						<select class="form-control" id="pickup">
						  <option value="1">Select Pickup Location</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="email" placeholder="Email">
					</div>
				</div>
				<div class="form-button text-center">
					 <button type="submit" class="btn btn-default" onclick="newsletter_subscribe()">Get me on board</button>
					
				</div>
			</form>
		</div>
	</div>
</div>
<!--location-area-closed-->
<!--our-buses-open-->
<div class="our-buses clearfix" style="background: url({!! asset('theme/site/images/bus.jpg') !!}) no-repeat top center; background-size:cover; ">
	<div class="container">
		<div class="our-buses-content clearfix">
			<h2 class="text-center">Our Buses</h2>
			<div class="bus-content-one clearfix">
				<div class="row">

	            <?php $count=0;
					foreach ($ourbus as $key => $value) {
					?>
					<?php $id=$value->id; ?>
					<?php if($id==1){ ?>
	             <div class="col-md-4">
						<div class="Convenient clearfix">
							<div class="row">
								<div class="col-md-3">
									<img src="{!! asset('theme/site/images/convenient-icon.png') !!}" alt="icon">
								</div>
								<div class="col-md-9">
									<ul class="list-unstyled">
										<li><h3>{{ $value->title }}</h3></li>
										<li><p>{{ $value->description }}</p></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
                 
	            <?php }  ?>
					
                <?php if($id==2){ ?>
                 <div class="col-md-4">
					</div>
					<div class="col-md-4">
						<div class="Convenient clearfix">
							<div class="row">
								<div class="col-md-3">
									<img src="{!! asset('theme/site/images/convenient-icon2.png') !!}" alt="icon">
								</div>
								<div class="col-md-9">
									<ul class="list-unstyled">
										<li><h3>{{ $value->title }}</h3></li>
										<li><p>{{ $value->description }}</p></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
              <?php }  ?>
              <?php }  ?>
				</div>
			</div>


			<div class="bus-content clearfix">
				<div class="row">
				 <?php $count=0;
			    foreach ($ourbus as $key => $value) {?>
				<?php $id=$value->id; ?>
				<?php if($id==3){ ?>
					<div class="col-md-4">
						<div class="Convenient clearfix">
							<div class="row">
								<div class="col-md-3">
									<img src="{!! asset('theme/site/images/convenient-icon1.png') !!}" alt="icon">
								</div>
								<div class="col-md-9">
									<ul class="list-unstyled">
										<li><h3>{{ $value->title }}</h3></li>
										<li><p>{{ $value->description }}</p></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
                   <?php if($id==4){ ?>

					<div class="col-md-4">
					</div>
					<div class="col-md-4">
						<div class="Convenient clearfix">
							<div class="row">
								<div class="col-md-3">
									<img src="{!! asset('theme/site/images/convenient-icon3.png') !!}" alt="icon">
								</div>
								<div class="col-md-9">
									<ul class="list-unstyled">
										<li><h3>{{ $value->title }}</h3></li>
										<li><p>{{ $value->description }}</p></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
                    <?php } ?>
                    <?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!--our-buses-closed-->

<!--How-you-ride-open-->




<section id="how-it-works" class="how-it-works horiz" data-compound="false">
    <div class="wrap">
        <h2 class="heading">HOW YOU RIDE</h2>
        <ul class="pag-dots">
        <?php
        	if( $ride ){
        			$i = 1;

        			foreach ( $ride as $value) {
        				if( $i == 1 ){
        					$class = 'on';
        				}elseif( $i == 2 ){
        					$class = 'on current';
        				}else{
        					$class = '';
        				}
        				?>
<li class="<?php echo $class; ?>"><a href="#" data-goto="<?php echo $i ?>"><?php echo $i ?></a></li>
        				<?php
        				$i++;
        			}

        	}
        ?>
                       
                    </ul>
        <div class="step-wrap">
            <ul class="step-list step-2" data-step-notified="true">
            <?php
        	if( $ride ){
        			$i = 1;

        			foreach ( $ride as $value) {
        				if( $i == 1 ){
        					$class = 'on';
        				}elseif( $i == 2 ){
        					$class = 'on current';
        				}else{
        					$class = '';
        				}
        				?>
 <li class="<?php echo $class; ?>">
                    <span class="num"><?php echo $i ?></span>
                    <h3>{{ $value->title }}</h3>
                    <p></p><p>{{ $value->description }}</p>
<p></p>
                </li>
        				<?php
        				$i++;
        			}

        	}
        ?>
                 
                                
            </ul>
        </div>
      
        <div class="phone iphone">
            <div class="mask">
                <ul class="screens step-2" data-step-notified="true">
                                        <li class="screen-1 on"><img src="{!! asset('theme/site/images/1473413718.png') !!}" alt="Sidecar iPhone screen"></li>
                                        <li class="on current"><img src="{!! asset('theme/site/images/1473413732.png') !!}" alt="Sidecar iPhone screen"></li>
                                        <li class=""><img src="{!! asset('theme/site/images/1473413737.png') !!}" alt="Sidecar iPhone screen"></li>
                                        <li class=""><img src="{!! asset('theme/site/images/1473413776.png') !!}" alt="Sidecar iPhone screen"></li>
                                        
                </ul>
            </div>
        </div>
    </div>
</section>
<!--How-you-ride-closed-->
@stop
