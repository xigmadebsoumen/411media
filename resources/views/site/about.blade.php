@extends('_layouts.site')
@section('content')

<button class="cd-nav-trigger cd-image-replace">Open navigation<span aria-hidden="true"></span></button>

<section class="about-area first-blocks">
	<div class="container">
		<h1 class="heading">{!! $about->title !!}</h1>

		 {!! $about->description !!}
		 
		<div class="our-team-grid">
			<ul class="list-unstyled">
				<li>
				<?php $count=0;
					foreach ($member as $key => $value) {
					?>
					<?php $id=$value->id; ?>
					<?php if($id==1){ ?>
					<div class="row">
						<div class="col-sm-6">
							
						{!! Html::image('products/crop/members/'.$value->image,$value->name, array("class"=>"img-responsive")) !!}	
						</div>
						<div class="col-sm-6 details-area">
							<h4 class="name"><a href="#"><?php print $value->name; ?></a></h4>
							<p class="position">fzdf</p>
						</div>
					</div>
				</li>
				<?php } ?>

		
					<?php if($id==2){ ?>
				<li>
					<div class="row">
						<div class="col-sm-6 details-area">
							<h4 class="name"><a href="#"><?php print $value->name; ?></a></h4>
							<p class="position">Chief Inspector</p>
						</div>
						<div class="col-sm-6">
							{!! Html::image('products/crop/members/'.$value->     image,$value->name, array("class"=>"img-responsive")) !!}	
						</div>
					</div>
				</li>
				<?php } ?>
				<?php } ?>
			</ul>
		</div>
	</div>
</section>
@stop	
