@extends('_layouts.timeline')
@section('content')
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement">
            <img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="mibble-short">
            <h3 class="short-area-heading">Notifications</h3>
            <ul class="noti-list">
                <li>
                    <div class="radio">
                        <label class="checkbox">
                            <input type="checkbox" value=""><i></i>It is a long established fact that a reader </label>
                    </div>
                </li>
                <li>
                    <div class="radio">
                        <label class="checkbox">
                            <input type="checkbox" value=""><i></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                    </div>
                </li>
                <li>
                    <div class="radio">
                        <label class="checkbox">
                            <input type="checkbox" value=""><i></i>Lorem Ipsum has been the industry's standard dummy text ever </label>
                    </div>
                </li>
                <li>
                    <div class="radio">
                        <label class="checkbox">
                            <input type="checkbox" value=""><i></i>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</label>
                    </div>
                </li>
                <li>
                    <div class="radio">
                        <label class="checkbox">
                            <input type="checkbox" value=""><i></i>Latin words, consectetur, from a Lorem Ipsum passage,</label>
                    </div>
                </li>
                <li>
                    <div class="radio">
                        <label class="checkbox">
                            <input type="checkbox" value=""><i></i>There are many variations of passages of Lorem Ipsum available, but the majority</label>
                    </div>
                </li>
            </ul>
            <ul class="button-list">
                <li><a href="#" class="pop-button login-buttom">Save</a>
                </li>
                <li><a href="#" class="pop-button login-buttom">Cancel</a>
                </li>
            </ul>
        </div>
    </div>
</section>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">
    $('#Formvalidate').validate({
        focusInvalid: false,
        rules: {
            confirm_password: {
                equalTo: "#new_password"
            }
        }
    });
</script>
@stop
