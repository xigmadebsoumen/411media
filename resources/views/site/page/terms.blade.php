@extends('_layouts.timeline')
@section('content')
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement">
            <img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        {!! Form::Model($user,array('route' => array('site.user.updatepassword'),'method' => 'post','id' => 'Formvalidate')) !!}
        <div class="mibble-short">
            <h3 class="short-area-heading">Terms and Condition</h3>
            <div class="panel-body">
                To start displaying ads, please review and accept our terms and conditions below
                <div class="hidden-box">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>.
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>
                </div>
                <ul class="noti-list">
                    <li>
                        <div class="radio">
                            <label class="checkbox">
                                <input type="checkbox" value=""><i></i>It is a long established fact that a reader </label>
                        </div>
                    </li>
                    <li>
                        <div class="radio">
                            <label class="checkbox">
                                <input type="checkbox" value=""><i></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</label>
                        </div>
                    </li>
                    <li>
                        <div class="radio">
                            <label class="checkbox">
                                <input type="checkbox" value=""><i></i>Lorem Ipsum has been the industry's standard dummy text ever </label>
                        </div>
                    </li>
                    <li>
                        <div class="radio">
                            <label class="checkbox">
                                <input type="checkbox" value=""><i></i>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots</label>
                        </div>
                    </li>
                </ul>
            </div>
            <ul class="button-list">
                <li>
                    <input type="submit" class="btn pop-button login-buttom" value="Cancel">
                </li>
                <li>
                    <input type="submit" class="btn pop-button login-buttom" value="I accept">
                </li>
            </ul>
        </div>
        {!! Form::close() !!}
    </div>
</section>
@stop