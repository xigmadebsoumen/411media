@extends('_layouts.search')
@section('content')
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement">
            <img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="mibble-short">
            <h3 class="short-area-heading">Copyright</h3>
            <div class="panel-group">
                <div class="panel-body">
                    <ul class="copy-right">
                        <li>
                            <a href="javascript:void(0)"><img src="{!! asset('theme/site/images/good.jpg') !!}"> 
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="{!! asset('theme/site/images/soso.jpg') !!}"> 
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="{!! asset('theme/site/images/poor.jpg') !!}"> 
                            </a>
                        </li>
                    </ul>
                    <p>You have 0 copyright strikes.</p>
                    <ul class="copy-right-list">
                        <li>
                            <div class="time-line-box text-post">
                                <a href="javascript:void(0)">
                                    <figure class="text-post-place">
                                        <img src="{!! asset('theme/site/images/profile03.jpg') !!}"> 
                                        <div class="time-post">52m</div>
                                    </figure>
                                    <figcaption class="tme-content">
                                        It is a long established fact that a reader will be......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-share-alt" aria-hidden="true"></i>253</a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i>25</a>
                                    </li>
                                </ul>
                            </div>
                            <!--time-line-box end-->
                        </li>
                        <li>
                            <div class="time-line-box photo-post-place">
                                <a href="javascript:void(0)">
                                    <figure class="photo-post">
                                        <img src="{!! asset('theme/site/images/Originalimage.jpg') !!}"> 
                                        <div class="time-post"><a href="javascript:void(0)">52m</a>
                                        </div>
                                    </figure>
                                    <figcaption class="tme-content">
                                        It is a long established fact that a reader will be......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 256</a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-share-alt" aria-hidden="true"></i>253</a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i>253</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="time-line-box video">
                                <a href="javascript:void(0)">
                                    <figure class="video-raw">
                                        <div class="video-file">
                                            <video id="video" class="fullscreen-bg__video" loop>
                                                <source src="{!! asset('theme/site/media/intro.mp4') !!}" type="video/mp4"> 
                                            </video>
                                            <div id="video-controls">
                                                <button type="button" id="play" class="play">Play</button>
                                                <button type="button" id="pause" class="pause">Play</button>
                                                <input type="range" id="seek-bar" value="0">
                                                <button type="button" id="mute" class="mute">Mute</button>
                                                <button type="button" id="unmute" class="unmute">UnMute</button>
                                                <input type="range" id="volume-bar" min="0" max="1" step="0.1" value="1">
                                                <button type="button" id="full-screen" class="fullscreen">Full-Screen</button>
                                            </div>
                                        </div>
                                        <div class="embed-video">
                                            <iframe id="embedvideo" src="https://www.youtube.com/embed/gASAL2CxDGs" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        <div class="time-post"><a href="javascript:void(0)">52m</a> 
                                        </div>
                                        <span id="play-video" class="video-icon"><a href="javascript:void(0)"><img src="{!! asset('theme/site/images/playbutton.png') !!}"></a></span>
                                    </figure>
                                    <figcaption class="tme-content">
                                        It is a long established fact that a reader will be......
                                    </figcaption>
                                    <ul class="two-list-all">
                                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                                        </li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-share-alt" aria-hidden="true"></i>253</a>
                                        </li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i>253</a>
                                        </li>
                                    </ul>
                                </a>
                            </div>
                            <div class="copy-fit"><img src="{!! asset('theme/site/images/good.jpg') !!}">
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="time-line-box blog-post-place">
                                <a href="javascript:void(0)">
                                    <figure class="blog-post">
                                        <img src="{!! asset('theme/site/images/ha_0512.jpg') !!}">
                                        <div class="time-post"><a href="javascript:void(0)">52m</a>
                                        </div>
                                        <span class="blog-icon"><a href="javascript:void(0)">
                                                <img src="{!! asset('theme/site/images/blog-icon.png') !!}"></a></span>
                                    </figure>

                                    <figcaption class="tme-content">
                                        It is a long established fact that a reader will be......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-share-alt" aria-hidden="true"></i>253</a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i>25</a>
                                    </li>
                                </ul>
                            </div>
                            <!--time-line-box end-->
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">
    $('#Formvalidate').validate({
        focusInvalid: false,
        rules: {
            confirm_password: {
                equalTo: "#new_password"
            }
        }
    });
</script>
@stop
