@extends('_layouts.home')
@section('content')
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        {!! Form::Model($user,array('route' => array('site.user.updatepassword'),'method' => 'post','id' => 'Formvalidate')) !!}
        <div class="mibble-short">
            <h3 class="short-area-heading">Privacy</h3>
            <div class="setting-place">
                <span class="privacy-heading">
                    Private Account
                    <label class="switch">
                        <input type="checkbox" checked>
                        <div class="slider round"></div>
                    </label>
                </span>
                <div class="clearfix"></div>
                <div class="privacy-place">
                    <div class="info-box">
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
                    </div>
                </div>
                <span class="privacy-heading">
                    Following
                    <label class="switch">
                        <input type="checkbox" checked>
                        <div class="slider round"></div>
                    </label>
                </span>
                <div class="clearfix"></div>
                <div class="privacy-place">
                    <div class="info-box">
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
                    </div>
                </div>
                <span class="privacy-heading">
                    Adult Safe
                    <label class="switch">
                        <input type="checkbox" checked>
                        <div class="slider round"></div>
                    </label>
                </span>
                <div class="clearfix"></div>
                <div class="privacy-place">
                    <div class="info-box">
                        Adult Safe will block all adult content post form your website.
                    </div>
                </div>
            </div>
            <!--setting-place end-->
            <ul class="button-list">
                <li>
                    <input type="submit" class="btn pop-button login-buttom" value="Back">
                </li>
            </ul>
        </div>
        {!! Form::close() !!}
    </div>
</section>
@stop