@extends('_layouts.timeline')
@section('content')
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement">
            <img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="mibble-short">
            <h3 class="short-area-heading">Blocked Users</h3>
            <div class="setting-place">
                <ul class="block-list">
                    <li>
                        <a href="#">
                            <div class="blockedphoto"><img src="{!! asset('theme/site/images/donts-taking-perfect.jpeg') !!}">
                            </div>
                            <div class="blockedphotoname">Ladla Malik </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="blockedphoto"><img src="{!! asset('theme/site/images/donts-taking-perfect.jpeg') !!}">
                            </div>
                            <div class="blockedphotoname">Karthick </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="blockedphoto"><img src="{!! asset('theme/site/images/donts-taking-perfect.jpeg') !!}">
                            </div>
                            <div class="blockedphotoname">Yogendra Katiya </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="blockedphoto"><img src="{!! asset('theme/site/images/donts-taking-perfect.jpeg') !!}">
                            </div>
                            <div class="blockedphotoname"> Sami Khan</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="blockedphoto"><img src="{!! asset('theme/site/images/donts-taking-perfect.jpeg') !!}">
                            </div>
                            <div class="blockedphotoname">Saren Delos Santos</div>
                        </a>
                    </li>
                </ul>
            </div>
            <ul class="button-list">
                <li>
                    <input type="submit" class="btn pop-button login-buttom" value="Done">
                </li>
            </ul>
        </div>
    </div>
</section>
@stop