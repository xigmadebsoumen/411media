@extends('_layouts.site')
@section('content')
<div class="payment">
	<div class="container">
		<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-7">
				<div class="artistprofile-cont">
				<h2>Payment</h2>
					<div class="ckeckout-sec">
			{!! Form::open(array('route'=>'order.save', 'role'=>'form', 'method' => 'post')) !!}
			<div class="col-md-6 cfr">
				<div class="round">
					<h2>1</h2>
				</div>
				<h3>BILLING ADDRESS</h3>
				<hr>
				<input type="hidden" name="product_id" value="{{$stores->id}}">
				<input type="hidden" name="price" value="{{$stores->price}}">
				<input type="hidden" name="product_name" value="{{$stores->album_name}}">
				<div class="checkout-form">
					<div class="form-group">
						<label>first name:</label>
						<input type="text" name="first_name" id="first_name" class="form-control">
					</div>
					<div class="form-group">
						<label>Last name:</label>
						<input type="text" name="last_name" id="last_name" class="form-control">
					</div>
					<div class="form-group">
						<label>Email:</label>
						<input type="email" name="email" id="email" class="form-control">
					</div>
					<div class="form-group">
						<label>Phone number:</label>
						<input type="text" name="phone_number" id="phone_number" class="form-control">
					</div>
					<div class="form-group">
						<label>Address:</label>
						<textarea name="address" id="address" class="form-control"></textarea>
					</div>
					
				</div>
				<div class="clearfix"></div>
			</div>
			
			<div class="col-md-6">
				<div class="round">
					<h2>2</h2>
				</div>
				<h3>REVIEW YOUR ORDER</h3>
				<hr>
				<div class="checkout-form">
					<div class="ord-his">
						<div class="ordlist">
							<div class="row">
								<div class="col-md-7 col-sm-7 col-xs-7">
									<h4>{!! $stores->album_name !!}</h4>
									<p>Album : {!! date('Y',$stores->album_launch_date) !!}</p>
								</div>
								<div class="col-md-5 col-sm-5 col-xs-5 rupee">
									<h3><i class="fa fa-inr" aria-hidden="true"></i> {!! $stores->price !!}</h3>
								</div>
							</div>
						</div>
						
						<div class="ordlist">
							<div class="row">
								<div class="col-md-7 col-sm-7 col-xs-7">
									<h2>Subtotal</h2>
								</div>
								<div class="col-md-5 col-sm-5 col-xs-5 rupee">
									<h3><i class="fa fa-inr" aria-hidden="true"></i> {!! $stores->price !!}</h3>
								</div>
							</div>
						</div>
						<div class="clearfix">&nbsp;</div>
						<div class="col-md-12">
							
							<p><label><input name="agree" id="agree" type="checkbox"> &nbsp;I Agree</label></p>
						</div>
					</div>
					<!-- <div class="form-group">
						<input type="submit" id="submit" value="Place order" class="placeorder">
					</div> -->
				</div>
			</div>
			{!! Form::close() !!}
		</div>
				</div>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
$('#submit').on('click', function(e) {

	if($('#first_name').firstNameCheck() & $('#last_name').lastNameCheck() &
		$('#email').validateEmail() & $('#phone_number').commonCheck() & 
		$('#address').commonCheck() & $('#agree').checkRadioOrCheck()){
		return true;
	} else {
		return false;
	}
	e.preventDefault();
})
</script>
@stop