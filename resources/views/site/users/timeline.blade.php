@extends('_layouts.timeline')
@section('content')

<?php

use App\Postviews;
use App\Postlikes;

function countPostViews($id) {
    //$viewcount = DB::table('postviews')->where('post_id', '=', $id)->select('postviews.*')->count();
    $dtCoutVal = Postviews::where('post_id', $id)->count();
    $dtCout = $dtCoutVal != 0 ? $dtCoutVal : $dtCoutVal;
    return $dtCout;
}

function countPostLikes($id) {
    //$viewcount = DB::table('postviews')->where('post_id', '=', $id)->select('postviews.*')->count();
    $dtCoutVal = Postlikes::where('post_id', $id)->count();
    $dtCout = $dtCoutVal != 0 ? $dtCoutVal : $dtCoutVal;
    return $dtCout;
}

function instagramLink($link) {
    $arrExp = explode("/?", $link);
    return $arrExp[0] . "/embed";
}

$adultUrl = "http://xigma.website/dev10/4elevenadult/public/";

?>

<?php //echo "<pre>"; print_r($post); echo "<pre>"; print_r($users); exit;?>
<section class="body-section">
    <div class="container-fluid">
        <div class="top-addvertisement">
            <img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="time-line-area text-center">
            <h3 class="short-area-heading2">Timeline</h3>
            <!--time-line-box start-->
            <?php if (!empty($post)) { ?>
                <?php foreach ($post as $pdt) { ?>
                    <?php if ($pdt['Type'] == 'post') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                        <div class="time-line-box text-post">
                            <a href="{!! URL::to('showpost/'.base64_encode($pdt['Post']->id)) !!}">
                                <figure class="text-post-place">
                                    {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                </figure>
                            </a>
                            <figcaption class="tme-content" style="color: <?php echo $pdt['Post']->textcol == '' ? '' : $pdt['Post']->textcol; ?>; background-color: <?php echo $pdt['Post']->bgcol == '' ? '' : $pdt['Post']->bgcol; ?>" >
                                <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                            </figcaption>
                            <ul class="two-list-all">
                                <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                </li>
                                <li><span href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></span>
                                </li>
                            </ul>
                            <div class="sml-profile-bar">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <div class="row">
                                        <figure class="sml-pro-img">
                                            {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                        </figure> 
                                        <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="row">
                                        <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div> 
                        <?php } else { ?>
                        <div class="time-line-box text-post">
<!--                            <a href="<?php echo $adultUrl."showpost/".base64_encode($pdt['Post']->id)?>" >-->
                            <a href="<?php echo $adultUrl."adult/showpost/".base64_encode($users[0]->id)."/".base64_encode($pdt['Post']->id)?>" >
                                <figure class="text-post-place">
                                    <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                </figure>
                            </a>
                            <figcaption class="tme-content" style="color: <?php echo $pdt['Post']->textcol == '' ? '' : $pdt['Post']->textcol; ?>; background-color: <?php echo $pdt['Post']->bgcol == '' ? '' : $pdt['Post']->bgcol; ?>" >
                                <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                            </figcaption>
                            <ul class="two-list-all">
                                <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                </li>
                                <li><span href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></span>
                                </li>
                            </ul>
                            <div class="sml-profile-bar">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <div class="row">
                                        <figure class="sml-pro-img">
                                            {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                        </figure> 
                                        <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="row">
                                        <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div> 
                        <?php } ?>
                    <?php } else if ($pdt['Type'] == 'twitterlink'){ ?>
                        <div class="time-line-box link-post-place">
<!--                            <a href="{!! URL::to('showlink/'.base64_encode($pdt['Post']->id)) !!}">-->
                                <figure class="link-post">
                                    <div class="link-post-image">
                                        <!-- <img src="{!! asset('theme/site/images/people-drinkin.jpg') !!}"> -->
            <!--                                        <iframe border=0 frameborder=0 height=250 width=550 scrolling="no" src="<?php echo $pdt['Post']->link; ?>"></iframe>-->
                                        <iframe border=0 frameborder=0 height=250 width=550 scrolling="no" src="http://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2Fjack%2Fstatus%2F20"></iframe>
                                    </div>
                                    <div class="link-post-content">
                                        <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                                    </div>
                                </figure>
<!--                            </a>-->
                            <ul class="two-list-all">
                                <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                </li>
                                <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                </li>
                            </ul>
                            <div class="sml-profile-bar">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <div class="row">
                                        <figure class="sml-pro-img">
                                            {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                        </figure> 
                                        <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="row">
                                        <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>            
                    <?php } else if ($pdt['Type'] == 'instagramlink'){ ?>
                        <div class="time-line-box link-post-place">
                            <!--                                <a href="{!! URL::to('showlink/'.base64_encode($pdt['Post']->id)) !!}">-->
                            <figure class="link-post">
                                <div class="link-post-image">
                                    <!-- <img src="{!! asset('theme/site/images/people-drinkin.jpg') !!}"> -->
                                    <iframe width="100%" height="100%" scrolling="no" src="<?php echo instagramLink($pdt['Post']->link); ?>" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <div class="link-post-content">
                                    <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                                </div>
                            </figure>
                            <!--                                </a>-->
                            <ul class="two-list-all">
                                <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                </li>
                                <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                </li>
                            </ul>
                            <div class="sml-profile-bar">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <div class="row">
                                        <figure class="sml-pro-img">
                                            {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                        </figure> 
                                        <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="row">
                                        <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>                 
                    <?php } else if ($pdt['Type'] == 'link') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                            <div class="time-line-box link-post-place">
                                <a href="{!! URL::to('showlink/'.base64_encode($pdt['Post']->id)) !!}">
                                    <figure class="link-post">
                                        <div class="link-post-image">
                                            <!--<img src="{!! asset('theme/site/images/people-drinkin.jpg') !!}">-->
                                            <iframe width="100%" scrolling="no" src="<?php echo substr($pdt['Post']->link, 0, 7) === "http://" ? $pdt['Post']->link : "http://" . $pdt['Post']->link; ?>" style="-webkit-transform:scale(1.0);-moz-transform-scale(1.0);"></iframe>
                                        </div>
                                        <div class="link-post-content">
                                            <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                                        </div>
                                    </figure>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="time-line-box link-post-place">
                                <!--<a href="<?php echo $adultUrl."showlink/".base64_encode($pdt['Post']->id)?>" >-->
                                <a href="<?php echo $adultUrl."adult/showlink/".base64_encode($users[0]->id)."/".base64_encode($pdt['Post']->id)?>" >
                                    <figure class="link-post">
                                        <div class="link-post-image">
                                           <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                        </div>
                                        <div class="link-post-content">
                                            <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                                        </div>
                                    </figure>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        <?php } ?>            
                    <?php } else if ($pdt['Type'] == 'photo') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                            <div class="time-line-box photo-post-place">
                                <a href="{!! URL::to('showphoto/'.base64_encode($pdt['Post']->id)) !!}">
                                    <figure class="photo-post">
                                        <?php if ($pdt['Post']->image != '') { ?>
                                            <img src="{!! asset('products/main/post/'.$pdt['Post']->image) !!}">
                                        <?php } else { ?>
                                            <?php echo $pdt['Post']->embed; ?>
                                        <?php } ?>                                   
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->caption, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>

                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div> 
                        <?php } else { ?>
                            <div class="time-line-box photo-post-place">
                                <!--<a href="<?php echo $adultUrl."showphoto/".base64_encode($pdt['Post']->id)?>" >-->
                                <a href="<?php echo $adultUrl."adult/showphoto/".base64_encode($users[0]->id)."/".base64_encode($pdt['Post']->id)?>" >
                                    <figure class="photo-post">
                                        <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->caption, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>

                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div> 
                        <?php } ?>            
                    <?php } else if ($pdt['Type'] == 'album') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                            <div class="time-line-box photo-post-place">
                                <a href="{!! URL::to('showalbum/'.base64_encode($pdt['Post']->id)) !!}">
                                    <figure class="photo-post">
                                        <img src="{!! asset('products/main/post/'.$pdt['Post']->image) !!}">
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->caption, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div>
                        <?php } else { ?>
                            <div class="time-line-box photo-post-place">
                                <a href="<?php echo $adultUrl."adult/showalbum/".base64_encode($users[0]->id)."/".base64_encode($pdt['Post']->id)?>" >
                                    <figure class="photo-post">
                                        <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->caption, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <!--<span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>-->
                            </div>            
                        <?php } ?>
                    <?php } else if ($pdt['Type'] == 'blog') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                            <div class="time-line-box blog-post-place">
                                <a href="{!! URL::to('showblog/'.base64_encode($pdt['Post']->id)) !!}">
                                    <figure class="blog-post">
                                        <img src="{!! asset('products/main/post/'.$pdt['Post']->image) !!}">
                                        <span class="blog-icon"><img src="{!! asset('theme/site/images/blog-icon.png') !!}"></span>
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div> 
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div>
                        <?php } else { ?>
                            <div class="time-line-box blog-post-place">
                                <!--<a href="<?php echo $adultUrl."showblog/".base64_encode($pdt['Post']->id)?>" >-->
                                <a href="<?php echo $adultUrl."adult/showblog/".base64_encode($users[0]->id)."/".base64_encode($pdt['Post']->id)?>" >
                                    <figure class="photo-post">
                                        <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                    </figcaption>
                                </a>                                    
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div> 
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div>
                        <?php } ?>            
                    <?php } else if ($pdt['Type'] == 'video') { ?>
                        <?php if ($pdt['Post']->video != "" || $pdt['Post']->embed != "") { ?>
                            <?php if($pdt['Post']->is_adult != 1){ ?> 
                                <div class="time-line-box video">
                                    <figure class="video-raw">
                                        <?php if ($pdt['Post']->video != "") { ?>
                                            <?php $thvid = $pdt['Post']->video ?>
                                            <video width="400" controls>
                                                <source src="{!! asset('products/main/post/'.$thvid) !!}" type="video/mp4">
                                            </video>
                                        <?php } else if ($pdt['Post']->embed != "") { ?>
                                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/KuQkQ9usmm4" frameborder="0" allowfullscreen></iframe>
                                        <?php } ?>
                                        <div class="time-post"><a href="javascript:void(0)"><?php echo $pdt['Time'] ?></a>
                                        </div>
<!--                                        <span id="play-video" class="video-icon"><a href="javascript:void(0)"><img src="{!! asset('theme/site/images/playbutton.png') !!}"></a></span>-->
                                    </figure>
                                    <a href="{!! URL::to('showvideo/'.base64_encode($pdt['Post']->id)) !!}">
                                        <figcaption class="tme-content">
                                            <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                        </figcaption>
                                    </a>
                                    <ul class="two-list-all">
                                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                        </li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                        </li>
                                    </ul>
                                    <div class="sml-profile-bar">
                                        <div class="col-xs-8 col-sm-8 col-md-8">
                                            <div class="row">
                                                <figure class="sml-pro-img">
                                                    {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                                </figure> 
                                                <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <div class="row">
                                                <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                                </div>
                            <?php } else { ?>
                                <div class="time-line-box video">
                                    <a href="<?php echo $adultUrl."adult/showvideo/".base64_encode($users[0]->id)."/".base64_encode($pdt['Post']->id)?>" >
                                        <figure class="photo-post">
                                            <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                        </figure>
                                        <figcaption class="tme-content">
                                            <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                        </figcaption>
                                    </a>                                    
                                    <!--
                                    <figure class="video-raw">
                                        <?php if ($pdt['Post']->video != "") { ?>
                                            <?php $thvid = $pdt['Post']->video ?>
                                            <video width="400" controls>
                                                <source src="{!! asset('products/main/post/'.$thvid) !!}" type="video/mp4">
                                            </video>
                                        <?php } else if ($pdt['Post']->embed != "") { ?>
                                            <?php echo $pdt['Post']->title; ?>
                                        <?php } ?>
                                        <div class="time-post"><a href="javascript:void(0)"><?php echo $pdt['Time'] ?></a>
                                        </div>
                                        <span id="play-video" class="video-icon"><a href="javascript:void(0)"><img src="{!! asset('theme/site/images/playbutton.png') !!}"></a></span>
                                    </figure>
                                    <a href="<?php echo $adultUrl."showvideo/".base64_encode($pdt['Post']->id)?>" >
                                        <figcaption class="tme-content">
                                            <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                        </figcaption>
                                    </a>
                                    -->
                                    <ul class="two-list-all">
                                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                        </li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                        </li>
                                    </ul>
                                    <div class="sml-profile-bar">
                                        <div class="col-xs-8 col-sm-8 col-md-8">
                                            <div class="row">
                                                <figure class="sml-pro-img">
                                                    {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                                </figure> 
                                                <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <div class="row">
                                                <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                                </div>
                            <?php } ?>            
                        <?php } ?>                
                    <?php } ?>
                <?php } ?>
            <?php } else { ?>
                <h1>You Have No Posts</h1>
            <?php } ?>

            <!--
            <div class="time-line-box photo-post-place">
                <a href="javascript:void(0)">
                    <figure class="photo-post">
                        <img src="{!! asset('theme/site/images/Originalimage.jpg') !!}">
                        <div class="bottom-profile-details">
                            <a href="javascript:void(0)">
                                <figure class="profile-pic"><img src="{!! asset('theme/site/images/profile02.png') !!}">
                                </figure>
                                <figcaption class="profile-name">Lorimay G. Siason</figcaption>
                            </a>
                        </div>
                        <div class="time-post"><a href="javascript:void(0)">52m</a>
                        </div>
                    </figure>
                    <figcaption class="tme-content">
                        It is a long established fact that a reader will be......
                    </figcaption>
                    <ul class="two-list-all">
                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                        </li>
                        <li><a href="javascript:void(0)"><i class="fafa-smile-o" aria-hidden="true"></i>253</a>
                        </li>
                    </ul>
                    <a href="javascript:void(0)" class="shareby">Shared by T-vizzCool</a>
                </a>
            </div>
            <div class="time-line-box blog-post-place">
                <a href="javascript:void(0)">
                    <figure class="blog-post">
                        <img src="{!! asset('theme/site/images/ha_0512.jpg') !!}">
                        <div class="profile-details">
                            <a href="javascript:void(0)">
                                <figure class="profile-pic"><img src="{!! asset('theme/site/images/profile02.png') !!}">
                                </figure>
                                <figcaption class="profile-name">Lorimay G. Siason</figcaption>
                            </a>
                        </div>
                        <div class="time-post"><a href="javascript:void(0)">52m</a>
                        </div>
                        <span class="blog-icon"><a href="javascript:void(0)"><img src="{!! asset('theme/site/images/blog-icon.png') !!}"></a></span>
                    </figure>
                    <figcaption class="tme-content">
                        It is a long established fact that a reader will be......
                    </figcaption>
                    <ul class="two-list-all">
                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                        </li>
                        <li><a href="javascript:void(0)"><i class="fafa-smile-o" aria-hidden="true"></i>253</a>
                        </li>
                    </ul>
                    <a href="javascript:void(0)" class="shareby">Shared by T-vizzCool</a>
                </a>
            </div>
            <div class="time-line-box link-post-place">
                <a href="javascript:void(0)">
                    <figure class="link-post">
                        <div class="link-post-image"><img src="{!! asset('theme/site/images/people-drinkin.jpg') !!}">
                        </div>
                        <div class="link-post-content">
                            <h4>Naruto vs. Sasuke | Final Battle</h4>
                            <p>It is a long established fact...</p>
                            YOUTUBE.COM
                        </div>
                        <div class="profile-details">
                            <a href="javascript:void(0)">
                                <figure class="profile-pic"><img src="{!! asset('theme/site/images/profile02.png') !!}">
                                </figure>
                                <figcaption class="profile-name">Lorimay G. Siason</figcaption>
                            </a>
                        </div>
                        <div class="time-post"><a href="javascript:void(0)">52m</a>
                        </div>
                    </figure>
                    <figcaption class="tme-content">
                        It is a long established fact that a reader will be......
                    </figcaption>
                    <ul class="two-list-all">
                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                        </li>
                        <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i>253</a>
                        </li>
                    </ul>
                    <a href="javascript:void(0)" class="shareby">Shared by T-vizz Cool</a>
                </a>
            </div>
            <div class="time-line-box link-post-place">
                <a href="javascript:void(0)">
                    <figure class="link-post">
                        <div class="link-post-image"><img src="{!! asset('theme/site/images/people-drinkin.jpg') !!}">
                        </div>
                        <div class="link-post-content">
                            <h4>Naruto vs. Sasuke | Final Battle</h4>
                            <p>It is a long established fact...</p>
                            YOUTUBE.COM
                        </div>
                        <div class="profile-details">
                            <a href="javascript:void(0)">
                                <figure class="profile-pic"><img src="{!! asset('theme/site/images/profile02.png') !!}">
                                </figure>
                                <figcaption class="profile-name">Lorimay G. Siason</figcaption>
                            </a>
                        </div>
                        <div class="time-post"><a href="javascript:void(0)">52m</a>
                        </div>
                    </figure>
                    <figcaption class="tme-content">
                        It is a long established fact that a reader will be......
                    </figcaption>
                    <ul class="two-list-all">
                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                        </li>
                        <li><a href="javascript:void(0)"><i class="fafa-smile-o" aria-hidden="true"></i>253</a>
                        </li>
                    </ul>
                </a>
            </div>
            <div class="time-line-box photo-post-place">
                <a href="javascript:void(0)">
                    <figure class="photo-post">
                        <img src="{!! asset('theme/site/images/Originalimage.jpg') !!}">
                        <div class="bottom-profile-details">
                            <a href="javascript:void(0)">
                                <figure class="profile-pic"><img src="{!! asset('theme/site/images/profile02.png') !!}">
                                </figure>
                                <figcaption class="profile-name">Lorimay G. Siason</figcaption>
                            </a>
                        </div>
                        <div class="time-post"><a href="javascript:void(0)">52m</a>
                        </div>
                    </figure>
                    <figcaption class="tme-content">
                        It is a long established fact that a reader will be......
                    </figcaption>
                    <ul class="two-list-all">
                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                        </li>
                        <li><a href="javascript:void(0)"><i class="fafa-smile-o" aria-hidden="true"></i>253</a>
                        </li>
                    </ul>
                    <a href="javascript:void(0)" class="shareby">Shared by T-vizzCool</a>
                </a>
            </div>
            <div class="time-line-box blog-post-place">
                <a href="javascript:void(0)">
                    <figure class="blog-post">
                        <img src="{!! asset('theme/site/images/ha_0512.jpg') !!}">
                        <div class="profile-details">
                            <a href="javascript:void(0)">
                                <figure class="profile-pic"><img src="{!! asset('theme/site/images/profile02.png') !!}">
                                </figure>
                                <figcaption class="profile-name">Lorimay G. Siason</figcaption>
                            </a>
                        </div>
                        <div class="time-post"><a href="javascript:void(0)">52m</a>
                        </div>
                        <span class="blog-icon"><a href="javascript:void(0)"><img src="{!! asset('theme/site/images/blog-icon.png') !!}"></a></span>
                    </figure>
                    <figcaption class="tme-content">
                        It is a long established fact that a reader will be......
                    </figcaption>
                    <ul class="two-list-all">
                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                        </li>
                        <li><a href="javascript:void(0)"><i class="fafa-smile-o" aria-hidden="true"></i>253</a>
                        </li>
                    </ul>
                    <a href="javascript:void(0)" class="shareby">Shared by T-vizzCool</a>
                </a>
            </div>
            <div class="time-line-box link-post-place">
                <a href="javascript:void(0)">
                    <figure class="link-post">
                        <div class="link-post-image"><img src="{!! asset('theme/site/images/people-drinkin.jpg') !!}">
                        </div>
                        <div class="link-post-content">
                            <h4>Naruto vs. Sasuke | Final Battle</h4>
                            <p>It is a long established fact...</p>
                            YOUTUBE.COM
                        </div>
                        <div class="profile-details">
                            <a href="javascript:void(0)">
                                <figure class="profile-pic"><img src="{!! asset('theme/site/images/profile02.png') !!}">
                                </figure>
                                <figcaption class="profile-name">Lorimay G. Siason</figcaption>
                            </a>
                        </div>
                        <div class="time-post"><a href="javascript:void(0)">52m</a>
                        </div>
                    </figure>
                    <figcaption class="tme-content">
                        It is a long established fact that a reader will be......
                    </figcaption>
                    <ul class="two-list-all">
                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> 25336</a>
                        </li>
                        <li><a href="javascript:void(0)"><i class="fafa-smile-o" aria-hidden="true"></i>253</a>
                        </li>
                    </ul>
                    <a href="javascript:void(0)" class="shareby">Shared by T-vizz Cool</a>
                </a>
            </div>
            -->
            <!--time-line-boxend-->
            <divclass="addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <aid="loadMore" href="" class="load-button">Load more</a>
    </div>
</div>
</section> 
@stop