@extends('_layouts.timeline')
@section('content')
<div class="block">
    {!! Form::Model($users,array('route' => array('site.user.updateprofile',$users->id),'method' => 'put','id' => 'Formvalidate','files' => true)) !!}
    <section class="body-section">
        <div class="container">
            <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
            </div>
            <div class="mibble-short">
                <h3 class="short-area-heading">Edit Profile</h3>
                <form>
                    <ul class="change-list">
                        <li>
                            <span class="change-profile-photo">                                                                                                     
                                {!! (isset($users->image) && $users->image != "") ? Html::image('products/main/user/'.$users->image, $users->site_name, array('height' => 60, 'id' => 'opimg'))  : Html::image('devimg/blankuser.png', $users->site_name, array('height' => 60, 'id' => 'opimg'))  !!}
                            </span>
                            <span id="fileselector">
                                <label class="btn black-btm" for="upload-file-selector">
                                    <input class="upload-file-selector" id="upload-file-selector1" onchange="loadFileOpimg(event)" name="image" type="file">
                                    <i class="fa fa-user" aria-hidden="true"></i> <span>Change Profile Photo</span>
                                </label>
                                <span class="help-block">Input type JPG, JPEG, PNG</span>
                            </span>
                        </li>
                        <script>
                          var loadFileOpimg = function(event) {
                            var output = document.getElementById('opimg');
                            output.src = URL.createObjectURL(event.target.files[0]);
                          };
                          var loadFileObimg = function(event) {
                            var output = document.getElementById('obimg');
                            output.src = URL.createObjectURL(event.target.files[0]);
                          };                          
                        </script>
                        <li>
                            <span class="change-profile-photo">
                                {!! !empty($users->headerimage) ? Html::image('products/main/user/'.$users->headerimage, $users->site_name, array('height' => 60, 'id' => 'obimg'))  : Html::image('devimg/noimg2.png', $users->site_name, array('height' => 60, 'id' => 'obimg'))  !!}
                            </span>
                            <span id="fileselector">
                                <label class="btn black-btm" for="upload-file-selector">
                                    <input class="upload-file-selector" id="upload-file-selector2" onchange="loadFileObimg(event)" name="headerimage" type="file">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i> <span>Change Header Photo</span>
                                </label>
                                <span class="help-block">Input type JPG, JPEG, PNG</span>
                            </span>
                        </li>
                    </ul>
                    <div class="form-place">
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">First Name</label>
<!--                            <input type="text" class="form-control inputstyle" id="fname" name="fname" placeholder="Enter User Name">-->
                            {!! Form::text('fname',null, array('class' => 'form-control inputstyle'))  !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Last Name</label>
<!--                            <input type="text" class="form-control inputstyle" id="lname" name="lname" placeholder="Enter User Name">-->
                            {!! Form::text('lname',null, array('class' => 'form-control inputstyle'))  !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Profile Name</label>
<!--                            <input type="text" class="form-control inputstyle" id="lname" name="pname" placeholder="Enter Profile Name">-->
                            {!! Form::text('pname',null, array('class' => 'form-control inputstyle'))  !!}
                        </div>

                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Tagline</label>
<!--                            <input type="text" class="form-control inputstyle" id="lname" name="tagline" placeholder="Enter Tagline">-->
                            {!! Form::text('tagline',null, array('class' => 'form-control inputstyle'))  !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Website</label>
<!--                            <input type="text" class="form-control inputstyle" id="lname" name="website" placeholder="Enter Website">-->
                            {!! Form::text('website',null, array('class' => 'form-control inputstyle'))  !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Twitter</label>
<!--                            <input type="text" class="form-control inputstyle" id="lname" name="twitter" placeholder="Enter Twitter Address">-->
                            {!! Form::text('twitter',null, array('class' => 'form-control inputstyle'))  !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Facebook</label>
<!--                            <input type="text" class="form-control inputstyle" id="lname" name="facebook" placeholder="Enter Facebook Address">-->
                            {!! Form::text('facebook',null, array('class' => 'form-control inputstyle'))  !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Bio</label>
<!--                            <textarea class="form-control textarea-style" rows="5" id="lname" name="bio" placeholder="Write Bio"></textarea>-->
                            {!! Form::textarea('bio',null,array('class'=>'form-control textarea-style','placeholder'=>'Write Bio', 'size' => '30x10')) !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Location</label>
<!--                            <input type="text" class="form-control inputstyle" id="lname" name="location" placeholder="Enter Location">-->
                            {!! Form::text('location',null, array('class' => 'form-control inputstyle'))  !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Contact Email</label>
<!--                            <input type="email" class="form-control inputstyle" id="lname" name="contactemail" placeholder="Contact Email">-->
                            {!! Form::text('contactemail',null, array('class' => 'form-control inputstyle'))  !!}
                        </div>
                        <h3 class="privatebold">Private Information</h3>
                        <span class="privatespan">Email (this email will be the email used to login)</span>
                        <div class="form-group">
<!--                            <input type="email" class="form-control inputstyle" id="email" name="email" placeholder="Email address">-->
                            {!! Form::text('email',null, array('class' => 'form-control inputstyle', 'readonly' => 'readonly'))  !!}
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">New Password</label>
                            <input class="form-control inputstyle" name="new_password" id="new_password" value="" type="password">
                        </div>
                        <div class="form-group">
                            <label for="email" class="label-style mediun-size">Confirm Password</label>
                            <input class="form-control inputstyle" name="confirm_password" value="" type="password">
                        </div>                        
                        
                        
                    </div>
                </form>
                <ul class="button-list">
                    <li>
                        <input type="submit" class="btn pop-button login-buttom" value="Save">
                    </li>
                    <li>
                        <input type="button" onclick="backToProfile();" class="btn pop-button login-buttom" value="Cancel">
                    </li>
                </ul>
            </div>
        </div>
    </section>             
    {!! Form::close() !!}
</div>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">
    $('#Formvalidate').validate({
        focusInvalid: false,
        rules: {
            confirm_password: {
                equalTo: "#new_password"
            }
        }
    });
    
    function backToProfile(){
        $(location).attr('href', '{!! URL::to('profile') !!}');
    }
    
    
</script>
@stop
