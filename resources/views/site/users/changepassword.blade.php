@extends('_layouts.home')
@section('content')
<!-- PAGE CONTENT WRAPPER -->

<?php //echo "<pre>"; print_r($user); exit; ?>
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive"></div>
        <div class="mibble-short">
            <h3 class="short-area-heading">Change Password</h3>
            {!! Form::Model($user,array('route' => array('site.user.updatepassword'),'method' => 'post','id' => 'Formvalidate')) !!}
            <input type="hidden" name="id" value="{!! $user[0]->id !!}" >
                <div class="form-place">
                    <div class="form-group">
                        <label for="email" class="label-style mediun-size">Current Password</label>
                        <input type="password" class="form-control inputstyle" name="cpass" id="cpass" placeholder="Enter Current Password">
                    </div>
                    <div class="form-group">
                        <label for="email" class="label-style mediun-size">New Password</label>
                        <input type="password" class="form-control inputstyle" name="npass" id="npass" placeholder="Enter New Password">
                    </div>
                    <div class="form-group">
                        <label for="email" class="label-style mediun-size">Rewrite Password</label>
                        <input type="password" class="form-control inputstyle" name="rpass" id="rpass" placeholder="Enter Rewrite Password">
                    </div>
                </div>
                <ul class="button-list">
                    <li><input type="submit" class="btn pop-button login-buttom" value="Save"></li>
                    <li><button class="btn pop-button login-buttom" type="reset" value="Reset">Cancel</button></li>
                </ul>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@stop