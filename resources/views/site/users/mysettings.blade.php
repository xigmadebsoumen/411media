@extends('_layouts.timeline')
@section('content')
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement">
            <img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="mibble-short">
            <h3 class="short-area-heading">Settings</h3>
            <div class="setting-place">
                <a href="javascript:void(0)" class="setting-heading">Account</a>
                <ul class="setting-list">
                    <li><a href="{!! URL::to('my-profile') !!}">Edit Profile  <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    <li><a href="{!! URL::to('profile') !!}">My Profile<i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>
                    <!--<li><a href="javascript:void(0)">Change Password <i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>-->
                    <li><a href="{!! URL::to('privacy') !!}">Privacy  <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    <li><a href="{!! URL::to('blockuser') !!}">Blocked User <i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>
                    <li><a href="{!! URL::to('notifications') !!}">Notifications <i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>
                </ul>
                <a href="javascript:void(0)" class="setting-heading">Content</a>
                <ul class="setting-list">
                    <li><a href="{!! URL::to('copyright') !!}">Copyright<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0)">Adult Content<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0)">Monetize Post<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                </ul>
                <a href="javascript:void(0)" class="setting-heading">ABOUT US</a>
                <ul class="setting-list">
                    <li><a href="{!! URL::to('terms') !!}">Terms of Service<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    <li><a href="{!! URL::to('privacy') !!}">Privacy Policy  <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0)">DMCA <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                </ul>
                <ul class="settingbuttonlist">
                    <li><a href="javascript:void(0)"><i class="fa fa-history" aria-hidden="true"></i> Clear search history</a></li>
                    <li><a href="{!! URL::to('logout') !!}"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
                </ul>
            </div>
            <!--setting-place end-->
            <!--<ul class="button-list">
                <li>
                    <input type="submit" class="btn pop-button login-buttom" value="Save">
                </li>
                <li>
                    <input type="submit" class="btn pop-button login-buttom" value="Cancel">
                </li>
            </ul>-->
        </div>
    </div>
</section>
@stop