@extends('_layouts.search')
@section('content')
<?php
    use App\Postviews;
    use App\Postlikes;

    function countPostViews($id){
        //$viewcount = DB::table('postviews')->where('post_id', '=', $id)->select('postviews.*')->count();
        $dtCoutVal = Postviews::where('post_id', $id)->count();
        $dtCout = $dtCoutVal != 0 ? $dtCoutVal : $dtCoutVal;
        return $dtCout;
    }

    function countPostLikes($id){
        //$viewcount = DB::table('postviews')->where('post_id', '=', $id)->select('postviews.*')->count();
        $dtCoutVal = Postlikes::where('post_id', $id)->count();
        $dtCout = $dtCoutVal != 0 ? $dtCoutVal : $dtCoutVal;
        return $dtCout;
    }
    
    function instagramLink($link){
        $arrExp = explode("/?",$link);
        return $arrExp[0]."/embed";
    }
    
    $adultUrl = "http://xigma.website/dev10/4elevenadult/public/";
    
?>
<section class="body-section">
    <div class="container">
        <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
        </div>
        <div class="time-line-area text-center">
            <div class="home-profile-section">
                <div class="home-profile-back-image">
                    <?php if ($users->headerimage != "") { ?>
                        <img src="{!! asset('products/main/user/'.$users->headerimage) !!}">
                    <?php } else { ?>
                        <img src="{!! asset('devimg/noimg2.png') !!}">
                    <?php } ?>                    
                </div>
                <div class="home-profile-info">
                    <span class="profile-photo">
                        <?php if ($users->image != "") { ?>
                            <img src="{!! asset('products/crop/user/'.$users->image) !!}">
                        <?php } else { ?>
                            <img src="{!! asset('devimg/noimg.png') !!}">
                        <?php } ?>
                    </span>
                    <span class="home-profile-caption">{!! $users->tagline !!}</span>
                    <!--<a href="{!! URL::to('my-profile') !!}" class="edit-button">
                        <i class="fa fa-pencil" aria-hidden="true"></i> 
                        <span>Edit profile</span>
                    </a>-->
                    <?php if (!empty($usersdt)) { ?>
                        <?php if (empty($getFollow)) { ?>
                            <span id="follwnow">
                                <a href="javascript:void(0)" onclick="follownow('<?php echo $users->id?>','<?php echo $usersdt->id?>')" class="folow-button">
                                    <i class="fa fa-pencil" aria-hidden="true"></i> <span>Follow</span>
                                </a>
                            </span>                   
                        <?php } else { ?>                  
                            <span id="unfollwnow">
                                <a href="javascript:void(0)" onclick="unfollownow('<?php echo $users->id?>','<?php echo $usersdt->id?>')" class="folow-button black-bg">
                                    <i class="fa fa-pencil" aria-hidden="true"></i> <span>Unfollow</span>
                                </a>
                            </span>
                        <?php } ?>
                    <?php } ?>
                    
                    <script type="text/javascript">
                        function follownow(followto,followby){
                            //alert(11);
                            url = "{!! URL::to('follownow') !!}";
                            $.ajax({
                                url: url,
                                async: false,
                                type: 'POST',
                                data: {_token: "{{ csrf_token() }}", followto: followto, followby: followby},
                            }).done(function (response) {
                                //alert(response);
                                window.location.reload();
                            });                            
                        }
                        
                        function unfollownow(followto,followby){
                            url = "{!! URL::to('unfollownow') !!}";
                            $.ajax({
                                url: url,
                                async: false,
                                type: 'POST',
                                data: {_token: "{{ csrf_token() }}", followto: followto, followby: followby},
                            }).done(function (response) {
                                window.location.reload();
                            });                           
                        }                        
                    </script>                    
                    <h2 class="home-profile-name">{!! $users->pname !!}</h2>
                    <ul class="home-three-list">
                        <li><span>Following</span> <?php echo $follwing; ?>
                        <li>
                        <li><span>Followers</span> <?php echo $followers; ?>
                        <li>
                        <li><span>Views</span> <?php echo $views; ?>
                        <li>
                    </ul>
                    <span class="flaticon-expand-button pull-right rightdropdown" data-toggle="dropdown"></span>
                    <div class="dropdown-menu dropdown-menu-large row col-md-12 home-dropdown">
                        <div class="info-box">
                            <h3>Name</h3> {!! $users->fname." ".$users->lname !!}
                        </div>
                        <div class="info-box">
                            <h3>Bio</h3> {!! $users->bio !!}
                        </div>
                        <div class="info-box">
                            <h3>Website</h3>
                            <a href="#">{!! $users->website !!}</a>
                        </div>
                        <div class="info-box">
                            <h3>Location</h3> {!! $users->location !!}
                        </div>
                        <div class="info-box">
                            <h3>Links</h3>
                            <hr>
                            <ul class="social-icon-list">
                                <li><a href="{!! $users->facebook !!}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> {!! $users->facebook !!}</a>
                                </li>
                                <li><a href="{!! $users->twitter !!}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></i>{!! $users->twitter !!}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="info-box">
                            <h3>Contact Email</h3>
                            <a href="#">{!! $users->contactemail !!}</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!--home-profile-section end-->
            <div class="top-addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
            </div>
            
            
            
            
            
            
            
            
            
            
            
            
            <!--time-line-box start-->
            <?php if (!empty($post)) { ?>
                <?php foreach ($post as $pdt) { ?>
                    <?php if ($pdt['Type'] == 'post') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                        <div class="time-line-box text-post">
                            <a href="<?php echo !empty($usersdt) ? URL::to('showpost/'.base64_encode($pdt['Post']->id)) : "javascript:void(0)" ?>" >
                                <figure class="text-post-place">
                                    {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                </figure>
                            </a>
                            <figcaption class="tme-content" style="color: <?php echo $pdt['Post']->textcol == '' ? '' : $pdt['Post']->textcol; ?>; background-color: <?php echo $pdt['Post']->bgcol == '' ? '' : $pdt['Post']->bgcol; ?>" >
                                <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                            </figcaption>
                            <ul class="two-list-all">
                                <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                </li>
                                <li><span href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></span>
                                </li>
                            </ul>
                            <div class="sml-profile-bar">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <div class="row">
                                        <figure class="sml-pro-img">
                                            {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                        </figure> 
                                        <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="row">
                                        <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div> 
                        <?php } else { ?>
                        <div class="time-line-box text-post">
                            <a href="<?php echo !empty($usersdt) ? $adultUrl."adult/showpost/".base64_encode($usersdt->id)."/".base64_encode($pdt['Post']->id) : "javascript:void(0)" ?>" >
                                <figure class="text-post-place">
                                    <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                </figure>
                            </a>
                            <figcaption class="tme-content" style="color: <?php echo $pdt['Post']->textcol == '' ? '' : $pdt['Post']->textcol; ?>; background-color: <?php echo $pdt['Post']->bgcol == '' ? '' : $pdt['Post']->bgcol; ?>" >
                                <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                            </figcaption>
                            <ul class="two-list-all">
                                <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                </li>
                                <li><span href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></span>
                                </li>
                            </ul>
                            <div class="sml-profile-bar">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <div class="row">
                                        <figure class="sml-pro-img">
                                            {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                        </figure> 
                                        <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="row">
                                        <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div> 
                        <?php } ?>
                    <?php } else if ($pdt['Type'] == 'twitterlink'){ ?>
                        <div class="time-line-box link-post-place">
<!--                            <a href="{!! URL::to('showlink/'.base64_encode($pdt['Post']->id)) !!}">-->
                                <figure class="link-post">
                                    <div class="link-post-image">
                                        <!-- <img src="{!! asset('theme/site/images/people-drinkin.jpg') !!}"> -->
            <!--                                        <iframe border=0 frameborder=0 height=250 width=550 src="<?php echo $pdt['Post']->link; ?>"></iframe>-->
                                        <iframe border=0 frameborder=0 height=250 width=550 src="http://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2Fjack%2Fstatus%2F20"></iframe>
                                    </div>
                                    <div class="link-post-content">
                                        <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                                    </div>
                                </figure>
<!--                            </a>-->
                            <ul class="two-list-all">
                                <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                </li>
                                <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                </li>
                            </ul>
                            <div class="sml-profile-bar">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <div class="row">
                                        <figure class="sml-pro-img">
                                            {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                        </figure> 
                                        <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="row">
                                        <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>            
                    <?php } else if ($pdt['Type'] == 'instagramlink'){ ?>
                        <div class="time-line-box link-post-place">
                            <!--                                <a href="{!! URL::to('showlink/'.base64_encode($pdt['Post']->id)) !!}">-->
                            <figure class="link-post">
                                <div class="link-post-image">
                                    <!-- <img src="{!! asset('theme/site/images/people-drinkin.jpg') !!}"> -->
                                    <iframe width="100%" height="100%" src="<?php echo instagramLink($pdt['Post']->link); ?>" frameborder="0"></iframe>
                                </div>
                                <div class="link-post-content">
                                    <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                                </div>
                            </figure>
                            <!--                                </a>-->
                            <ul class="two-list-all">
                                <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                </li>
                                <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                </li>
                            </ul>
                            <div class="sml-profile-bar">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <div class="row">
                                        <figure class="sml-pro-img">
                                            {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                        </figure> 
                                        <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="row">
                                        <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>                 
                    <?php } else if ($pdt['Type'] == 'link') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                            <div class="time-line-box link-post-place">
                                <a href="<?php echo !empty($usersdt) ? URL::to('showlink/'.base64_encode($pdt['Post']->id)) : "javascript:void(0)" ?>" >
                                    <figure class="link-post">
                                        <div class="link-post-image">
                                            <!--<img src="{!! asset('theme/site/images/people-drinkin.jpg') !!}">-->
                                            <iframe width="100%" src="<?php echo substr($pdt['Post']->link, 0, 7) === "http://" ? $pdt['Post']->link : "http://" . $pdt['Post']->link; ?>" style="-webkit-transform:scale(1.0);-moz-transform-scale(1.0);"></iframe>
                                        </div>
                                        <div class="link-post-content">
                                            <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                                        </div>
                                    </figure>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="time-line-box link-post-place">
                                <a href="<?php echo $adultUrl."showlink/".base64_encode($pdt['Post']->id)?>" >
                                <a href="<?php echo !empty($usersdt) ? $adultUrl."adult/showlink/".base64_encode($usersdt->id)."/".base64_encode($pdt['Post']->id) : "javascript:void(0)" ?>" >
                                    <figure class="link-post">
                                        <div class="link-post-image">
                                           <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                        </div>
                                        <div class="link-post-content">
                                            <?php echo substr($pdt['Post']->description, 0, 40); ?>......
                                        </div>
                                    </figure>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        <?php } ?>            
                    <?php } else if ($pdt['Type'] == 'photo') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                            <div class="time-line-box photo-post-place">
                                <a href="<?php echo !empty($usersdt) ? URL::to('showphoto/'.base64_encode($pdt['Post']->id)) : "javascript:void(0)" ?>" >
                                    <figure class="photo-post">
                                        <?php if ($pdt['Post']->image != '') { ?>
                                            <img src="{!! asset('products/main/post/'.$pdt['Post']->image) !!}">
                                        <?php } else { ?>
                                            <?php echo $pdt['Post']->embed; ?>
                                        <?php } ?>                                   
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->caption, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div> 
                        <?php } else { ?>
                            <div class="time-line-box photo-post-place">
                                <a href="<?php echo !empty($usersdt) ? $adultUrl."adult/showphoto/".base64_encode($usersdt->id)."/".base64_encode($pdt['Post']->id) : "javascript:void(0)" ?>" >
                                    <figure class="photo-post">
                                        <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->caption, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div> 
                        <?php } ?>            
                    <?php } else if ($pdt['Type'] == 'album') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                            <div class="time-line-box photo-post-place">
                                <a href="<?php echo !empty($usersdt) ? URL::to('showalbum/'.base64_encode($pdt['Post']->id)) : "javascript:void(0)" ?>" >
                                    <figure class="photo-post">
                                        <img src="{!! asset('products/main/post/'.$pdt['Post']->image) !!}">
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->caption, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div>
                        <?php } else { ?>
                            <div class="time-line-box photo-post-place">
                                <a href="<?php echo !empty($usersdt) ? $adultUrl."adult/showalbum/".base64_encode($usersdt->id)."/".base64_encode($pdt['Post']->id) : "javascript:void(0)" ?>" >
                                    <figure class="photo-post">
                                        <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->caption, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <!--<span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>-->
                            </div>            
                        <?php } ?>
                    <?php } else if ($pdt['Type'] == 'blog') { ?>
                        <?php if($pdt['Post']->is_adult != 1){ ?> 
                            <div class="time-line-box blog-post-place">
                                <a href="<?php echo !empty($usersdt) ? URL::to('showblog/'.base64_encode($pdt['Post']->id)) : "javascript:void(0)" ?>" >
                                    <figure class="blog-post">
                                        <img src="{!! asset('products/main/post/'.$pdt['Post']->image) !!}">
                                        <span class="blog-icon"><img src="{!! asset('theme/site/images/blog-icon.png') !!}"></span>
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                    </figcaption>
                                </a>
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div> 
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div>
                        <?php } else { ?>
                            <div class="time-line-box blog-post-place">
                                <a href="<?php echo !empty($usersdt) ? $adultUrl."adult/showblog/".base64_encode($usersdt->id)."/".base64_encode($pdt['Post']->id) : "javascript:void(0)" ?>" >
                                    <figure class="photo-post">
                                        <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                    </figure>
                                    <figcaption class="tme-content">
                                        <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                    </figcaption>
                                </a>                                    
                                <ul class="two-list-all">
                                    <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                    </li>
                                </ul>
                                <div class="sml-profile-bar">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <div class="row">
                                            <figure class="sml-pro-img">
                                                {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                            </figure> 
                                            <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                        <div class="row">
                                            <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div> 
                                <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                            </div>
                        <?php } ?>            
                    <?php } else if ($pdt['Type'] == 'video') { ?>
                        <?php if ($pdt['Post']->video != "" || $pdt['Post']->embed != "") { ?>
                            <?php if($pdt['Post']->is_adult != 1){ ?> 
                                <div class="time-line-box video">
                                    <figure class="video-raw">
                                        <?php if ($pdt['Post']->video != "") { ?>
                                            <?php $thvid = $pdt['Post']->video ?>
                                            <video width="400" controls>
                                                <source src="{!! asset('products/main/post/'.$thvid) !!}" type="video/mp4">
                                            </video>
                                        <?php } else if ($pdt['Post']->embed != "") { ?>
                                            <?php echo $pdt['Post']->title; ?>
                                        <?php } ?>
                                        <div class="time-post"><a href="javascript:void(0)"><?php echo $pdt['Time'] ?></a>
                                        </div>
                                        <span id="play-video" class="video-icon"><a href="javascript:void(0)"><img src="{!! asset('theme/site/images/playbutton.png') !!}"></a></span>
                                    </figure>
                                    <a href="<?php echo !empty($usersdt) ? URL::to('showvideo/'.base64_encode($pdt['Post']->id)) : "javascript:void(0)" ?>" >
                                        <figcaption class="tme-content">
                                            <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                        </figcaption>
                                    </a>
                                    <ul class="two-list-all">
                                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                        </li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                        </li>
                                    </ul>
                                    <div class="sml-profile-bar">
                                        <div class="col-xs-8 col-sm-8 col-md-8">
                                            <div class="row">
                                                <figure class="sml-pro-img">
                                                    {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                                </figure> 
                                                <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <div class="row">
                                                <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                                </div>
                            <?php } else { ?>
                                <div class="time-line-box video">
                                    <a href="<?php echo !empty($usersdt) ? $adultUrl."adult/showblog/".base64_encode($usersdt->id)."/".base64_encode($pdt['Post']->id) : "javascript:void(0)" ?>" >
                                        <figure class="photo-post">
                                            <img src="{!! asset('devimg/adult-content.jpg') !!}">
                                        </figure>
                                        <figcaption class="tme-content">
                                            <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                        </figcaption>
                                    </a>                                    
                                    <!--
                                    <figure class="video-raw">
                                        <?php if ($pdt['Post']->video != "") { ?>
                                            <?php $thvid = $pdt['Post']->video ?>
                                            <video width="400" controls>
                                                <source src="{!! asset('products/main/post/'.$thvid) !!}" type="video/mp4">
                                            </video>
                                        <?php } else if ($pdt['Post']->embed != "") { ?>
                                            <?php echo $pdt['Post']->title; ?>
                                        <?php } ?>
                                        <div class="time-post"><a href="javascript:void(0)"><?php echo $pdt['Time'] ?></a>
                                        </div>
                                        <span id="play-video" class="video-icon"><a href="javascript:void(0)"><img src="{!! asset('theme/site/images/playbutton.png') !!}"></a></span>
                                    </figure>
                                    <a href="<?php echo $adultUrl."showvideo/".base64_encode($pdt['Post']->id)?>" >
                                        <figcaption class="tme-content">
                                            <?php echo substr($pdt['Post']->title, 0, 40); ?>......
                                        </figcaption>
                                    </a>
                                    -->
                                    <ul class="two-list-all">
                                        <li><a href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo countPostViews($pdt['Post']->id); ?></a>
                                        </li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-smile-o" aria-hidden="true"></i><?php echo countPostLikes($pdt['Post']->id); ?></a>
                                        </li>
                                    </ul>
                                    <div class="sml-profile-bar">
                                        <div class="col-xs-8 col-sm-8 col-md-8">
                                            <div class="row">
                                                <figure class="sml-pro-img">
                                                    {!! (isset($pdt['User']->image) && $pdt['User']->image != "") ? Html::image('products/main/user/'.$pdt['User']->image, 'Image', array('height' => 60))  : Html::image('devimg/blankuser.png', 'Image', array('height' => 60))  !!}
                                                </figure> 
                                                <figcaption class="profile-name"> <?php echo $pdt['User']->fname . " " . $pdt['User']->lname ?> </figcaption>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <div class="row">
                                                <div class="time-post"><a href="javascript:void(0)"> <?php echo $pdt['Time'] ?> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <span href="javascript:void(0)" class="shareby">Shared by T-vizzCool</span>
                                </div>
                            <?php } ?>            
                        <?php } ?>                
                    <?php } ?>
                <?php } ?>
            <?php } else { ?>
                <h1>No Posts Exists</h1> 
            <?php } ?>
            <!--time-line-box end-->
            <div class="addvertisement"><img src="{!! asset('theme/site/images/banner57.jpg') !!}" class="ing-responsive">
            </div>
            <a id="loadMore" href="" class="load-button">Load more</a>
        </div>
    </div>
</section>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">
    $('#Formvalidate').validate({
        focusInvalid: false,
        rules: {
            confirm_password: {
                equalTo: "#new_password"
            }
        }
    });
</script>
@stop
