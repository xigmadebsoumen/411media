@extends('_layouts.site')
@section('content')
<button class="cd-nav-trigger cd-image-replace">Open navigation<span aria-hidden="true"></span></button>

<section class="services-details-area first-blocks">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				{!! Html::image('products/crop/content/'.$maintenance->image, $maintenance->name, array("class"=>"img-responsive")) !!}	
			</div>
			<div class="col-md-6">
				<h1 class="heading">{!! $maintenance->title !!}</h1>
				{!! $maintenance->description !!}
			</div>
		</div>
	</div>
</section>
@stop
