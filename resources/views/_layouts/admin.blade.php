<?php //echo "Ok"; print_r($settingdt); exit; ?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>411 Media| Admin Panel</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="{!! asset('theme/admin/css/theme-blue.css') !!}"/>
        <link rel="stylesheet" type="text/css" href="{!! asset('theme/admin/css/style.css') !!}"/>
        <!-- EOF CSS INCLUDE -->
        <!-- MIN JQUERY -->
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/jquery/jquery.min.js') !!}"></script>

    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="#"><h3>411 Media</h3></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            {!! Html::image('products/crop/members/'.$user->image, $user->name) !!} 
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                {!! Html::image('products/crop/members/'.$user->image, $user->name) !!} 
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{!! ucfirst($user->name) !!}</div>
                                <div class="profile-data-title">{!! ucfirst($user->designation) !!}</div>
                            </div>
                            <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <?php
                    $path = isset($_SERVER['HTTP_REFERER']);
                    $_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
                    $segments = explode('/', $_SERVER['REQUEST_URI_PATH']);
                    $last = end($segments);
                    $second_last = prev($segments);
                    $third_last = $segments[count($segments)-3];
                    // print_r($last); return;
                    ?>
                    <li class="<?php if ($last == 'dashboard' || $second_last == 'dashboard') { ?>active<?php } ?>" >
                        <a href="{!! URL::to('admin/dashboard') !!}"><span class="fa fa-tachometer"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li> 

                    <li class="xn-openable <?php if ($second_last == 'settings' || $second_last == 'site-banner') { ?>active<?php } ?>">
                        <a href="#"><span class="fa fa-cog"></span> <span class="xn-text">Settings</span></a>
                        <ul>
                            <li class="<?php if ($last == 'site-setting') { ?>active<?php } ?>"><a href="{!! URL::to('admin/settings/site-setting') !!}">Site Settings</a></li>
                            <li class="<?php if ($last == 'site-banner' || $second_last == 'site-banner' || $third_last == 'site-banner') { ?>active<?php } ?>"><a href="{!! URL::to('admin/settings/site-banner') !!}">Site Banner</a></li>
                            <li class="<?php if ($last == 'email-template') { ?>active<?php } ?>"><a href="{!! URL::to('admin/settings/email-template') !!}">Email Template</a></li>
                            <!--<li class="<?php if ($last == 'site-banner') { ?>active<?php } ?>"><a href="{!! URL::to('admin/settings/site-banner') !!}">Site Banner</a></li>-->
                        </ul>
                    </li>
                    
                    <li  class="<?php if($last == 'users' || $second_last == 'users'){ ?>active<?php } ?>">
                        <a href="{!! URL::to('admin/users') !!}"><span class="fa fa-user" aria-hidden="true"></span> <span class="xn-text">User</span></a>
                    </li>        
                                      
                    
                    <li class="xn-openable <?php if($last == 'contents' || $second_last == 'contents' || $third_last == 'contents'){ ?>active<?php } ?>">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Content Pages</span></a>
                        <ul>
                            <li class="<?php if($second_last == 'over-staticfietsen'){ ?>active<?php } ?>" >
                                <a href="{!! URL::to('admin/contents') !!}"> Contents List </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="xn-openable <?php if($last == 'ads' || $second_last == 'ads' || $third_last == 'ads'){ ?>active<?php } ?>">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Pages Advertisements </span></a>
                        <ul>
                            <li class="<?php if($last == 'ads' || $second_last == 'ads' || $third_last == 'ads'){ ?>active<?php } ?>" >
                                <a href="{!! URL::to('admin/ads') !!}"> Advertisements List </a>
                            </li>
                        </ul>
                    </li>                    
                    
                    <li class="<?php if ($last == 'my-profile' || $second_last == 'my-profile') { ?>active<?php } ?>" >
                        <a href="{!! URL::to('admin/my-profile') !!}"><span class="fa fa-user"></span> <span class="xn-text">My Profile</span></a>                        
                    </li> 
                     <!--<li class="<?php if ($last == 'banner-content' || $second_last == 'banner-content') { ?>active<?php } ?>" >
                        <a href="{!! URL::to('admin/banner-content') !!}"><span class="fa fa-user"></span> <span class="xn-text">Banner Content</span></a>                        
                    </li> -->  
                    
                    <!--
                    <li class="xn-openable <?php if ($second_last == 'ourbus') { ?>active<?php } ?>">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Our Buses</span></a>
                        <ul>
                            <li class="<?php if ($last == 'about') { ?>active<?php } ?>" >
                                <a href="{!! URL::to('admin/ourbus/about') !!}"><span class="fa fa-comments"></span>Our Buses</a>
                            </li>
                        </ul>
                    </li>


                    <li class="xn-openable <?php if ($second_last == 'ride') { ?>active<?php } ?>">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Ride</span></a>
                        <ul>
                            <li class="<?php if ($last == 'about') { ?>active<?php } ?>">
                                <a href="{!! URL::to('admin/ride/about') !!}"><i class="fa fa-comments" ></i>Ride List</a>
                            </li>
                        </ul>
                    </li>
                    <li class="xn-openable <?php if ($second_last == 'route') { ?>active<?php } ?>">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Our Routes</span></a>
                        <ul>
                            <li class="<?php if ($last == 'about') { ?>active<?php } ?>" >
                                <a href="{!! URL::to('admin/route/about') !!}"><span class="fa fa-comments"></span>Our Route</a>
                            </li>
                        </ul>
                    </li>
                    <li class="xn-openable <?php if ($second_last == 'rideturn') { ?>active<?php } ?>">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Newsletter Detail</span></a>
                        <ul>
                            <li class="<?php if ($last == 'about') { ?>active<?php } ?>" >
                                <a href="{!! URL::to('admin/rideturn/about') !!}"><span class="fa fa-comments"></span>Newsletter Details</a>
                            </li>
                        </ul>
                    </li>
                    -->
              <!--      <li class="<?php if ($last == 'our-route' || $second_last == 'our-route') { ?>active<?php } ?>" >
                        <a href="{!! URL::to('admin/our-route') !!}"><span class="fa fa-user"></span> <span class="xn-text">Our Route</span></a>                        
                    </li>  -->
                  <!-- <li  class="<?php if ($last == 'online-store' || $second_last == 'online-store') { ?>active<?php } ?>">
                        <a href="{!! URL::to('admin/online-store') !!}"><span class="fa fa-shopping-cart" aria-hidden="true"></span> <span class="xn-text">Online Store</span></a>
                    </li>
                    <li  class="<?php if ($last == 'order' || $second_last == 'order') { ?>active<?php } ?>">
                        <a href="{!! URL::to('admin/order') !!}"><span class="fa fa-truck" aria-hidden="true"></span> <span class="xn-text">Order</span></a>
                    </li>-->
                  <!-- <li class="xn-openable <?php if ($second_last == 'gallery') { ?>active<?php } ?>">
                        <a href="#"><span class="fa fa-desktop"></span> <span class="xn-text">Gallery</span></a>
                        <ul>
                            <li class="<?php if ($last == 'photos') { ?>active<?php } ?>">
                                <a href="{!! URL::to('admin/gallery/photos') !!}"><i class="fa fa-camera" aria-hidden="true"></i>Photos Gallery</a>
                            </li>
                            <li class="<?php if ($last == 'videos') { ?>active<?php } ?>">
                                <a href="{!! URL::to('admin/gallery/videos') !!}"><i class="fa fa-video-camera" aria-hidden="true"></i>Video Gallery</a>
                            </li>
                        </ul>
                    </li>
                    <li  class="<?php if ($last == 'release' || $second_last == 'release') { ?>active<?php } ?>">
                        <a href="{!! URL::to('admin/release') !!}"><span class="fa fa-file-o" aria-hidden="true"></span> <span class="xn-text">Press Release</span></a>
                    </li>
                    <li  class="<?php if ($last == 'news' || $second_last == 'news') { ?>active<?php } ?>">
                        <a href="{!! URL::to('admin/news') !!}"><span class="fa fa-calendar"></span> <span class="xn-text">News & Events</span></a>
                    </li>
                    <li  class="<?php if ($last == 'blog' || $second_last == 'blog') { ?>active<?php } ?>">
                        <a href="{!! URL::to('admin/blog') !!}"><span class="fa fa-bullhorn"></span> <span class="xn-text">Blogs</span></a>
                    </li>
                    <li  class="<?php if ($last == 'query' || $second_last == 'query') { ?>active<?php } ?>">
                        <a href="{!! URL::to('admin/query') !!}"><span class="fa fa-question-circle" aria-hidden="true"></span> <span class="xn-text">User Queries</span></a>
                    </li>-->
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                </ul>
                <!-- END BREADCRUMB -->

                @yield('content')
            </div>
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="{{URL::to('admin/logout')}}" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->                

        <!-- START SCRIPTS -->
        <!-- CKEDITER -->
        <script type="text/javascript" src="{!! asset('theme/admin/js/editor/ckeditor/ckeditor.js') !!}"></script>                              

        <script>
    window.onload = function(){

    CKEDITOR.replace('description', {
    filebrowserBrowseUrl : '{!! URL::to("theme/admin/js/editor/kcfinder/browse.php?type=files") !!}',
            filebrowserImageBrowseUrl : '{!! URL::to("theme/admin/js/editor/kcfinder/browse.php?type=images") !!}',
            filebrowserFlashBrowseUrl : '{!! URL::to("theme/admin/js/editor/kcfinder/browse.php?type=flash") !!}',
            filebrowserUploadUrl : '{!! URL::to("theme/admin/js/editor/kcfinder/upload.php?type=files") !!}',
            filebrowserImageUploadUrl : '{!! URL::to("theme/admin/js/editor/kcfinder/upload.php?type=images") !!}',
            filebrowserFlashUploadUrl : '{!! URL::to("theme/admin/js/editor/kcfinder/upload.php?type=flash") !!}'
    });
    }
        </script>
        <!-- END CKEDITER -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/jquery/jquery-ui.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/bootstrap/bootstrap.min.js') !!}"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src="{!! asset('theme/admin/js/plugins/icheck/icheck.min.js') !!}"></script>        
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/scrolltotop/scrolltopcontrol.js') !!}"></script>

        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/morris/raphael-min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/morris/morris.min.js') !!}"></script>       
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/rickshaw/d3.v3.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/rickshaw/rickshaw.min.js') !!}"></script>
        <script type='text/javascript' src="{!! asset('theme/admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}"></script>
        <script type='text/javascript' src="{!! asset('theme/admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}"></script>                
        <script type='text/javascript' src="{!! asset('theme/admin/js/plugins/bootstrap/bootstrap-datepicker.js') !!}"></script>                
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/owl/owl.carousel.min.js') !!}"></script>                 

        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/moment.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/daterangepicker/daterangepicker.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/datatables/jquery.dataTables.min.js') !!}"></script> 

        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/bootstrap/bootstrap-datepicker.js') !!}"></script>                
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/bootstrap/bootstrap-file-input.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/bootstrap/bootstrap-select.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins/tagsinput/jquery.tagsinput.min.js') !!}"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript">
        var site_url = '<?php echo asset("theme/admin/") ?>';
        </script>
        <script type="text/javascript" src="{!! asset('theme/admin/js/settings.js') !!}"></script>

        <script type="text/javascript" src="{!! asset('theme/admin/js/plugins.js') !!}"></script>        
        <script type="text/javascript" src="{!! asset('theme/admin/js/actions.js') !!}"></script>

        <script type="text/javascript" src="{!! asset('theme/admin/js/demo_dashboard.js') !!}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
        <!-- END TEMPLATE -->
        <!-- END SCRIPTS -->   
        <script type="text/javascript">
    var cookieValue = $.cookie("css");
    // alert(cookieValue);
    if (cookieValue != 'undefined') {
    $("#theme").attr("href", cookieValue);
    }
        </script>       
    </body>
</html>






