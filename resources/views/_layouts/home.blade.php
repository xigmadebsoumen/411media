<!DOCTYPE html>
<html lang=en>
    <head>
        <meta charset=utf-8>
        <meta content="IE=edge" http-equiv=X-UA-Compatible>
        <meta content="width=device-width,initial-scale=1" name=viewport>
        <title>411 Media</title>
        <link href="https://fonts.googleapis.com/css?family=Nixie+One" rel=stylesheet>
        <link href="{!! asset('theme/site/css/font-awesome.css') !!}" rel=stylesheet>
        <link href="{!! asset('theme/site/css/flaticon.css') !!}" rel=stylesheet>
        <link href="{!! asset('theme/site/css/bootstrap.min.css') !!}" rel=stylesheet>
        <link href="{!! asset('theme/site/css/style.css') !!}" rel=stylesheet>
        <link href="{!! asset('theme/site/css/responsive.css') !!}" rel=stylesheet>
        <!--[if lt IE 9]><script src=https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js></script><script src=https://oss.maxcdn.com/respond/1.4.2/respond.min.js></script><![endif]-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <script src="{!! asset('theme/site/js/vunit.js') !!}" ></script>
        <script>
            new vUnit({
                CSSMap: {
                    ".vh": {
                        property: "height",
                        reference: "vh"
                    },
                    ".vw": {
                        property: "width",
                        reference: "vw"
                    },
                    ".vwfs": {
                        property: "font-size",
                        reference: "vw"
                    },
                    ".vhmt": {
                        property: "margin-top",
                        reference: "vh"
                    },
                    ".vhmb": {
                        property: "margin-bottom",
                        reference: "vh"
                    },
                    ".vminw": {
                        property: "width",
                        reference: "vmin"
                    },
                    ".vmaxw": {
                        property: "width",
                        reference: "vmax"
                    }
                }
            }).init()
        </script>
    </head>
    <body>
        @if(Session::has('message'))
        <div style="margin-top:18px;" class="alert {{ Session::get('alert-class', 'alert-info') }} fade in">
            <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
            {{ Session::get('message') }}
        </div>
        @endif        
        @yield('content')
        <!--<script src=js/1.12.0-jquery.min.js></script>-->
        <script src="{!! asset('theme/site/js/bootstrap.min.js') !!}"></script>
        <script src="{!! asset('theme/site/js/main.js') !!}"></script>
    </body>
</html>