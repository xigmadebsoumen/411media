<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Peoplesbus</title>
  
    <link href="{!! asset('theme/site/css/bootstrap.min.css') !!}" rel="stylesheet">
  <link href="{!! asset('theme/site/css/style.css') !!}" rel="stylesheet">
  <link href="{!! asset('theme/site/css/responsivetabs.css') !!}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="{!! asset('theme/site/css/font-awesome.min.css') !!}" rel="stylesheet">
  </head>
  
  <body>
<!--header open-->
<header class="header-area clearfix">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <div class="logo-area">
          <a href="{!! URL::to('') !!}"><img src="{!! asset('theme/site/images/logo.png') !!}" alt="logo" class="img-responsive"></a>
        </div>
      </div>
      <div class="col-sm-7">
        <div class="menu-area">
          <nav class="navbar navbar-default">
           
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
     
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
              <li><a href="#"> <i class="fa fa-map-marker" aria-hidden="true"></i> San Francisco</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GettingAround <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ URL::route('site.farespasses') }}">Fares & Passes</a></li>
                  <li><a href="#">Routes & Stops</a></li>
                </ul>
              </li>
              <li><a href="#">Link</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
              <li><a href="#">Log In</a></li>
              <li><a href="#"><span>Sign Up</span></a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>
<!--header closed-->
   @yield('content')
  <footer class="footer-area">
  <div class="container">
    <div class="row">
      <div class="footer-links">
        <div class="col-md-3">
          <div class="info-area">
            <h2>Info</h2>
            <ul class="list-unstyled">
              <li><a href="#">Overview</a></li>
              <li><a href="#">Safety</a></li>
              <li><a href="#">Drivers</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="info-area">
            <h2>Company</h2>
            <ul class="list-unstyled">
              <li><a href="#">Our Story</a></li>
              <li><a href="#">Blog</a></li>
              <li><a href="#">Careers</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="info-area">
            <h2>Contact Us</h2>
            <ul class="list-unstyled">
              <li><a href="#">Support Center</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Facebook</a></li>
              <li><a href="#">Instagram</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="info-area">
            <h2>Policies</h2>
            <ul class="list-unstyled">
              <li><a href="#">Terms Of Services</a></li>
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">$IM Insurance</a></li>
              <li><a href="#">Rules of the Road</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer_bottom">
      <div class="row">
        <div class="col-md-8">
          <div class="footer-logo">
            <a href="index.html"><img src="{!! asset('theme/site/images/footer-logo.png') !!}" alt="logo" class="img-responsive"></a>
          </div>
          </div>
        <div class="col-md-4">
          <div class="footer-copyright">
            <p>Peoples.Bus© 2016. All rights reserved</p>
          </div>
        </div>
      
    </div>
  </div>
</div>  
</footer>
<!--footer-closed-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="{!! asset('theme/site/js/responsivetabs.js') !!}"></script>
  <script src="{!! asset('theme/site/js/sidecar.js') !!}"></script>
  <script src="{!! asset('theme/site/js/bootstrap.min.js') !!}"></script>

  <script type="text/javascript">
      
  function newsletter_subscribe() {

  var email = $("#email").val();
  var dep_datetime = $("#datetime").val();
  var pickup = $("#pickup").val();

   if(datetime==""){
   alert("Please Enter Dateparture Date And Time ");
   }
   if(pickup==""){
   alert("Please Select A Pickup Location");
   }

   url= "{{ Config::get('config.baseurl') }}ride-and-turn";
   var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

    
   if (testEmail.test(email)) {
 
    $.ajax({
        url : url,
        async: false,
        type :'POST',
        data : { 
             email:email,dep_datetime:dep_datetime,pickup:pickup,
            _token : "{{ csrf_token() }}",
        },
    }).done(function(response){
      //alert(response); return;
      $("#newsletteremail").val('');
      if(response == 0){
        alert('You already subscribe to our newsletter!');
      } else {
        
        alert('Thank You!');
      }
      
    });
   } else {
    alert('Plaese Enter a valid email!');
    }
   }
    </script>
	
<script>

function openNav() {
    document.getElementById("mySidenav").style.width = "300px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

</script>	
	
	
	
  </body>
</html>