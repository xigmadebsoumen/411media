<!DOCTYPE html>
<html lang=en>
    <head>
        <meta charset=utf-8>
        <meta content="IE=edge" http-equiv=X-UA-Compatible>
        <meta content="width=device-width,initial-scale=1" name=viewport>
        <title>411 Media</title>
        <link href="https://fonts.googleapis.com/css?family=Nixie+One" rel=stylesheet>
        <link href="{!! asset('theme/site/css/font-awesome.css') !!}" rel=stylesheet>
        <link href="{!! asset('theme/site/css/flaticon.css') !!}" rel=stylesheet>
        <link href="{!! asset('theme/site/css/bootstrap.min.css') !!}" rel=stylesheet>
        <link href="{!! asset('theme/site/css/style.css') !!}" rel=stylesheet>
        <link href="{!! asset('theme/site/css/responsive.css') !!}" rel=stylesheet>
        <!--[if lt IE 9]><script src=https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js></script><script src=https://oss.maxcdn.com/respond/1.4.2/respond.min.js></script><![endif]-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <script src="{!! asset('theme/site/js/vunit.js') !!}" ></script>
        <script>
            new vUnit({
                CSSMap: {
                    ".vh": {
                        property: "height",
                        reference: "vh"
                    },
                    ".vw": {
                        property: "width",
                        reference: "vw"
                    },
                    ".vwfs": {
                        property: "font-size",
                        reference: "vw"
                    },
                    ".vhmt": {
                        property: "margin-top",
                        reference: "vh"
                    },
                    ".vhmb": {
                        property: "margin-bottom",
                        reference: "vh"
                    },
                    ".vminw": {
                        property: "width",
                        reference: "vmin"
                    },
                    ".vmaxw": {
                        property: "width",
                        reference: "vmax"
                    }
                }
            }).init()
        </script>
    </head>
<?php
    $path = isset($_SERVER['HTTP_REFERER']);
    $_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $segments = explode('/', $_SERVER['REQUEST_URI_PATH']);
    $last = end($segments);
    $second_last = prev($segments);
?>
    <body>
        @if(Session::has('message'))
        <div style="margin-top:18px;" class="alert {{ Session::get('alert-class', 'alert-info') }} fade in">
            <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">X</a>
            {{ Session::get('message') }}
        </div>
        @endif         
<?php if(!Auth::check()){ $viewLogDt = 0; } else { $viewLogDt = 0; } ?>       
    <header>
        <div class="container-fluid">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <?php if(Auth::check()){?>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <span class="flaticon-right-menu-bars ham" onclick="openNav()"></span>
                    <!--navigation-->
                    <div id="mySidenav" class="sidenav">
                        <span class="closebtn flaticon-error cross" onclick="closeNav()"></span>
                        <ul class="menu">
                            <li><a href="{!! URL::to('timeline') !!}"><span class="flaticon-calendar-clock"></span>Timeline</a></li>
                            <!--<li><a href="{!! URL::to('my-profile') !!}"><span class="flaticon-user"></span>Edit Profile</a></li> -->
                            <li><a href="{!! URL::to('profile') !!}"><span class="flaticon-user"></span>My Profile</a></li>
                            <!-- <li><a href="{!! URL::to('mysettings') !!}"><span class="flaticon-user"></span> Settings </a></li> --> 
                            <li><a href="javascript:void(0)"><span class="flaticon-money-bag-with-dollar-symbol"></span>Revenue Program</a></li>
                            <li><a href="javascript:void(0)"><span class="flaticon-message"></span>Messages</a></li>
                            <li><a href="filter-timeline.html"><span class="flaticon-google-trends"></span>Trending</a></li>
                            <li><a href="{!! URL::to('addpost') !!}"><span class="flaticon-post-it"></span>Post</a></li>
                            <li><a href="{!! URL::to('logout') !!}"><span class="flaticon-post-it"></span>Logout</a></li>
                        </ul>
                        <div class="nav-search">
                            <form class="form-horizontal">
                                <div class="form-group search-box">
                                    <input type="text" class="form-control search-input" id="search_boxdtqq" placeholder="Search...">
                                    <input type="submit" class="magni-button">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <a href="{!! URL::to('') !!}" class="inner-logo">
                        <img src="{!! asset('theme/site/images/inner-logo.png') !!}" class="img-responsive">
                    </a>
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <div class="search-place">
                    <a href="{!! URL::to('') !!}" class="inner-logo"><img src="{!! asset('theme/site/images/inner-logo.png') !!}" class="img-responsive">
                    </a>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 mob-grid-off">
                <?php if(Auth::check()){?>
                    <span><?php $udt = Auth::user(); echo $udt->pname; ?></span>
                <?php } ?>                          
                    </div>
                    <div class="col-xs-6 col-sm-9 col-md-9 col-lg-9 mob-grid-off">
                        <form class="form-horizontal">
                            <div class="form-group search-box">
                                <input type="text" class="form-control search-input" id="search_boxdt" placeholder="Search...">
                                <input type="button" class="magni-button">
                                <div id="searchres" class="searchres" style="display: none;"></div>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <!-- <a href="javascript:void(0)" class="filter"><span class="flaticon-filter"></span></a> -->
                <?php if(Auth::check()){?>
                <a href="{!! URL::to('mysettings') !!}"class="filter"><span class="flaticon-nut-icon"></span></a>
                <?php } ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </header>
    <!--header-->        
        
        @if(Session::has('message'))
        <div style="margin-top:18px;" class="alert {{ Session::get('alert-class', 'alert-info') }} fade in">
            <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">�</a>
            {{ Session::get('message') }}
        </div>
        @endif        
        @yield('content')
        <!--<script src=js/1.12.0-jquery.min.js></script>-->
        <script src="{!! asset('theme/site/js/bootstrap.min.js') !!}"></script>
        <script src="{!! asset('theme/site/js/script.js') !!}"></script>
        <!--<script src="{!! asset('theme/site/js/main.js') !!}"></script>-->
    <script>
        $(function() {
            $('#play').hide();
            $('#unmute').hide();
        })
    </script>  
    <script type="text/javascript">
        
        $(document).on("click", function(e){
            if( !$("#search_box").is(e.target) ){ 
            //if your box isn't the target of click, hide it
                $("#searchres").hide();
            }
        });        
        
        $(document).ready(function () {
            //var urlsearch = "{{ Config::get('config.baseurl') }}searchfriends1";
            var urlsearch = "{!! URL::to('') !!}/searchfriends"; 
            $("#search_boxdt").keyup(function () {
                var search_string = $("#search_boxdt").val();
                if (search_string == '') {
                    $("#searchres").html('');
                    $("#searchres").hide("slow");
                } else {
                    postdata = {'string': search_string}
                    $.post(urlsearch, postdata, function (data) {
                        $("#searchres").html(data);
                        $("#searchres").show("slow");
                    });
                }
            });
        });
        function fillme(name) {
            $("#search_boxdt").val(name);
            $("#searchres").html('');
        }
    </script>   

<script>

function openNav() {
    document.getElementById("mySidenav").style.width = "300px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

</script>
	
    </body>
</html>